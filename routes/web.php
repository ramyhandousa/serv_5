<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'HomeController@index');
Route::get('lang', 'HomeController@language');

// admin dashboard
Route::group(array('prefix' => 'admin', 'middleware' => 'admin'), function() {
    Route::resource('site', 'Admin\SiteController')->middleware('role:site');
    Route::resource('groups', 'Admin\GroupController')->middleware('role:groups');
    Route::resource('pages', 'Admin\PagesController')->middleware('role:pages');
    Route::resource('continents', 'Admin\ContinentController')->middleware('role:continents');
    Route::resource('countries', 'Admin\CountryController')->middleware('role:countries');
    Route::resource('governates', 'Admin\GovernateController')->middleware('role:governates');
    Route::resource('cities', 'Admin\CityController')->middleware('role:cities');
    Route::resource('activity-types', 'Admin\ActivityTypeController')->middleware('role:activity_type');
    Route::resource('activities', 'Admin\ActivitiesController')->middleware('role:activities');
    Route::resource('restaurant-categories', 'Admin\RestaurantCategoriesController')->middleware('role:restaurant_category');
    Route::resource('restaurants', 'Admin\RestaurantsController')->middleware('role:restaurants');
    Route::resource('market-places', 'Admin\MarketsController')->middleware('role:market_place');
    Route::resource('newsletter', 'Admin\NewsletterController')->middleware('role:newsletter');
    Route::resource('users', 'Admin\UsersController')->middleware('role:users');
    Route::resource('visitors', 'Admin\VisitorsController')->middleware('role:visitors');
    Route::resource('admins', 'Admin\AdminsController')->middleware('role:admins');
    Route::resource('slider', 'Admin\SliderController')->middleware('role:slider');
    Route::resource('contact', 'Admin\ContactController')->middleware('role:contact');
    Route::resource('customer_service', 'Admin\CustomerServiceController')->middleware('role:customer_service');
    Route::resource('faq', 'Admin\FaqController')->middleware('role:faq');
    Route::resource('bank', 'Admin\BankController')->middleware('role:bank');
    Route::resource('office', 'Admin\OfficeController')->middleware('role:office');
    Route::resource('job', 'Admin\JobController')->middleware('role:job');
    Route::resource('cars', 'Admin\CarController')->middleware('role:car');
    Route::resource('brands', 'Admin\CarBrandController')->middleware('role:brand');
    Route::resource('models', 'Admin\CarModelController')->middleware('role:model');
    Route::resource('airports', 'Admin\AirportController')->middleware('role:airport');
    Route::resource('drivers', 'Admin\DriverController')->middleware('role:driver');
    Route::resource('addons', 'Admin\CarAddonController')->middleware('role:addon');
    Route::resource('security', 'Admin\SecurityController')->middleware('role:security');
    Route::resource('cars_city', 'Admin\CityDifferentController')->middleware('role:car_city');
    Route::resource('cars_offers', 'Admin\CarOfferController')->middleware('role:offer');
    Route::resource('cars_booking', 'Admin\CarBookingController')->middleware('role:cars_booking');
    Route::resource('facilities', 'Admin\FacilitiesController')->middleware('role:facilities');
    Route::resource('parking', 'Admin\ParkingController')->middleware('role:parking');
    Route::resource('room_types', 'Admin\RoomTypesController')->middleware('role:room_types');
    Route::resource('room_features', 'Admin\RoomFeaturesController')->middleware('role:room_features');
    Route::resource('food_types', 'Admin\FoodTypesController')->middleware('role:food_types');
    Route::resource('room_views', 'Admin\RoomViewsController')->middleware('role:room_views');
    Route::resource('branches', 'Admin\BranchesController')->middleware('role:branches');
    Route::resource('agents_categories', 'Admin\AgentsCategoryController')->middleware('role:agents_categories');
    
    Route::resource('agents', 'Admin\AgentsController')->middleware('role:agents');

    Route::resource('hotels', 'Admin\HotelsController')->middleware('role:hotels');
    Route::resource('rooms', 'Admin\RoomsController')->middleware('role:rooms');    
    Route::resource('suppliers', 'Admin\SuppliersController')->middleware('role:suppliers');

    Route::get('get_models', 'Admin\CarBrandController@getModels');
    Route::get('get_governates/{id}', 'Admin\GovernateController@getGovernates');
    Route::get('get_governates_agent/{id}', 'Admin\GovernateController@getGovernatesAgent');
    Route::get('get_cities/{id}', 'Admin\CityController@getCities');
    Route::get('get_cities_agent/{id}', 'Admin\CityController@getCitiesAgent');
    
    Route::get('logout', 'Admin\AdminController@logout');
    Route::get('/', 'Admin\AdminController@index');
});

Route::group(array('prefix' => 'admin'), function() {
    Route::get('login', 'Admin\AdminController@getLogin');
    Route::post('login', 'Admin\AdminController@postLogin');
    Route::get('not_allow', 'Admin\AdminController@not_allow');
});

