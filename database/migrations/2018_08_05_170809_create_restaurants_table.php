<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->string('ar_name');
            $table->string('en_name');
            $table->string('image');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('phone');
            $table->string('ar_address');
            $table->string('en_address');
            $table->string('ar_overview');
            $table->string('en_overview');
            $table->string('email');
            $table->string('facebook');
            $table->string('website');
            $table->boolean('delivery');
            $table->date('work_from');
            $table->date('work_to');
            $table->enum('active', ['yes', 'no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
