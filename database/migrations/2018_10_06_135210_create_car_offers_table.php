<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('active', ['yes', 'no']);
            $table->string('ar_title');
            $table->string('en_title');
            $table->string('ar_details');
            $table->string('en_details');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_offers');
    }
}
