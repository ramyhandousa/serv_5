<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('brand_id');
            $table->string('model_id');
            $table->string('city_id');
            $table->string('count');
            $table->string('seats');
            $table->string('doors');
            $table->string('bags');
            $table->enum('air', ['yes', 'no']);
            $table->enum('vetis', ['auto', 'manual']);
            $table->enum('fuel', ['petrol', 'petrol']);
            $table->enum('size', ['economy','lux','normal','record','full','medium','small','compact','transport','family','suv','standard']);
            $table->enum('active', ['yes', 'no']);
            $table->enum('paypal', ['yes', 'no']);
            $table->enum('bank', ['yes', 'no']);
            $table->enum('cash', ['yes', 'no']);
            $table->enum('admin_sure', ['yes', 'no']);
            $table->double('price');
            $table->double('comision');
            $table->text('ar_details');
            $table->text('en_details');
            $table->text('ar_conditions');
            $table->text('en_conditions');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
