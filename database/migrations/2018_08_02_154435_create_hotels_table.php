<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ar_name');
            $table->string('en_name');
            $table->string('image');
            $table->string('ar_address');
            $table->string('en_address');
            $table->string('longitude');
            $table->string('latitude');
            $table->string('country_id');
            $table->string('state_id');
            $table->string('city_id');
            $table->string('stars');
            $table->string('email');
            $table->string('website');
            $table->string('phone');
            $table->text('ar_policy_and_terms');
            $table->text('en_policy_and_terms');
            $table->boolean('parking');
            $table->enum('active', ['yes', 'no']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
