<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_bookings', function (Blueprint $table) {
           
            $table->increments('id');
            $table->string('user_id');
            $table->string('car_id');
            $table->string('country_id');
            $table->string('bank_id');
            $table->string('txn_id');
            $table->string('city_id');
            $table->string('airport_id');
            $table->string('driver_id');
            $table->string('city_dev');
            $table->string('airport_dev');
            $table->string('date_from');
            $table->string('time_from');
            $table->string('date_to');
            $table->string('time_to');
            $table->string('car_delivery');
            $table->string('flight_no');
            $table->string('flight_name');
            $table->string('flight_date');
            $table->string('flight_time');
            $table->string('flight_from');
            $table->string('flight_to');
            $table->string('insurances');
            $table->double('price');
            $table->double('plus_price');
            $table->string('seen');
            $table->double('security_price');
            $table->double('addons_price');
            $table->text('extras');
            $table->string('days');
            $table->string('active');
            $table->string('pay_method');
            $table->string('owner_account');
            $table->string('ipan');
            $table->string('paypal_result');
           

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_bookings');
    }
}
