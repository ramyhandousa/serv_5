<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hotel_id')->unsigned();
            $table->string('ar_name');
            $table->string('en_name');
            $table->string('ar_foods');
            $table->string('en_foods');
            $table->string('ar_open_for');
            $table->string('en_open_for');
            $table->double('price');
            $table->text('ar_menu');
            $table->text('en_menu');
            $table->boolean('smoking');
            $table->boolean('breakfast');
            $table->text('ar_description');
            $table->text('en_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_restaurants');
    }
}
