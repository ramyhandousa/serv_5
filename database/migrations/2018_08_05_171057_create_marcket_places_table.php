<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarcketPlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_places', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id')->unsigned();
            $table->string('ar_name');
            $table->string('en_name');
            $table->text('ar_overview');
            $table->text('en_overview');
            $table->string('image');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('phone');
            $table->string('ar_address');
            $table->string('en_address');
            $table->string('email');
            $table->string('no_of_shops');
            $table->enum('active', ['yes', 'no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_places');
    }
}
