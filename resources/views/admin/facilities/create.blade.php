@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.facilities')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/facilities')}}"> {{trans('layout.facilities')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.facility_add')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.facility_add')}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/facilities')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="input_1" class="col-sm-3 col-form-label">{{trans('layout.facility_name_ar')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('ar_name', null, ['class'=>'form-control','id'=>'input_1']) }}
                                @if($errors->has('ar_name'))
                                <div class="alert alert-danger">{{$errors->first('ar_name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.facility_name_en')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('en_name', null, ['class'=>'form-control','id'=>'input_2']) }}
                                @if($errors->has('en_name'))
                                <div class="alert alert-danger">{{$errors->first('en_name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="icon" class="col-sm-3 col-form-label">{{trans('layout.icon')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('icon', null, ['class'=>'form-control','id'=>'icon']) }}
                                @if($errors->has('icon'))
                                <div class="alert alert-danger">{{$errors->first('icon')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="options" class="col-sm-3 col-form-label">{{trans('layout.options')}}</label>
                            <div class="col-sm-9" id="options">
                                <div class="col-sm-5 {{$pull}}" id="options_1">
                                    {{ Form::text('ar_options[]', null, ['class'=>'form-control','placeholder'=>trans('layout.option_ar')]) }}
                                </div>
                                <div class="col-sm-5 {{$pull}}" id="options_2">
                                    {{ Form::text('en_options[]', null, ['class'=>'form-control','placeholder'=>trans('layout.option_en')]) }}
                                </div>
                                <div class="col-sm-2 pull-right">
                                    <button class="btn btn-success btn-lg btn-block" style="height: 36px;" id="more_options">{{trans('layout.more')}}</button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.add')}}</button>
                <a href="{{URL::to('admin/facilities')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection

@section('scripts')
<script>
    $('#more_options').click(function () {
        $('#options').append('<br>');
        $('#options').append($('#options_1').clone());
        $('#options').append($('#options_2').clone());
        $('#options').append('<div class="col-sm-2 <?php echo $pull; ?>"><button class="btn btn-danger btn-lg btn-block delete_options" style="height: 36px;"><?php echo trans('layout.delete'); ?></button></div><div class="clearfix"></div>');
        return false;
    });
    $("body").on("click", ".delete_options", function (e) {
        $(this).parent("div").prev("div").prev("div").prev("br").remove();
        $(this).parent("div").prev("div").remove();
        $(this).parent("div").prev("div").remove();
        $(this).parent("div").remove();
    });
</script>
@endsection