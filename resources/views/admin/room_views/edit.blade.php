@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.room_views')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/room_views')}}"> {{trans('layout.room_views')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.room_view_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.room_view_edit')}} : {{$room_view[$lang.'_name']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/room_views/'.$room_view['id'])}}" method="post" enctype="multipart/form-data">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="input_1" class="col-sm-3 col-form-label">{{trans('layout.room_view_name_ar')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('ar_name', $room_view['ar_name'], ['class'=>'form-control','id'=>'input_1']) }}
                                @if($errors->has('ar_name'))
                                <div class="alert alert-danger">{{$errors->first('ar_name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.room_view_name_en')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('en_name', $room_view['en_name'], ['class'=>'form-control','id'=>'input_2']) }}
                                @if($errors->has('en_name'))
                                <div class="alert alert-danger">{{$errors->first('en_name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ar_notes" class="col-sm-3 col-form-label">{{trans('layout.room_view_notes_ar')}}</label>
                            <div class="col-sm-9">
                                {{ Form::textarea('ar_notes', $room_view['ar_notes'], ['class'=>'form-control','id'=>'ar_notes']) }}
                                @if($errors->has('ar_notes'))
                                <div class="alert alert-danger">{{$errors->first('ar_notes')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="en_notes" class="col-sm-3 col-form-label">{{trans('layout.room_view_notes_en')}}</label>
                            <div class="col-sm-9">
                                {{ Form::textarea('en_notes', $room_view['en_notes'], ['class'=>'form-control','id'=>'en_notes']) }}
                                @if($errors->has('en_notes'))
                                <div class="alert alert-danger">{{$errors->first('en_notes')}}</div>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/room_views')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection