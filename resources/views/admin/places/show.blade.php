@extends('admin.layouts.table')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.places')}} : {{$branch[$lang.'_name']}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/branches?id='.$branch['supplier_id'])}}"> {{trans('layout.branches')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.place_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.place_edit')}} : {{$branch[$lang.'_name']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/places/'.$branch['id'])}}" method="post" enctype="multipart/form-data">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="input_1" class="col-sm-2 col-form-label">{{trans('layout.branch')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('branch', $branch[$lang.'_name'], ['class'=>'form-control','id'=>'input_1','readonly']) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_0" class="col-sm-2 col-form-label">{{trans('layout.places')}}</label>
                            <div class="col-sm-10">
                                <?php $cities = App\City::orderBy('governate_id', 'desc')->get(); ?>
                                @foreach($cities as $city)
                                <?php
                                $found = FALSE;
                                $price = 0;
                                $time = 0;
                                $one = App\Place::where('branch_id', $branch['id'])->where('city_id', $city['id'])->first();
                                if ($one) {
                                    $found = TRUE;
                                    $price = $one['price'];
                                    $time = $one['time'];
                                }
                                ?>
                                <div class="col-sm-12 {{$pull}}">
                                    <div class="col-sm-2 {{$pull}}" style="vertical-align: middle;">
                                        {{ Form::checkbox('places[]',$city['id'],$found,['id'=>'Checkbox_'.$city['id']]) }}
                                        <label for="Checkbox_{{$city['id']}}">{{$city[$lang.'_name']}}</label>
                                    </div>
                                    <div class="col-sm-2 {{$pull}} col-form-label" style="vertical-align: middle;">
                                        <label>{{trans('layout.delivery_cost')}}</label>
                                    </div>
                                    <div class="col-sm-2 {{$pull}}" style="vertical-align: middle;">
                                        {{ Form::text('prices_'.$city['id'], $price, ['class'=>'form-control']) }}
                                    </div>
                                    <div class="col-sm-1 {{$pull}} col-form-label" style="vertical-align: middle;">
                                        {{$branch['supplier']['Country'][$lang.'_currency']}}
                                    </div>
                                    <div class="col-sm-2 {{$pull}} col-form-label" style="vertical-align: middle;">
                                        <label>{{trans('layout.activity_time')}}</label>
                                    </div>
                                    <div class="col-sm-2 {{$pull}}" style="vertical-align: middle;">
                                        {{ Form::text('times_'.$city['id'], $time, ['class'=>'form-control']) }}
                                    </div>
                                    <div class="col-sm-1 {{$pull}} col-form-label" style="vertical-align: middle;">
                                        {{trans('layout.minute')}}
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                                <br>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/branches?id='.$branch['supplier_id'])}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection