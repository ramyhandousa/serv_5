@extends('admin.layouts.table')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/cars')}}"> {{trans('layout.car')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.booking')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            @if (Session::has('message'))
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissible">
                        {{ Session::get('message')}}
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    </div>
                </div>
            </div>
            @endif

            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body">
                    <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive">
                        <thead>
                            <tr> 
                             <th class="{{$text}}">
                                #
                            </th>
                            <th class="{{$text}}">
                                الحجز
                            </th>
                            <th class="{{$text}}">
                                العضو
                            </th>
                            <th class="{{$text}}">
                                السيارة
                            </th>
                            <th class="{{$text}}">
                                التكلفة
                            </th>
                            <th class="{{$text}}">
                                الفترة
                            </th>
                            <th class="{{$text}}">
                                الحالة
                            </th>
                            <th class="{{$text}}">
                                التفاصيل
                            </th>
                            <th class="{{$text}}">
                                مسح
                            </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($car->Bookings as $booking)
                            <tr>
                                <td>{{$i}}</td>
                                <td>  {{ $booking->id }}</td>
                                <td> {{ $booking->user_id }} - {{ $booking->User->name }}</td>
                                <td>
                                    {{ $booking['Car']['Brand'][$lang.'_title'] .' - '.$booking['car']['Models'][$lang.'_title'] }} 
                                </td>
                                <td>
                                    <?php
                                    $count_day = ceil(abs((strtotime($booking['date_to']) - strtotime($booking['date_from'])) / 86400));
                                    $plus_price = 0;
                                    $addons_price = 0;
                                    if (isset($booking['addons'])) {
                                        foreach ($booking['addons'] as $add) {
                                            $addons_price = $addons_price + Addon::find($add)['price'];
                                        }
                                    }
                                    $addons_price = $addons_price * $count_day;
                                    $security_price = 0;
                                    if ($booking['insurances'] == 1) {
                                        foreach ($booking['car']->Security as $secure) {
                                            $security_price = $security_price + $secure['price'];
                                        }
                                    }
                                    if ($booking['car_delivery'] == "off") {
                                        $delivery_price = CityDelivery::where('from', $booking['city_dev'])->where('to', $booking['city'])->first();
                                        if (count($delivery_price) == 0) {
                                            $delivery_pricee = CityDelivery::where('to', $booking['city_dev'])->where('from', $booking['city'])->first();
                                            if (count($delivery_pricee) > 0) {
                                                $plus_price = $delivery_pricee->price;
                                            }
                                        } else {
                                            $plus_price = $delivery_price->price;
                                        }
                                    }
                                    $one_price = ($booking['car']->price + $plus_price) * $count_day;
                                    $total_price = $one_price + $addons_price + $security_price;
                                    ?>
                                    {{ $total_price }} 
                                </td>
                                
                                <td>{{$booking->date_from.'&nbsp;&nbsp;&nbsp;&nbsp;'.$booking->date_to}}</td>

                                <td>
                                    @if($booking->active == "yet")
                                    <span class="btn btn-xs btn-info">فى انتظار الدفع</span>
                                    @elseif($booking->active == "wait")
                                    <span class="btn btn-xs btn-warning">فى انتظار تأكيد الدفع</span>
                                    @elseif($booking->active == "no")
                                    <span class="btn btn-xs btn-danger">ملغى من الإدارة</span>
                                    @elseif($booking->active == "yes")
                                    <span class="btn btn-xs btn-success">تم الدفع</span>
                                    @elseif($booking->active == "end")
                                    <span class="btn btn-xs btn-primary">منتهى</span>
                                    @elseif($booking->active == "cancel")
                                    <span class="btn btn-xs btn-warning">مكنسل</span>
                                    @elseif($booking->active == "cancel_admin")
                                    <span class="btn btn-xs btn-danger">مكنسل من الإدارة</span>
                                    @endif
                                </td>
                                <td>
                                    {{ Form::open(array('url' =>'admin/cars_booking/'.$booking->id.'/edit', 'method' => 'GET')) }}
                                        <button  type="submit" class="btn default btn-xs blue"><i class="fa fa-edit"></i> {{trans('layout.show')}} </button>
                                    {{ Form::close() }}
                                </td>
                                <td>
                                    {{ Form::open(array('url' => 'admin/cars_booking/'.$booking->id,'method' => 'DELETE')) }}
                                        <button  type="submit" class="btn default btn-xs red"><i class="fa fa-trash-o"></i> {{trans('layout.delete')}}  </button>
                                    {{ Form::close() }}

                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>


                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->          
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection