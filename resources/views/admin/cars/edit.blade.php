@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.car')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/cars')}}"> {{trans('layout.car')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.car_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.car_edit')}} : {{$car[$lang.'_name']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/cars/'.$car['id'])}}" method="post" enctype="multipart/form-data">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif


                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_paypal')}}</label>
                            <div class="col-sm-9">
                                {{Form::select('paypal',['yes'=>trans('layout.yes_active') , 'no'=>trans('layout.no_active')],$car['paypal'],['class'=>'form-control']) }}
                                {{ $errors->first('paypal','<div class="alert alert-danger">:message</div>')}}    

                                @if($errors->has('paypal'))
                                    <div class="alert alert-danger">{{$errors->first('paypal')}}</div>
                                @endif

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_cash')}}</label>
                            <div class="col-sm-9">
                                {{Form::select('cash',['yes'=>trans('layout.yes_active') , 'no'=>trans('layout.no_active')],$car['cash'],['class'=>'form-control']) }}

                                @if($errors->has('cash'))
                                <div class="alert alert-danger">{{$errors->first('cash')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_bank')}}  </label>
                            <div class="col-sm-9">
                                {{Form::select('bank',['yes'=>trans('layout.yes_active') , 'no'=>trans('layout.no_active')],$car['bank'],['class'=>'form-control']) }}

                                @if($errors->has('bank'))
                                <div class="alert alert-danger">{{$errors->first('bank')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">  {{trans('layout.car_admin_sure')}} </label>
                            <div class="col-sm-9">
                                {{Form::select('admin_sure',['yes'=>trans('layout.yes_active') , 'no'=>trans('layout.no_active')],$car['admin_sure'],['class'=>'form-control']) }}

                                @if($errors->has('admin_sure'))
                                <div class="alert alert-danger">{{$errors->first('admin_sure')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_brand')}}</label>
                            <div class="col-sm-9">
                                {{Form::select('brand_id',$brand,$car['brand_id'],['class'=>'form-control','id'=>"brand_id"]) }}

                                @if($errors->has('brand_id'))
                                <div class="alert alert-danger">{{$errors->first('brand_id')}}</div>
                                @endif
                            </div>
                        </div>
                      
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_model')}}</label>
                            <div class="col-sm-9" id="models">
                                {{Form::select('model_id',[''=>'إختر الماركة اولا'],$car['model_id'],['class'=>'form-control','id'=>'model_id']) }}
                                @if($errors->has('model_id'))
                                <div class="alert alert-danger">{{$errors->first('model_id')}}</div>
                                @endif
                                <input type="hidden" value="{{$car->model_id}}" id="model">

                            </div>
                            <input type="hidden" value="" id="model">
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_city')}}</label>
                            <div class="col-sm-9">
                                {{Form::select('city_id',$cities,$car['city_id'],['class'=>'form-control']) }}

                                @if($errors->has('city_id'))
                                <div class="alert alert-danger">{{$errors->first('city_id')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_count')}}</label>
                            <div class="col-sm-9">
                                {{Form::number('count',$car['count'],['class'=>'form-control']) }}

                                @if($errors->has('count'))
                                <div class="alert alert-danger">{{$errors->first('count')}}</div>
                                @endif
                            </div>
                        </div>

                          <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_seats')}}</label>
                            <div class="col-sm-9">
                                {{Form::number('seats',$car['seats'],['class'=>'form-control']) }}

                                @if($errors->has('seats'))
                                <div class="alert alert-danger">{{$errors->first('seats')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_doors')}}</label>
                            <div class="col-sm-9">
                                {{Form::number('doors',$car['doors'],['class'=>'form-control']) }}

                                @if($errors->has('doors'))
                                <div class="alert alert-danger">{{$errors->first('doors')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_bags')}}</label>
                            <div class="col-sm-9">
                                {{Form::number('bags',$car['bags'],['class'=>'form-control']) }}

                                @if($errors->has('bags'))
                                <div class="alert alert-danger">{{$errors->first('bags')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_air')}}</label>
                            <div class="col-sm-9">
                                {{Form::select('air',['no'=>'لا','yes'=>'نعم'],$car['air'],['class'=>'form-control']) }}

                                @if($errors->has('air'))
                                <div class="alert alert-danger">{{$errors->first('air')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_vetis')}}</label>
                            <div class="col-sm-9">
                                {{Form::select('vetis',['auto'=>'أوتوماتيك','manual'=>'عادى'],$car['vetis'],['class'=>'form-control']) }}

                                @if($errors->has('vetis'))
                                <div class="alert alert-danger">{{$errors->first('vetis')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_fuel')}}</label>
                            <div class="col-sm-9">
                                {{Form::select('fuel',['petrol'=>'بنزين','desil'=>'ديزل'],$car['fuel'],['class'=>'form-control']) }}

                                @if($errors->has('fuel'))
                                <div class="alert alert-danger">{{$errors->first('fuel')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_size')}}</label>
                            <div class="col-sm-9">
                                {{Form::select('size',['small' => 'صغيره', 'compact' => 'كومبكت',  'transport' => 'نقل',   'family' => 'عائليه', 'suv' => 'suv',  'standard' => 'ستاندرد', 'economy' => 'إقتصادية', 'lux' => 'فاخرة','normal' => 'عادية','record' => 'قياسية','full' => 'متكاملة','medium' => 'متوسطه'],$car['size'],['class'=>'form-control']) }}

                                @if($errors->has('size'))
                                <div class="alert alert-danger">{{$errors->first('size')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_price')}}</label>
                            <div class="col-sm-9">
                                {{Form::text('price',$car['price'],['class'=>'form-control']) }}

                                @if($errors->has('price'))
                                <div class="alert alert-danger">{{$errors->first('price')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_comision')}}</label>
                            <div class="col-sm-9">
                                {{Form::text('comision',$car['comision'],['class'=>'form-control']) }}
                                @if($errors->has('comision'))
                                <div class="alert alert-danger">{{$errors->first('comision')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group last row">
                            <label class="control-label col-md-3"> {{trans('layout.car_details_ar')}}</label>
                            <div class="col-md-9">
                                {{Form::textarea('ar_details',$car['ar_details'],array('class'=>'ckeditor form-control','rows'=>"6",'data-error-container'=>"#editor2_error"))}}
                                <div id="editor2_error">
                                </div>
                                @if($errors->has('ar_details'))
                                <div class="alert alert-danger">{{$errors->first('ar_details')}}</div>
                                @endif 
                            </div>
                        </div>

                        <div class="form-group last row">
                            <label class="control-label col-md-3"> {{trans('layout.car_details_en')}} </label>
                            <div class="col-md-9">
                                {{Form::textarea('en_details',$car['en_details'],array('class'=>'ckeditor form-control','rows'=>"6",'data-error-container'=>"#editor2_error"))}}
                                <div id="editor2_error">
                                </div>
                                @if($errors->has('en_details'))
                                <div class="alert alert-danger">{{$errors->first('en_details')}}</div>
                                @endif 
                            </div>
                        </div>

                    

                        <div class="form-group last row">
                            <label class="control-label col-md-3">{{trans('layout.car_conditions_ar')}}</label>
                            <div class="col-md-9">
                                {{Form::textarea('ar_conditions',$car['ar_conditions'],array('class'=>'ckeditor form-control','rows'=>"6",'data-error-container'=>"#editor2_error"))}}
                                <div id="editor2_error">
                                </div>
                                @if($errors->has('ar_conditions'))
                                <div class="alert alert-danger">{{$errors->first('ar_conditions')}}</div>
                                @endif 
                           </div>
                        </div>

                        <div class="form-group last row">
                            <label class="control-label col-md-3"> {{trans('layout.car_conditions_en')}}</label>
                            <div class="col-md-9">
                                {{Form::textarea('en_conditions',$car['en_conditions'],array('class'=>'ckeditor form-control','rows'=>"6",'data-error-container'=>"#editor2_error"))}}
                                <div id="editor2_error">
                                </div>
                                @if($errors->has('en_conditions'))
                                <div class="alert alert-danger">{{$errors->first('en_conditions')}}</div>
                                @endif
                            </div>
                        </div>




                      <?php $security = App\Security::where('active', 'yes')->get(); ?>

                                @foreach($security as $one)
                                   
                                    <?php
                                    $current = \App\CarSecurity::where('car_id', $car->id)->where('security_id', $one->id)->first();
                                    if (!empty($current)) {
                                        $price = $current['price'];
                                    } else {
                                        $price = $one['price'];
                                    }
                                    ?>
                                    <div class="form-group row">
                                        <label class="col-sm-3 control-label"> {{$one['ar_title']}} 
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="number" name="security_price[]" value="{{$price}}"
                                                class="form-control">
                                            <input type="hidden" name="security_id[]" value="{{$one['id']}}"
                                                class="form-control">
                                        </div>
                                    </div>
                               
                                @endforeach


                        <div class="form-group row">
                            <label for="input_3" class="col-sm-3 col-form-label">{{trans('layout.car_image')}}</label>
                            <div class="col-sm-9">
                                {{ Form::file('image',['class'=>'form-control','id'=>'input_3']) }}
                                @if($errors->has('image'))
                                <div class="alert alert-danger">{{$errors->first('image')}}</div>
                                @endif
                            </div>
                        </div>
      

                        <div class="form-group row">
                            <label for="input_3" class="col-sm-3 col-form-label">{{trans('layout.car_image')}}</label>
                            <div class="col-sm-9">
                                <img src="{{URL::to('upload/car/'.$car['image'])}}" class="img-thumbnail">
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/cars')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection
@section('scripts')
<script>
        
        $('#brand_id').change(function (e) {
            var val = $(this).val();
            var model = $('#model').val();
            $.ajax({
                type: "GET",
                url: "{{asset('/')}}" + "admin/get_models?id=" + val + "&model=" + model,
                success: function (datas) {
                    $('#models').html(datas);
                }
            });
            return false;
        });

</script>
@endsection