@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.car_models')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/models')}}"> {{trans('layout.car_model')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.car_model_add')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.car_model_add')}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/models')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.choose_brand')}}</label>
                            <div class="col-sm-9">
                                {{Form::select('brand_id',$brands,null,['class'=>'form-control']) }}

                                @if($errors->has('brand_id'))
                                <div class="alert alert-danger">{{$errors->first('brand_id')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_model_name_ar')}} </label>
                            <div class="col-sm-9">
                                {{Form::text('ar_title',null,['class'=>'form-control']) }}
                                @if($errors->has('ar_title'))
                                <div class="alert alert-danger">{{$errors->first('ar_title')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_model_name_en')}} </label>
                            <div class="col-sm-9">
                                {{Form::text('en_title',null,['class'=>'form-control']) }}
                                @if($errors->has('en_title'))
                                <div class="alert alert-danger">{{$errors->first('en_title')}}</div>
                                @endif

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_model_year')}} </label>
                            <div class="col-sm-9">
                                {{Form::text('year',null,['class'=>'form-control']) }}
                                @if($errors->has('year'))
                                <div class="alert alert-danger">{{$errors->first('year')}}</div>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.add')}}</button>
                <a href="{{URL::to('admin/models')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection
@section('scripts')
<script>

    $('#mark_id').change(function (e) {
        var val = $(this).val();
        var model = $('#model').val();
        $.ajax({
            type: "GET",
            url: "{{asset('/')}}" + "admin/get_models?id=" + val + "&model=" + model,
            success: function (datas) {
                $('#models').html(datas);
            }
        });
        return false;
    });

</script>
@endsection
