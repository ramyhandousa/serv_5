{{ Form::select('city_id',$cities, null, ['class'=>'form-control select3','id'=>'city_id']) }}

<script>
    // Select 2
    $('.select3').select2();
</script>


@if($from == "branch")
<script>
    $('#governate_id').change(function () {
        var val = $(this).val();
        var base_url = $('#base-url').val();
        if (val == "") {
            val = 0;
        }
        $.ajax({
            type: "GET",
            url: base_url + "/admin/branches/" + val,
            success: function (data) {
                $('#cities').html(data);
            }
        });
    });
</script>
@endif