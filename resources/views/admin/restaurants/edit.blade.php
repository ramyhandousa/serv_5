@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        {{trans('layout.restaurants')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/restaurants')}}"> {{trans('layout.restaurants')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.restaurant_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.restaurant_edit')}} : {{$Restaurants[$lang.'_name']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/restaurants/'.$Restaurants['id'])}}" method="post" enctype="multipart/form-data">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="input_0" class="col-sm-3 col-form-label">{{trans('layout.restaurant_active')}}</label>
                            <div class="col-sm-9">
                                {{ Form::select('active', ['yes'=>trans('layout.yes_active') , 'no'=>trans('layout.no_active')],$Restaurants['active'], ['class'=>'form-control','id'=>'input_0']) }}
                                @if($errors->has('active'))
                                <div class="alert alert-danger">{{$errors->first('active')}}</div>
                                @endif
                            </div>
                        </div>

                            <div class="form-group row">
                                <label for="country_id" class="col-sm-3 col-form-label">{{trans('layout.country')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::select('country_id',$countries, $Restaurants['Country']['city_id'], ['class'=>'form-control select2','id'=>'country_id']) }}
                                    @if($errors->has('country_id'))
                                        <div class="alert alert-danger">{{$errors->first('country_id')}}</div>
                                    @endif
                                </div>
                            </div>

                        <div class="form-group row">
                            <label for="country_id" class="col-sm-3 col-form-label">{{trans('layout.city')}}</label>
                            <div class="col-sm-9">
                                <div id="cities">
                                    {{ Form::select('city_id',$cities, $Restaurants['city_id'], ['class'=>'form-control select2','id'=>'country_id']) }}
                                </div>
                                @if($errors->has('city_id'))
                                <div class="alert alert-danger">{{$errors->first('city_id')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="type_id" class="col-sm-3 col-form-label">{{trans('layout.restaurant_category')}}</label>
                            <div class="col-sm-9">
                                <div id="cities">
                                    {{ Form::select('category_id',$RestaurantCategory, $Restaurants['category_id'], ['class'=>'form-control select2','id'=>'governate_id']) }}
                                </div>
                                @if($errors->has('type_id'))
                                <div class="alert alert-danger">{{$errors->first('type_id')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_1" class="col-sm-3 col-form-label">{{trans('layout.restaurant_name_ar')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('ar_name', $Restaurants['ar_name'], ['class'=>'form-control','id'=>'input_1']) }}
                                @if($errors->has('ar_name'))
                                <div class="alert alert-danger">{{$errors->first('ar_name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.restaurant_name_en')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('en_name', $Restaurants['en_name'], ['class'=>'form-control','id'=>'input_2']) }}
                                @if($errors->has('en_name'))
                                <div class="alert alert-danger">{{$errors->first('en_name')}}</div>
                                @endif
                            </div>
                        </div>
                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.restaurant_overview_ar')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::textarea('ar_overview', $Restaurants['ar_overview'], ['class'=>'form-control','id'=>'input_2']) }}
                                    @if($errors->has('ar_overview'))
                                        <div class="alert alert-danger">{{$errors->first('ar_overview')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.restaurant_overview_ar')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::textarea('en_overview', $Restaurants['en_overview'], ['class'=>'form-control','id'=>'input_2']) }}
                                    @if($errors->has('en_overview'))
                                        <div class="alert alert-danger">{{$errors->first('en_overview')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.restaurant_address_ar')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('ar_address', $Restaurants['ar_address'], ['class'=>'form-control','id'=>'input_2']) }}
                                    @if($errors->has('ar_address'))
                                        <div class="alert alert-danger">{{$errors->first('ar_address')}}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.restaurant_address_en')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('en_address', $Restaurants['en_address'], ['class'=>'form-control','id'=>'input_2']) }}
                                    @if($errors->has('en_address'))
                                        <div class="alert alert-danger">{{$errors->first('en_address')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.restaurant_email')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('email', $Restaurants['email'], ['class'=>'form-control','id'=>'input_2']) }}
                                    @if($errors->has('email'))
                                        <div class="alert alert-danger">{{$errors->first('email')}}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.restaurant_phone')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('phone', $Restaurants['phone'], ['class'=>'form-control','id'=>'input_2']) }}
                                    @if($errors->has('phone'))
                                        <div class="alert alert-danger">{{$errors->first('phone')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.restaurant_work_from')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('work_from', $Restaurants['work_from'], ['class'=>'form-control','id'=>'input_2']) }}
                                    @if($errors->has('work_from'))
                                        <div class="alert alert-danger">{{$errors->first('work_from')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.restaurant_work_to')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('work_to', $Restaurants['work_to'], ['class'=>'form-control','id'=>'input_12']) }}
                                    @if($errors->has('open_to'))
                                        <div class="alert alert-danger">{{$errors->first('open_to')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.facebook')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('facebook', $Restaurants['facebook'], ['class'=>'form-control','id'=>'input_12']) }}
                                    @if($errors->has('facebook'))
                                        <div class="alert alert-danger">{{$errors->first('facebook')}}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.website')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('website', $Restaurants['website'], ['class'=>'form-control','id'=>'input_12']) }}
                                    @if($errors->has('website'))
                                        <div class="alert alert-danger">{{$errors->first('website')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.delivery')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::select('delivery', ['yes'=>trans('layout.yes') , 'no'=>trans('layout.no')],$Restaurants['delivery'], ['class'=>'form-control','id'=>'input_0']) }}
                                    @if($errors->has('delivery'))
                                        <div class="alert alert-danger">{{$errors->first('delivery')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_3" class="col-sm-3 col-form-label">{{trans('layout.restaurant_image')}}</label>
                                <div class="col-sm-9">
                                    <img class="mb-4 img-bordered" src="{{asset('/upload/restaurants/').'/'.$Restaurants['image']}}" width="100px" height="100px">
                                    <input class="form-control" id="input_13" name="image" type="file">
                                </div>
                                @if($errors->has('image'))
                                    <div class="alert alert-danger">{{$errors->first('image')}}</div>
                                @endif
                            </div>

                        <div class="form-group row">
                            <label for="input_5" class="col-sm-3 col-form-label">{{trans('layout.map')}}</label>
                            <div class="col-sm-9">
                                <div id="mapCanvas" style="height:450px;" class="col-md-12"></div>
                                <input type="hidden" value="{{$Restaurants['lat']}}" id="lat" name="latitude" />
                                <input type="hidden" value="{{$Restaurants['lng']}}" id="lng" name="longitude" />
                                @if($errors->has('lat'))
                                <div class="alert alert-danger">{{$errors->first('lat')}}</div>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/cities')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection


@section('scripts')
<script>
    $('#country_id').change(function () {
        var val = $(this).val();
        var base_url = $('#base-url').val();
        if (val == "") {
            val = 0;
        }
        $.ajax({
            type: "GET",
            url: base_url + "/admin/activities/" + val + "?from=city",
            success: function (data) {
                console.log(data)
                $('#cities').html(data);
            }
        });
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9uAy1z9dxw9aHx6_NJPRA0ih0UD-dCOU&callback=initMap&language=ar&region=EG"></script>
<script type="text/javascript">
    var marker;
    var lat;
    var lng;
    var map;
    function updateMarkerPosition(latLng) {
        document.getElementById('lat').value = latLng.lat();
        document.getElementById('lng').value = latLng.lng();
    }

    function placeMarker(location) {
        if (marker) {
            marker.setPosition(location);
        } else {
            marker = new google.maps.Marker({
                position: location,
                map: map
            });
        }
    }

    function initialize() {
        var lat = document.getElementById('lat').value;
        var lng = document.getElementById('lng').value;
        if (!lat && !lng) {
            var latLng = new google.maps.LatLng({{$Restaurants['latitude']}}, {{$Restaurants['longitude']}});
        } else {
            var latLng = new google.maps.LatLng(lat, lng);
        }

        map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 5,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        marker = new google.maps.Marker({
            position: latLng,
            map: map
        });
        marker.set(map);
        updateMarkerPosition(latLng);
        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
            updateMarkerPosition(event.latLng);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection