@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.restaurant_category')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/restaurant_categories')}}"> {{trans('layout.restaurant_category')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.restaurant_category_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.restaurant_category')}} : {{$ActivityType[$lang.'_title']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/restaurant-categories/'.$ActivityType['id'])}}" method="post" enctype="multipart/form-data">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif

                            <div class="form-group row">
                                <label for="input_0" class="col-sm-3 col-form-label">{{trans('layout.restaurant_category_active')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::select('active', ['yes'=>trans('layout.yes_active') , 'no'=>trans('layout.no_active')],$ActivityType['active'], ['class'=>'form-control','id'=>'input_0']) }}
                                    @if($errors->has('active'))
                                        <div class="alert alert-danger">{{$errors->first('active')}}</div>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group row">
                                <label for="input_1" class="col-sm-3 col-form-label">{{trans('layout.restaurant_category_name_ar')}}</label>

                                <div class="col-sm-9">

                                  {{ Form::text('ar_title', $ActivityType['ar_title'], ['class'=>'form-control','id'=>'input_1']) }}

                                        @if($errors->has('ar_title'))
                                            <div class="alert alert-danger">{{$errors->first('ar_title')}}</div>
                                        @endif

                                </div>

                            </div>


                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.restaurant_category_name_en')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('en_title', $ActivityType['en_title'], ['class'=>'form-control','id'=>'input_2']) }}
                                    @if($errors->has('en_title'))
                                        <div class="alert alert-danger">{{$errors->first('en_title')}}</div>
                                    @endif
                                </div>
                            </div>


                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/restaurant-categories')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection


@section('scripts')
<script>
    $('#country_id').change(function () {
        var val = $(this).val();
        var base_url = $('#base-url').val();
        if (val == "") {
            val = 0;
        }
        $.ajax({
            type: "GET",
            url: base_url + "/admin/cities/" + val + "?from=city",
            success: function (data) {
                $('#cities').html(data);
            }
        });
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9uAy1z9dxw9aHx6_NJPRA0ih0UD-dCOU&callback=initMap&language=ar&region=EG"></script>
@endsection