@extends('admin.layouts.table')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.restaurant_category')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.restaurant_category')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">

        @if (Session::has('message'))
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissible">
                        {{ Session::get('message')}}
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    </div>
                </div>
            </div>
        @endif

            <div class="box">

                <div class="box-header">
                    <a href="{{URL::to('admin/restaurant-categories/create')}}" class="btn btn-lg bg-fuchsia">{{trans('layout.restaurant_category_add')}}</a>
                </div>

                <div class="box-body">
                    <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive">
                        <thead>
                            <tr> 
                                <th class="{{$text}}">#</th>
                                <th class="{{$text}}">{{trans('layout.restaurant_category_name_ar')}}</th>
                                <th class="{{$text}}">{{trans('layout.restaurant_category_name_en')}}</th>
                                <th class="{{$text}}">{{trans('layout.restaurant_category_active')}}</th>
                                <th class="{{$text}}">{{trans('layout.edit')}}</th>
                                <th class="{{$text}}">{{trans('layout.delete')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                            @foreach($restaurant_categories as $restaurant)
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$restaurant['ar_title']}}</td>
                                <td>{{$restaurant['en_title']}}</td>

                                <td>
                                    @if($restaurant['active'] == "yes")
                                    {{trans('layout.yes_active')}}
                                    @else
                                    {{trans('layout.no_active')}}
                                    @endif
                                </td>

                                <td>

                                    {{ Form::open(array('url' =>'admin/restaurant-categories/'.$restaurant->id.'/edit', 'method' => 'GET')) }}
                                    <button  type="submit" class="btn default btn-sm btn-info"><i class="fa fa-edit"></i> {{trans('layout.edit')}} </button>
                                    {{ Form::close() }}

                                </td>

                                <td>

                                    {{ Form::open(array('url' =>'admin/restaurant-categories/'.$restaurant->id, 'method' => 'DELETE')) }}
                                    <button  type="submit" class="btn default btn-sm btn-danger"><i class="fa fa-trash-o"></i> {{trans('layout.delete')}} </button>
                                    {{ Form::close() }}

                                </td>
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                        </tbody>
                    </table>


                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->          
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection