@extends('admin.layouts.table')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.customer_service')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.customer_service')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">

            @if (Session::has('message'))
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissible">
                        {{ Session::get('message')}}
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    </div>
                </div>
            </div>
            @endif

            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">{{trans('layout.customer_service_search')}}</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <form action="{{URL::to('admin/customer_service')}}" method="get" enctype="multipart/form-data">

                    <div class="box-body">
                        <div class="row">
                            <div class="col-12">
                                <br>
                                <div class="form-group row">
                                    <label for="status" class="col-sm-2 col-form-label">{{trans('layout.ticket_status')}}</label>
                                    <div class="col-sm-10">
                                        {{ Form::select('status',[''=>trans('layout.choose_status'),'new'=>trans('layout.ticket_new'),'yet'=>trans('layout.ticket_yet'),'done'=>trans('layout.ticket_done')], null, ['class'=>'form-control','id'=>'status']) }}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone" class="col-sm-2 col-form-label">{{trans('layout.phone')}}</label>
                                    <div class="col-sm-10">
                                        {{ Form::text('phone', null, ['class'=>'form-control','id'=>'phone']) }}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-danger btn-lg" style="font-size: 16px;">{{trans('layout.search')}}</button>
                    </div>

                </form>

            </div>

            <div class="box">
                <div class="box-header">
                    <a href="{{URL::to('admin/customer_service/create')}}" class="btn btn-lg bg-fuchsia">{{trans('layout.customer_service_add')}}</a>
                </div>
                <div class="box-body">
                    <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive">
                        <thead>
                            <tr>
                                <th class="{{$text}}">#</th>
                                <th class="{{$text}}">{{trans('layout.phone')}}</th>
                                <th class="{{$text}}">{{trans('layout.ticket_title')}}</th>
                                <th class="{{$text}}">{{trans('layout.admins_one')}}</th>
                                <th class="{{$text}}">{{trans('layout.ticket_status')}}</th>
                                <th class="{{$text}}">{{trans('layout.edit')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tickets as $ticket)
                            <tr>
                                <td>{{$ticket['id']}}</td>
                                <td>{{$ticket['phone']}}</td>
                                <td>{{$ticket['title']}}</td>
                                <td>{{$ticket['Admin']['name']}}</td>
                                <td>{{trans('layout.ticket_'.$ticket['status'])}}</td>
                                <td>
                                    {{ Form::open(array('url' =>'admin/customer_service/'.$ticket->id.'/edit', 'method' => 'GET')) }}
                                    <button  type="submit" class="btn default btn-sm btn-info"><i class="fa fa-edit"></i> {{trans('layout.edit')}}</button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->          
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection