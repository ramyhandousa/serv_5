@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.customer_service')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/customer_service')}}"> {{trans('layout.customer_service')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.customer_service_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.customer_service_edit')}} : {{$ticket[$lang.'_title']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/customer_service/'.$ticket['id'])}}" method="post" enctype="multipart/form-data">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="input_1" class="col-sm-3 col-form-label">{{trans('layout.phone')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('phone', $ticket['phone'], ['class'=>'form-control','id'=>'input_1','readonly']) }}
                                @if($errors->has('phone'))
                                <div class="alert alert-danger">{{$errors->first('phone')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">{{trans('layout.admins_one')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('admin', $ticket['Admin']['name'], ['class'=>'form-control','id'=>'name','readonly']) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status" class="col-sm-3 col-form-label">{{trans('layout.ticket_status')}}</label>
                            <div class="col-sm-9">
                                {{ Form::select('status',[''=>trans('layout.choose_status'),'new'=>trans('layout.ticket_new'),'yet'=>trans('layout.ticket_yet'),'done'=>trans('layout.ticket_done')], $ticket['status'], ['class'=>'form-control','id'=>'status']) }}
                                @if($errors->has('status'))
                                <div class="alert alert-danger">{{$errors->first('status')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.ticket_title')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('title', $ticket['title'], ['class'=>'form-control','id'=>'input_2']) }}
                                @if($errors->has('title'))
                                <div class="alert alert-danger">{{$errors->first('title')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="note" class="col-sm-3 col-form-label">{{trans('layout.ticket_note')}}</label>
                            <div class="col-sm-9">
                                {{ Form::textarea('note', $ticket['note'], ['class'=>'form-control','id'=>'note','rows'=>8]) }}
                                @if($errors->has('note'))
                                <div class="alert alert-danger">{{$errors->first('note')}}</div>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/customer_service')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection