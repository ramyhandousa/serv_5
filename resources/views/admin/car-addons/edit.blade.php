@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.car_addon')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/addons')}}"> {{trans('layout.car_addon')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.car_addon_name_ar')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.car_addon_name_ar')}} : {{$addon[$lang.'_title']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

                 

        <form action="{{URL::to('admin/addons/'.$addon['id'])}}" method="post" enctype="multipart/form-data">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif
                            <div class="form-group row">
                                <label for="input_0" class="col-sm-3 col-form-label">{{trans('layout.car_addon_active')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::select('active', ['yes'=>trans('layout.yes_active') , 'no'=>trans('layout.no_active')],$addon['active'], ['class'=>'form-control','id'=>'input_0']) }}
                                    @if($errors->has('active'))
                                        <div class="alert alert-danger">{{$errors->first('active')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_1" class="col-sm-3 col-form-label">{{trans('layout.car_addon_name_ar')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('ar_title', $addon['ar_title'], ['class'=>'form-control','id'=>'input_1']) }}
                                    @if($errors->has('ar_title'))
                                        <div class="alert alert-danger">{{$errors->first('ar_title')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.car_addon_name_en')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('en_title', $addon['en_title'], ['class'=>'form-control','id'=>'en_title']) }}
                                    @if($errors->has('en_title'))
                                        <div class="alert alert-danger">{{$errors->first('en_title')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.car_addon_price')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('price',  $addon['price'], ['class'=>'form-control','id'=>'price']) }}
                                    @if($errors->has('price'))
                                        <div class="alert alert-danger">{{$errors->first('price')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.car_addon_details_ar')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::textarea('ar_details',  $addon['ar_details'], ['class'=>'form-control','id'=>'en_details']) }}
                                    @if($errors->has('ar_details'))
                                        <div class="alert alert-danger">{{$errors->first('ar_details')}}</div>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.car_addon_details_en')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::textarea('en_details', $addon['en_details'], ['class'=>'form-control','id'=>'ar_details']) }}
                                    @if($errors->has('ar_details '))
                                        <div class="alert alert-danger">{{$errors->first('ar_details')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_3" class="col-sm-3 col-form-label">{{trans('layout.car_addon_image')}}</label>
                                <div class="col-sm-9 ">
                                    <img class="mb-4" src="{{asset('/upload/addon/').'/'.$addon['image']}}" width="100px">
                                    <input class="form-control" id="input_3" name="image" type="file">
                                </div>
                            </div>

                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/addons')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection

