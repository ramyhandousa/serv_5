@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.car_addon')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/addons/')}}"> {{trans('layout.car_addon')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.car_addon_add')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.car_addon_add')}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/addons')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif




                        <div class="form-group row">
                            <label for="input_1" class="col-sm-3 col-form-label">{{trans('layout.car_addon_name_ar')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('ar_title', null, ['class'=>'form-control','id'=>'input_1']) }}
                                @if($errors->has('ar_title'))
                                <div class="alert alert-danger">{{$errors->first('ar_title')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.car_addon_name_en')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('en_title', null, ['class'=>'form-control','id'=>'en_title']) }}
                                @if($errors->has('en_title'))
                                <div class="alert alert-danger">{{$errors->first('en_title')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.car_addon_price')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('price', null, ['class'=>'form-control','id'=>'price']) }}
                                @if($errors->has('price'))
                                <div class="alert alert-danger">{{$errors->first('price')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.car_addon_details_ar')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::textarea('ar_details', null, ['class'=>'form-control','id'=>'ar_details']) }}
                                    @if($errors->has('ar_details'))
                                        <div class="alert alert-danger">{{$errors->first('ar_details')}}</div>
                                    @endif
                                </div>
                            </div>

                        <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.car_addon_details_en')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::textarea('en_details', null, ['class'=>'form-control','id'=>'en_details']) }}
                                    @if($errors->has('ar_description '))
                                        <div class="alert alert-danger">{{$errors->first('en_details')}}</div>
                                    @endif
                                </div>
                        </div>

                            <div class="form-group row">
                                <label for="input_3" class="col-sm-3 col-form-label">{{trans('layout.car_addon_image')}}</label>
                                <div class="col-sm-9">
                                    <input class="form-control" id="input_3" name="image" type="file">
                                </div>
                            </div>

                        
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.add')}}</button>
                <a href="{{URL::to('admin/addons/')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection


@section('scripts')
<script>
    $('#country_id').change(function () {
        var val = $(this).val();
        var base_url = $('#base-url').val();
        if (val == "") {
            val = 0;
        }
        $.ajax({
            type: "GET",
            url: base_url + "/admin/cities/" + val + "?from=city",
            success: function (data) {
                $('#cities').html(data);
            }
        });
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9uAy1z9dxw9aHx6_NJPRA0ih0UD-dCOU&callback=initMap&language=ar&region=EG"></script>
<script type="text/javascript">
    var marker;
    var lat;
    var lng;
    var map;
    function updateMarkerPosition(latLng) {
        document.getElementById('lat').value = latLng.lat();
        document.getElementById('lng').value = latLng.lng();
    }

    function placeMarker(location) {
        if (marker) {
            marker.setPosition(location);
        } else {
            marker = new google.maps.Marker({
                position: location,
                map: map
            });
        }
    }

    function initialize() {
        var lat = document.getElementById('lat').value;
        var lng = document.getElementById('lng').value;
        if (!lat && !lng) {
            var latLng = new google.maps.LatLng(24.598411724742483, 46.7138671875);
        } else {
            var latLng = new google.maps.LatLng(lat, lng);
        }

        map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 5,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        marker = new google.maps.Marker({
            position: latLng,
            map: map
        });
        marker.set(map);
        updateMarkerPosition(latLng);
        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
            updateMarkerPosition(event.latLng);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection