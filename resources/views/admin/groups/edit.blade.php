@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.groups')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/groups')}}"> {{trans('layout.groups')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.group_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.group_edit')}} : {{$group[$lang.'_name']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/groups/'.$group['id'])}}" method="post">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="input_1" class="col-sm-3 col-form-label">{{trans('layout.group_name_ar')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('ar_name', $group['ar_name'], ['class'=>'form-control','id'=>'input_1']) }}
                                @if($errors->has('ar_name'))
                                <div class="alert alert-danger">{{$errors->first('ar_name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.group_name_en')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('en_name', $group['en_name'], ['class'=>'form-control','id'=>'input_2']) }}
                                @if($errors->has('en_name'))
                                <div class="alert alert-danger">{{$errors->first('en_name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_3" class="col-sm-3 col-form-label">{{trans('layout.group_power')}}</label>
                            <div class="col-sm-9">
                                {{ Form::number('power', $group['power'], ['class'=>'form-control','id'=>'input_3']) }}
                                @if($errors->has('power'))
                                <div class="alert alert-danger">{{$errors->first('power')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_4" class="col-sm-3 col-form-label">{{trans('layout.group_country')}}</label>
                            <div class="col-sm-9">
                                {{ Form::select('country',['one'=>trans('layout.one_country') , 'all'=>trans('layout.all_country')], $group['country'], ['class'=>'form-control','id'=>'input_4']) }}
                                @if($errors->has('country'))
                                <div class="alert alert-danger">{{$errors->first('country')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_5" class="col-sm-3 col-form-label">{{trans('layout.permissions')}}</label>
                            <div class="col-sm-9">
                                <?php
                                $agents_categories = $branches = $branches_employees = $agents = $agents_employees = $suppliers = $rooms = $hotels = $room_views = $food_types = $room_features = $room_types = $parking = $facilities = $faq = $car = $offer = $bank = $office = $job = $cars_booking = $airport = $security = $car_city = $addon = $driver = $customer_service = $model = $brand = $market_place = $restaurants = $restaurant_category = $activity_type = $contact = $activities = $continent_country = $continent_cites = $continents = $slider = $groups = $site = $newsletter = $countries = $governates = $cities = $pages = $admins = $users = $visitors = FALSE;
                                foreach ($group['Permissions'] as $pre) {
                                    if ($pre['permission'] == "agents_categories") {
                                        $agents_categories = TRUE;
                                    } elseif ($pre['permission'] == "branches") {
                                        $branches = TRUE;
                                    } elseif ($pre['permission'] == "branches_employees") {
                                        $branches_employees = TRUE;
                                    } elseif ($pre['permission'] == "agents") {
                                        $agents = TRUE;
                                    } elseif ($pre['permission'] == "agents_employees") {
                                        $agents_employees = TRUE;
                                    } elseif ($pre['permission'] == "suppliers") {
                                        $suppliers = TRUE;
                                    } elseif ($pre['permission'] == "rooms") {
                                        $rooms = TRUE;
                                    } elseif ($pre['permission'] == "hotels") {
                                        $hotels = TRUE;
                                    } elseif ($pre['permission'] == "room_views") {
                                        $room_views = TRUE;
                                    } elseif ($pre['permission'] == "food_types") {
                                        $food_types = TRUE;
                                    } elseif ($pre['permission'] == "room_features") {
                                        $room_features = TRUE;
                                    } elseif ($pre['permission'] == "room_types") {
                                        $room_types = TRUE;
                                    } elseif ($pre['permission'] == "parking") {
                                        $parking = TRUE;
                                    } elseif ($pre['permission'] == "facilities") {
                                        $facilities = TRUE;
                                    } elseif ($pre['permission'] == "groups") {
                                        $groups = TRUE;
                                    } elseif ($pre['permission'] == "site") {
                                        $site = TRUE;
                                    } elseif ($pre['permission'] == "faq") {
                                        $faq = TRUE;
                                    } elseif ($pre['permission'] == "newsletter") {
                                        $newsletter = TRUE;
                                    } elseif ($pre['permission'] == "countries") {
                                        $countries = TRUE;
                                    } elseif ($pre['permission'] == "continents") {
                                        $continents = TRUE;
                                    } elseif ($pre['permission'] == "governates") {
                                        $governates = TRUE;
                                    } elseif ($pre['permission'] == "cities") {
                                        $cities = TRUE;
                                    } elseif ($pre['permission'] == "pages") {
                                        $pages = TRUE;
                                    } elseif ($pre['permission'] == "admins") {
                                        $admins = TRUE;
                                    } elseif ($pre['permission'] == "users") {
                                        $users = TRUE;
                                    } elseif ($pre['permission'] == "visitors") {
                                        $visitors = TRUE;
                                    } elseif ($pre['permission'] == "slider") {
                                        $slider = TRUE;
                                    } elseif ($pre['permission'] == "contact") {
                                        $contact = TRUE;
                                    } elseif ($pre['permission'] == "customer_service") {
                                        $customer_service = TRUE;
                                    } elseif ($pre['permission'] == "continent_countries") {
                                        $continent_country = TRUE;
                                    } elseif ($pre['permission'] == "continent_cites") {
                                        $continent_cites = TRUE;
                                    } elseif ($pre['permission'] == "market_place") {
                                        $market_place = TRUE;
                                    } elseif ($pre['permission'] == "restaurants") {
                                        $restaurants = TRUE;
                                    } elseif ($pre['permission'] == "restaurant_category") {
                                        $restaurant_category = TRUE;
                                    } elseif ($pre['permission'] == "activity_type") {
                                        $activity_type = TRUE;
                                    } elseif ($pre['permission'] == "activities") {
                                        $activities = TRUE;
                                    } elseif ($pre['permission'] == "airport") {
                                        $airport = TRUE;
                                    } elseif ($pre['permission'] == "driver") {
                                        $driver = TRUE;
                                    } elseif ($pre['permission'] == "addon") {
                                        $addon = TRUE;
                                    } elseif ($pre['permission'] == "security") {
                                        $security = TRUE;
                                    } elseif ($pre['permission'] == "model") {
                                        $model = TRUE;
                                    } elseif ($pre['permission'] == "brand") {
                                        $brand = TRUE;
                                    } elseif ($pre['permission'] == "car") {
                                        $car = TRUE;
                                    } elseif ($pre['permission'] == "car_city") {
                                        $car_city = TRUE;
                                    } elseif ($pre['permission'] == "offer") {
                                        $offer = TRUE;
                                    } elseif ($pre['permission'] == "cars_booking") {
                                        $cars_booking = TRUE;
                                    } elseif ($pre['permission'] == "bank") {
                                        $bank = TRUE;
                                    } elseif ($pre['permission'] == "job") {
                                        $job = TRUE;
                                    } elseif ($pre['permission'] == "office") {
                                        $office = TRUE;
                                    }
                                }
                                ?>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','groups',$groups,['id'=>'Checkbox_1']) }}
                                    <label for="Checkbox_1">{{trans('layout.groups')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','site',$site,['id'=>'Checkbox_2']) }}
                                    <label for="Checkbox_2">{{trans('layout.site')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','newsletter',$newsletter,['id'=>'Checkbox_3']) }}
                                    <label for="Checkbox_3">{{trans('layout.newsletter')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','countries',$countries,['id'=>'Checkbox_4']) }}
                                    <label for="Checkbox_4">{{trans('layout.countries')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','governates',$governates,['id'=>'Checkbox_5']) }}
                                    <label for="Checkbox_5">{{trans('layout.governates')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','cities',$cities,['id'=>'Checkbox_6']) }}
                                    <label for="Checkbox_6">{{trans('layout.cities')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','pages',$pages,['id'=>'Checkbox_7']) }}
                                    <label for="Checkbox_7">{{trans('layout.pages')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','faq',$faq,['id'=>'Checkbox_24']) }}
                                    <label for="Checkbox_24">{{trans('layout.faq')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','admins',$admins,['id'=>'Checkbox_8']) }}
                                    <label for="Checkbox_8">{{trans('layout.admins')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','users',$users,['id'=>'Checkbox_9']) }}
                                    <label for="Checkbox_9">{{trans('layout.users')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','visitors',$visitors,['id'=>'Checkbox_10']) }}
                                    <label for="Checkbox_10">{{trans('layout.visitors')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','slider',$slider,['id'=>'Checkbox_11']) }}
                                    <label for="Checkbox_11">{{trans('layout.slider')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','contact',$contact,['id'=>'Checkbox_12']) }}
                                    <label for="Checkbox_12">{{trans('layout.contact')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','customer_service',$customer_service,['id'=>'Checkbox_14']) }}
                                    <label for="Checkbox_14">{{trans('layout.customer_service')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','continents',$continents,['id'=>'Checkbox_15']) }}
                                    <label for="Checkbox_15">{{trans('layout.continents')}}</label>
                                </div>


                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','activities',$activities,['id'=>'Checkbox_18']) }}
                                    <label for="Checkbox_18">{{trans('layout.activities')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','activity_type',$activity_type,['id'=>'Checkbox_20']) }}
                                    <label for="Checkbox_20">{{trans('layout.activity_types')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','restaurants',$restaurants,['id'=>'Checkbox_19']) }}
                                    <label for="Checkbox_19">{{trans('layout.restaurants')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','restaurant_category',$restaurant_category,['id'=>'Checkbox_21']) }}
                                    <label for="Checkbox_21">{{trans('layout.restaurant_category')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','market_place',$market_place,['id'=>'Checkbox_22']) }}
                                    <label for="Checkbox_22">{{trans('layout.market_place')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','car',$car,['id'=>'Checkbox_23']) }}
                                    <label for="Checkbox_23">{{trans('layout.cars')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','brand',$brand,['id'=>'Checkbox_28']) }}
                                    <label for="Checkbox_28">{{trans('layout.car_brand')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','model',$model,['id'=>'Checkbox_29']) }}
                                    <label for="Checkbox_29">{{trans('layout.car_models')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','airport',$airport,['id'=>'Checkbox_30']) }}
                                    <label for="Checkbox_30">{{trans('layout.car_airport')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','driver',$driver,['id'=>'Checkbox_31']) }}
                                    <label for="Checkbox_31">{{trans('layout.car_driver')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','addon',$addon,['id'=>'Checkbox_32']) }}
                                    <label for="Checkbox_32">{{trans('layout.car_addon')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','security',$security,['id'=>'Checkbox_33']) }}
                                    <label for="Checkbox_33">{{trans('layout.car_security')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','car_city',$car_city,['id'=>'Checkbox_34']) }}
                                    <label for="Checkbox_34">{{trans('layout.car_city')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','offer',$offer,['id'=>'Checkbox_35']) }}
                                    <label for="Checkbox_35">{{trans('layout.car_offer')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','cars_booking',$cars_booking,['id'=>'Checkbox_36']) }}
                                    <label for="Checkbox_36">{{trans('layout.car_booking')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','bank',$bank,['id'=>'Checkbox_37']) }}
                                    <label for="Checkbox_37">{{trans('layout.banks')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','job',$job,['id'=>'Checkbox_38']) }}
                                    <label for="Checkbox_38">{{trans('layout.jobs')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','office',$office,['id'=>'Checkbox_39']) }}
                                    <label for="Checkbox_39">{{trans('layout.offices')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','facilities',$facilities,['id'=>'Checkbox_40']) }}
                                    <label for="Checkbox_40">{{trans('layout.facilities')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','parking',$parking,['id'=>'Checkbox_41']) }}
                                    <label for="Checkbox_41">{{trans('layout.parking')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','room_types',$room_types,['id'=>'Checkbox_42']) }}
                                    <label for="Checkbox_42">{{trans('layout.room_types')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','room_features',$room_features,['id'=>'Checkbox_43']) }}
                                    <label for="Checkbox_43">{{trans('layout.room_features')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','food_types',$food_types,['id'=>'Checkbox_44']) }}
                                    <label for="Checkbox_44">{{trans('layout.food_types')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','room_views',$room_views,['id'=>'Checkbox_45']) }}
                                    <label for="Checkbox_45">{{trans('layout.room_views')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','hotels',$hotels,['id'=>'Checkbox_46']) }}
                                    <label for="Checkbox_46">{{trans('layout.hotels')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','rooms',$rooms,['id'=>'Checkbox_47']) }}
                                    <label for="Checkbox_47">{{trans('layout.rooms')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','suppliers',$suppliers,['id'=>'Checkbox_48']) }}
                                    <label for="Checkbox_48">{{trans('layout.suppliers')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','branches',$branches,['id'=>'Checkbox_49']) }}
                                    <label for="Checkbox_49">{{trans('layout.branches')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','branches_employees',$branches_employees,['id'=>'Checkbox_50']) }}
                                    <label for="Checkbox_50">{{trans('layout.branches_employees')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','agents',$agents,['id'=>'Checkbox_51']) }}
                                    <label for="Checkbox_51">{{trans('layout.agents')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','agents_employees',$agents_employees,['id'=>'Checkbox_52']) }}
                                    <label for="Checkbox_52">{{trans('layout.agents_employees')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','agents_categories',$agents_categories,['id'=>'Checkbox_53']) }}
                                    <label for="Checkbox_53">{{trans('layout.agents_categories')}}</label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/groups')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection