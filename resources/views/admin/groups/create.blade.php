@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.groups')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/groups')}}"> {{trans('layout.groups')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.group_add')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.group_add')}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/groups')}}" method="post" >
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="input_1" class="col-sm-3 col-form-label">{{trans('layout.group_name_ar')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('ar_name', null, ['class'=>'form-control','id'=>'input_1']) }}
                                @if($errors->has('ar_name'))
                                <div class="alert alert-danger">{{$errors->first('ar_name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.group_name_en')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('en_name', null, ['class'=>'form-control','id'=>'input_2']) }}
                                @if($errors->has('en_name'))
                                <div class="alert alert-danger">{{$errors->first('en_name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_3" class="col-sm-3 col-form-label">{{trans('layout.group_power')}}</label>
                            <div class="col-sm-9">
                                {{ Form::number('power', null, ['class'=>'form-control','id'=>'input_3']) }}
                                @if($errors->has('power'))
                                <div class="alert alert-danger">{{$errors->first('power')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_4" class="col-sm-3 col-form-label">{{trans('layout.group_country')}}</label>
                            <div class="col-sm-9">
                                {{ Form::select('country',['one'=>trans('layout.one_country') , 'all'=>trans('layout.all_country')], null, ['class'=>'form-control','id'=>'input_4']) }}
                                @if($errors->has('country'))
                                <div class="alert alert-danger">{{$errors->first('country')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_5" class="col-sm-3 col-form-label">{{trans('layout.permissions')}}</label>
                            <div class="col-sm-9">
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','groups',false,['id'=>'Checkbox_1']) }}
                                    <label for="Checkbox_1">{{trans('layout.groups')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','site',false,['id'=>'Checkbox_2']) }}
                                    <label for="Checkbox_2">{{trans('layout.site')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','newsletter',false,['id'=>'Checkbox_3']) }}
                                    <label for="Checkbox_3">{{trans('layout.newsletter')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','countries',false,['id'=>'Checkbox_4']) }}
                                    <label for="Checkbox_4">{{trans('layout.countries')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','governates',false,['id'=>'Checkbox_5']) }}
                                    <label for="Checkbox_5">{{trans('layout.governates')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','cities',false,['id'=>'Checkbox_6']) }}
                                    <label for="Checkbox_6">{{trans('layout.cities')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','pages',false,['id'=>'Checkbox_7']) }}
                                    <label for="Checkbox_7">{{trans('layout.pages')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','faq',false,['id'=>'Checkbox_24']) }}
                                    <label for="Checkbox_24">{{trans('layout.faq')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','admins',false,['id'=>'Checkbox_8']) }}
                                    <label for="Checkbox_8">{{trans('layout.admins')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','users',false,['id'=>'Checkbox_9']) }}
                                    <label for="Checkbox_9">{{trans('layout.users')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','visitors',false,['id'=>'Checkbox_10']) }}
                                    <label for="Checkbox_10">{{trans('layout.visitors')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','slider',false,['id'=>'Checkbox_11']) }}
                                    <label for="Checkbox_11">{{trans('layout.slider')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','contact',false,['id'=>'Checkbox_12']) }}
                                    <label for="Checkbox_12">{{trans('layout.contact')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','customer_service',false,['id'=>'Checkbox_14']) }}
                                    <label for="Checkbox_14">{{trans('layout.customer_service')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','continents',false,['id'=>'Checkbox_15']) }}
                                    <label for="Checkbox_15">{{trans('layout.continents')}}</label>
                                </div>

                        
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','activities',false,['id'=>'Checkbox_18']) }}
                                    <label for="Checkbox_18">{{trans('layout.activities')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','activity_type',false,['id'=>'Checkbox_20']) }}
                                    <label for="Checkbox_20">{{trans('layout.activity_types')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','restaurants',false,['id'=>'Checkbox_19']) }}
                                    <label for="Checkbox_19">{{trans('layout.restaurants')}}</label>
                                </div>
                                
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','restaurant_category',false,['id'=>'Checkbox_21']) }}
                                    <label for="Checkbox_21">{{trans('layout.restaurant_category')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','market_place',false,['id'=>'Checkbox_22']) }}
                                    <label for="Checkbox_22">{{trans('layout.market_place')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','car',false,['id'=>'Checkbox_23']) }}
                                    <label for="Checkbox_23">{{trans('layout.cars')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','brand',false,['id'=>'Checkbox_28']) }}
                                    <label for="Checkbox_28">{{trans('layout.car_brand')}}</label>
                                </div>
                                
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','model',false,['id'=>'Checkbox_29']) }}
                                    <label for="Checkbox_29">{{trans('layout.car_models')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','airport',false,['id'=>'Checkbox_30']) }}
                                    <label for="Checkbox_30">{{trans('layout.car_airport')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','driver',false,['id'=>'Checkbox_31']) }}
                                    <label for="Checkbox_31">{{trans('layout.car_driver')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','addon',false,['id'=>'Checkbox_32']) }}
                                    <label for="Checkbox_32">{{trans('layout.car_addon')}}</label>
                                </div>

                                 <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','security',false,['id'=>'Checkbox_33']) }}
                                    <label for="Checkbox_33">{{trans('layout.car_security')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','car_city',false,['id'=>'Checkbox_34']) }}
                                    <label for="Checkbox_34">{{trans('layout.car_city')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','offer',false,['id'=>'Checkbox_35']) }}
                                    <label for="Checkbox_35">{{trans('layout.car_offer')}}</label>
                                </div>
                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','cars_booking',false,['id'=>'Checkbox_36']) }}
                                    <label for="Checkbox_36">{{trans('layout.car_booking')}}</label>
                                </div>

                                  <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','bank',false,['id'=>'Checkbox_37']) }}
                                    <label for="Checkbox_37">{{trans('layout.banks')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','job',false,['id'=>'Checkbox_38']) }}
                                    <label for="Checkbox_38">{{trans('layout.jobs')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','office',false,['id'=>'Checkbox_39']) }}
                                    <label for="Checkbox_39">{{trans('layout.offices')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','facilities',false,['id'=>'Checkbox_40']) }}
                                    <label for="Checkbox_40">{{trans('layout.facilities')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','parking',false,['id'=>'Checkbox_41']) }}
                                    <label for="Checkbox_41">{{trans('layout.parking')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','room_types',false,['id'=>'Checkbox_42']) }}
                                    <label for="Checkbox_42">{{trans('layout.room_types')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','room_features',false,['id'=>'Checkbox_43']) }}
                                    <label for="Checkbox_43">{{trans('layout.room_features')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','food_types',false,['id'=>'Checkbox_44']) }}
                                    <label for="Checkbox_44">{{trans('layout.food_types')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','room_views',false,['id'=>'Checkbox_45']) }}
                                    <label for="Checkbox_45">{{trans('layout.room_views')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','hotels',false,['id'=>'Checkbox_46']) }}
                                    <label for="Checkbox_46">{{trans('layout.hotels')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','rooms',false,['id'=>'Checkbox_47']) }}
                                    <label for="Checkbox_47">{{trans('layout.rooms')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','suppliers',false,['id'=>'Checkbox_48']) }}
                                    <label for="Checkbox_48">{{trans('layout.suppliers')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','branches',false,['id'=>'Checkbox_49']) }}
                                    <label for="Checkbox_49">{{trans('layout.branches')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','branches_employees',false,['id'=>'Checkbox_50']) }}
                                    <label for="Checkbox_50">{{trans('layout.branches_employees')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','agents',false,['id'=>'Checkbox_51']) }}
                                    <label for="Checkbox_51">{{trans('layout.agents')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','agents_employees',false,['id'=>'Checkbox_52']) }}
                                    <label for="Checkbox_52">{{trans('layout.agents_employees')}}</label>
                                </div>

                                <div class="col-sm-4 {{$pull}}">
                                    {{ Form::checkbox('permissions[]','agents_categories',false,['id'=>'Checkbox_53']) }}
                                    <label for="Checkbox_53">{{trans('layout.agents_categories')}}</label>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.add')}}</button>
                <a href="{{URL::to('admin/groups')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection