@extends('admin.layouts.index')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.admin')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{URL::to('admin')}}"><i class="fa fa-home"></i> {{trans('layout.home')}}</a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    
</section>
<!-- /.content -->

@endsection


@section('scripts')
@endsection