@extends('admin.layouts.table')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.visitors')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.visitors')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            @if (Session::has('message'))
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissible">
                        {{ Session::get('message')}}
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    </div>
                </div>
            </div>
            @endif

            <div class="box">
                <div class="box-body">
                    <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive">
                        <thead>
                            <tr>
                                <th class="{{$text}}">{{trans('layout.name')}}</th>
                                <th class="{{$text}}">{{trans('layout.country')}}</th>
                                <th class="{{$text}}">{{trans('layout.phone')}}</th>
                                <th class="{{$text}}">{{trans('layout.email')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($visitors as $visitor)
                            @if(Auth::User()->Group['country'] == "all")
                            <tr>
                                <td>{{$visitor['name']}}</td>
                                <td>{{$visitor['Country'][$lang.'_name']}}</td>
                                <td>{{$visitor['phone']}}</td>
                                <td>{{$visitor['email']}}</td>
                            </tr>
                            @else
                            @if(Auth::User()->country_id == $visitor['country_id'])
                            <tr>
                                <td>{{$visitor['name']}}</td>
                                <td>{{$visitor['Country'][$lang.'_name']}}</td>
                                <td>{{$visitor['phone']}}</td>
                                <td>{{$visitor['email']}}</td>
                            </tr>
                            @endif
                            @endif
                            @endforeach
                        </tbody>
                    </table>


                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->          
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection