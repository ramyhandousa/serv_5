@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.branches')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/branches')}}"> {{trans('layout.branches')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.branch_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.branch_edit')}} : {{$branch[$lang.'_name']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/branches/'.$branch['id'])}}" method="post" enctype="multipart/form-data">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="input_0" class="col-sm-3 col-form-label">{{trans('layout.branch_active')}}</label>
                            <div class="col-sm-9">
                                {{ Form::select('active', ['yes'=>trans('layout.yes_active') , 'no'=>trans('layout.no_active')],$branch['active'], ['class'=>'form-control','id'=>'input_0']) }}
                                @if($errors->has('active'))
                                <div class="alert alert-danger">{{$errors->first('active')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="country_id" class="col-sm-3 col-form-label">{{trans('layout.country')}}</label>
                            <div class="col-sm-9">
                                {{ Form::select('country_id', $countries,$branch['country_id'], ['class'=>'form-control select2','id'=>'country_id']) }}
                                @if($errors->has('country_id'))
                                <div class="alert alert-danger">{{$errors->first('country_id')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="governate_id" class="col-sm-3 col-form-label">{{trans('layout.governate')}}</label>
                            <div class="col-sm-9">
                                <div id="governates">
                                    {{ Form::select('governate_id',$governates, $branch['governate_id'], ['class'=>'form-control select2','id'=>'governate_id']) }}
                                </div>
                                @if($errors->has('governate_id'))
                                <div class="alert alert-danger">{{$errors->first('governate_id')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="city_id" class="col-sm-3 col-form-label">{{trans('layout.city')}}</label>
                            <div class="col-sm-9">
                                <div id="cities">
                                    {{ Form::select('city_id',$cities, $branch['city_id'], ['class'=>'form-control select2','id'=>'city_id']) }}
                                </div>
                                @if($errors->has('city_id'))
                                <div class="alert alert-danger">{{$errors->first('city_id')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_11" class="col-sm-3 col-form-label">{{trans('layout.branch_name_ar')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('ar_name', $branch['ar_name'], ['class'=>'form-control','id'=>'input_11']) }}
                                @if($errors->has('ar_name'))
                                <div class="alert alert-danger">{{$errors->first('ar_name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_22" class="col-sm-3 col-form-label">{{trans('layout.branch_name_en')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('en_name', $branch['en_name'], ['class'=>'form-control','id'=>'input_22']) }}
                                @if($errors->has('en_name'))
                                <div class="alert alert-danger">{{$errors->first('en_name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_3" class="col-sm-3 col-form-label">{{trans('layout.phone')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('phone', $branch['phone'], ['class'=>'form-control','id'=>'input_3']) }}
                                @if($errors->has('phone'))
                                <div class="alert alert-danger">{{$errors->first('phone')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_4" class="col-sm-3 col-form-label">{{trans('layout.mobile')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('mobile', $branch['mobile'], ['class'=>'form-control','id'=>'input_4']) }}
                                @if($errors->has('mobile'))
                                <div class="alert alert-danger">{{$errors->first('mobile')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ar_address" class="col-sm-3 col-form-label">{{trans('layout.ar_address')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('ar_address', $branch['ar_address'], ['class'=>'form-control','id'=>'ar_address']) }}
                                @if($errors->has('ar_address'))
                                <div class="alert alert-danger">{{$errors->first('ar_address')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="en_address" class="col-sm-3 col-form-label">{{trans('layout.en_address')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('en_address', $branch['en_address'], ['class'=>'form-control','id'=>'en_address']) }}
                                @if($errors->has('en_address'))
                                <div class="alert alert-danger">{{$errors->first('en_address')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_5" class="col-sm-3 col-form-label">{{trans('layout.map')}}</label>
                            <div class="col-sm-9">
                                <div id="mapCanvas" style="height:450px;" class="col-md-12"></div>
                                <input type="hidden" value="{{$branch['lat']}}" id="lat" name="lat" />
                                <input type="hidden" value="{{$branch['lng']}}" id="lng" name="lng" />
                                @if($errors->has('lat'))
                                <div class="alert alert-danger">{{$errors->first('lat')}}</div>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/branches')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection


@section('scripts')
<script>
    $('#country_id').change(function () {
        var val = $(this).val();
        var base_url = $('#base-url').val();
        if (val == "") {
            val = 0;
        }
        $.ajax({
            type: "GET",
            url: base_url + "/admin/get_governates/" + val + '?branch_id = <?php echo $branch['id']; ?>',
            success: function (data) {
                $('#governates').html(data);
            }
        });
    });

    $('#governate_id').change(function () {
        var val = $(this).val();
        var base_url = $('#base-url').val();
        if (val == "") {
            val = 0;
        }
        $.ajax({
            type: "GET",
            url: base_url + "/admin/get_cities/" + val + '?branch_id = <?php echo $branch['id']; ?>',
            success: function (data) {
                $('#cities').html(data);
            }
        });
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9uAy1z9dxw9aHx6_NJPRA0ih0UD-dCOU&callback=initMap&language=ar&region=EG"></script>
<script type="text/javascript">
    var marker;
    var lat;
    var lng;
    var map;
    function updateMarkerPosition(latLng) {
        document.getElementById('lat').value = latLng.lat();
        document.getElementById('lng').value = latLng.lng();
    }

    function placeMarker(location) {
        if (marker) {
            marker.setPosition(location);
        } else {
            marker = new google.maps.Marker({
                position: location,
                map: map
            });
        }
    }

    function initialize() {
        var lat = document.getElementById('lat').value;
        var lng = document.getElementById('lng').value;
        if (!lat && !lng) {
            var latLng = new google.maps.LatLng(24.598411724742483, 46.7138671875);
        } else {
            var latLng = new google.maps.LatLng(lat, lng);
        }

        map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 5,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        marker = new google.maps.Marker({
            position: latLng,
            map: map
        });
        marker.set(map);
        updateMarkerPosition(latLng);
        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
            updateMarkerPosition(event.latLng);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection
