@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.countries')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/countries')}}"> {{trans('layout.countries')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.country_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.country_edit')}} : {{$country[$lang.'_name']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/countries/'.$country['id'])}}" method="post" enctype="multipart/form-data">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="input_0" class="col-sm-3 col-form-label">{{trans('layout.country_active')}}</label>
                            <div class="col-sm-9">
                                {{ Form::select('active', ['yes'=>trans('layout.yes_active') , 'no'=>trans('layout.no_active')],$country['active'], ['class'=>'form-control','id'=>'input_0']) }}
                                @if($errors->has('active'))
                                <div class="alert alert-danger">{{$errors->first('active')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_1" class="col-sm-3 col-form-label">{{trans('layout.country_name_ar')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('ar_name', $country['ar_name'], ['class'=>'form-control','id'=>'input_1']) }}
                                @if($errors->has('ar_name'))
                                <div class="alert alert-danger">{{$errors->first('ar_name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.country_name_en')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('en_name', $country['en_name'], ['class'=>'form-control','id'=>'input_2']) }}
                                @if($errors->has('en_name'))
                                <div class="alert alert-danger">{{$errors->first('en_name')}}</div>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.country_description_ar')}}</label>
                            <div class="col-sm-9">
                                {{ Form::textarea('ar_description', $country['ar_description'], ['class'=>'form-control','id'=>'ar_description']) }}
                                @if($errors->has('ar_description'))
                                <div class="alert alert-danger">{{$errors->first('ar_description')}}</div>
                                @endif
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.country_description_en')}}</label>
                            <div class="col-sm-9">
                                {{ Form::textarea('en_description', $country['en_description'], ['class'=>'form-control','id'=>'en_description']) }}
                                @if($errors->has('en_description'))
                                <div class="alert alert-danger">{{$errors->first('en_description')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_7" class="col-sm-3 col-form-label">{{trans('layout.country_code')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('code', $country['code'], ['class'=>'form-control','id'=>'input_7']) }}
                                @if($errors->has('code'))
                                <div class="alert alert-danger">{{$errors->first('code')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_4" class="col-sm-3 col-form-label">{{trans('layout.country_currency_ar')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('ar_currency', $country['ar_currency'], ['class'=>'form-control','id'=>'input_4']) }}
                                @if($errors->has('ar_currency'))
                                <div class="alert alert-danger">{{$errors->first('ar_currency')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_5" class="col-sm-3 col-form-label">{{trans('layout.country_currency_en')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('en_currency', $country['en_currency'], ['class'=>'form-control','id'=>'input_5']) }}
                                @if($errors->has('en_currency'))
                                <div class="alert alert-danger">{{$errors->first('en_currency')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_6" class="col-sm-3 col-form-label">{{trans('layout.translate')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('iso_code', $country['iso_code'], ['class'=>'form-control','id'=>'input_6']) }}
                                @if($errors->has('iso_code'))
                                <div class="alert alert-danger">{{$errors->first('iso_code')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_3" class="col-sm-3 col-form-label">{{trans('layout.country_image')}}</label>
                            <div class="col-sm-9">
                                {{ Form::file('image',['class'=>'form-control','id'=>'input_3']) }}
                                @if($errors->has('image'))
                                <div class="alert alert-danger">{{$errors->first('image')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_3" class="col-sm-3 col-form-label">{{trans('layout.country_image_current')}}</label>
                            <div class="col-sm-9">
                                <img src="{{URL::to('upload/countries/'.$country['image'])}}" class="img-thumbnail">
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/countries')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection