@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.jobs')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/job')}}"> {{trans('layout.jobs')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.job_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.job_edit')}} : {{$job[$lang.'_name']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

       

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif
                    
                   

                    <div class="form-group row">
                        <label class="col-sm-3 control-label">الإسم</label>
                        <div class="col-sm-9">
                            {{Form::text('name',$job->name,['readonly','class'=>'form-control']) }}
                            {{ $errors->first('name','<div class="alert alert-danger">:message</div>')}}
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-sm-3 control-label">رقم الهاتف</label>
                        <div class="col-sm-9">
                            {{Form::text('phone',$job->phone,['readonly','class'=>'form-control']) }}
                            {{ $errors->first('phone','<div class="alert alert-danger">:message</div>')}}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 control-label">البريد الإلكترونى</label>
                        <div class="col-sm-9">
                            {{Form::text('email',$job->email,['readonly','class'=>'form-control']) }}
                            {{ $errors->first('email','<div class="alert alert-danger">:message</div>')}}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 control-label">الملاحظات</label>
                        <div class="col-sm-9">
                            {{Form::textarea('notes',$job->notes,['readonly','class'=>'form-control']) }}
                            {{ $errors->first('notes','<div class="alert alert-danger">:message</div>')}}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 control-label">السيرة الذاتية</label>
                        <div class="col-sm-9">
                            <a href="{{URL::to('images/cv/'.$job->url)}}" class="btn btn-primary"><i class="fa fa-save"></i> تحميل</a>
                        </div>
                    </div>
                    
                </div>
            </div>

            <div class="box-footer">
                <a href="{{URL::to('admin/job')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        

    </div>

</section>

@endsection
@section('scripts')
<script>
    $('#country_id').change(function () {
        var val = $(this).val();
        var base_url = $('#base-url').val();
        if (val == "") {
            val = 0;
        }
        $.ajax({
            type: "GET",
            url: base_url + "/admin/cities/" + val + "?from=city",
            success: function (data) {
                $('#cities').html(data);
            }
        });
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9uAy1z9dxw9aHx6_NJPRA0ih0UD-dCOU&callback=initMap&language=ar&region=EG"></script>
<script type="text/javascript">
    var marker;
    var lat;
    var lng;
    var map;
    function updateMarkerPosition(latLng) {
        document.getElementById('lat').value = latLng.lat();
        document.getElementById('lng').value = latLng.lng();
    }

    function placeMarker(location) {
        if (marker) {
            marker.setPosition(location);
        } else {
            marker = new google.maps.Marker({
                position: location,
                map: map
            });
        }
    }

    function initialize() {
        var lat = document.getElementById('lat').value;
        var lng = document.getElementById('lng').value;
        if (!lat && !lng) {
            var latLng = new google.maps.LatLng(24.598411724742483, 46.7138671875);
        } else {
            var latLng = new google.maps.LatLng(lat, lng);
        }

        map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 5,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        marker = new google.maps.Marker({
            position: latLng,
            map: map
        });
        marker.set(map);
        updateMarkerPosition(latLng);
        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
            updateMarkerPosition(event.latLng);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection