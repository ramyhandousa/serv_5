@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.cities')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/continents')}}"> {{trans('layout.continent')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.continent_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.continent_edit')}} : {{$continent[$lang.'_name']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/continents/'.$continent['id'])}}" method="post" enctype="multipart/form-data">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif
                            <div class="form-group row">
                                <label for="input_0" class="col-sm-3 col-form-label">{{trans('layout.continent_active')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::select('active', ['yes'=>trans('layout.yes_active') , 'no'=>trans('layout.no_active')],$continent['active'], ['class'=>'form-control','id'=>'input_0']) }}
                                    @if($errors->has('active'))
                                        <div class="alert alert-danger">{{$errors->first('active')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_1" class="col-sm-3 col-form-label">{{trans('layout.continent_name_ar')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('ar_name', $continent['ar_name'], ['class'=>'form-control','id'=>'input_1']) }}
                                    @if($errors->has('ar_name'))
                                        <div class="alert alert-danger">{{$errors->first('ar_name')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.continent_name_en')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('en_name', $continent['en_name'], ['class'=>'form-control','id'=>'input_2']) }}
                                    @if($errors->has('en_name'))
                                        <div class="alert alert-danger">{{$errors->first('en_name')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.continent_code')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('code',  $continent['code'], ['class'=>'form-control','id'=>'input_2']) }}
                                    @if($errors->has('code'))
                                        <div class="alert alert-danger">{{$errors->first('code')}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.continent_description_ar')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('ar_description',  $continent['ar_description'], ['class'=>'form-control','id'=>'input_2']) }}
                                    @if($errors->has('ar_description'))
                                        <div class="alert alert-danger">{{$errors->first('en_name')}}</div>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.continent_description_en')}}</label>
                                <div class="col-sm-9">
                                    {{ Form::text('en_description', $continent['en_description'], ['class'=>'form-control','id'=>'input_2']) }}
                                    @if($errors->has('ar_description '))
                                        <div class="alert alert-danger">{{$errors->first('en_name')}}</div>
                                    @endif
                                </div>
                            </div>



                            <div class="form-group row">
                                <label for="input_3" class="col-sm-3 col-form-label">{{trans('layout.continent_image')}}</label>
                                <div class="col-sm-9 ">
                                    <img class="mb-4" src="{{asset('/upload/continents/').'/'.$continent['image']}}" width="100px">
                                    <input class="form-control" id="input_3" name="image" type="file">
                                </div>
                            </div>



                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/cities')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection


@section('scripts')
<script>
    $('#country_id').change(function () {
        var val = $(this).val();
        var base_url = $('#base-url').val();
        if (val == "") {
            val = 0;
        }
        $.ajax({
            type: "GET",
            url: base_url + "/admin/cities/" + val + "?from=city",
            success: function (data) {
                $('#cities').html(data);
            }
        });
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9uAy1z9dxw9aHx6_NJPRA0ih0UD-dCOU&callback=initMap&language=ar&region=EG"></script>
<script type="text/javascript">
    var marker;
    var lat;
    var lng;
    var map;
    function updateMarkerPosition(latLng) {
        document.getElementById('lat').value = latLng.lat();
        document.getElementById('lng').value = latLng.lng();
    }

    function placeMarker(location) {
        if (marker) {
            marker.setPosition(location);
        } else {
            marker = new google.maps.Marker({
                position: location,
                map: map
            });
        }
    }

    function initialize() {
        var lat = document.getElementById('lat').value;
        var lng = document.getElementById('lng').value;
        if (!lat && !lng) {
            var latLng = new google.maps.LatLng(24.598411724742483, 46.7138671875);
        } else {
            var latLng = new google.maps.LatLng(lat, lng);
        }

        map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 5,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        marker = new google.maps.Marker({
            position: latLng,
            map: map
        });
        marker.set(map);
        updateMarkerPosition(latLng);
        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
            updateMarkerPosition(event.latLng);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection