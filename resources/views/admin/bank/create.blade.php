@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.banks')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/bank')}}"> {{trans('layout.banks')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.bank_add')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.bank_add')}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/bank')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif

                    <div class="form-group row">
                        <label class="col-sm-3 control-label">البنك بالعربى</label>
                        <div class="col-sm-9">
                            {{Form::text('ar_title',null,['class'=>'form-control']) }}
                            @if($errors->has('ar_title'))
                                <div class="alert alert-danger">{{$errors->first('ar_title')}}</div>
                            @endif
                        </div>
                    </div>


                    <div class="form-group row">
                        <label class="col-sm-3 control-label">البنك بالإنجليزى</label>
                        <div class="col-sm-9">
                            {{Form::text('en_title',null,['class'=>'form-control']) }}
                            @if($errors->has('en_title'))
                                <div class="alert alert-danger">{{$errors->first('en_title')}}</div>
                            @endif
                        </div>
                    </div>


                   

                    <div class="form-group row">
                        <label class="col-sm-3 control-label">دولة الحساب</label>
                        <div class="col-sm-9">
                            {{Form::select('country_id',$countries,null,['class'=>'form-control']) }}
                            @if($errors->has('country_id'))
                                <div class="alert alert-danger">{{$errors->first('country_id')}}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 control-label">صاحب الحساب</label>
                        <div class="col-sm-9">
                            {{Form::text('owner',null,['class'=>'form-control']) }}
                            @if($errors->has('owner'))
                                <div class="alert alert-danger">{{$errors->first('owner')}}</div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 control-label">رقم الحساب</label>
                        <div class="col-sm-9">
                            {{Form::text('number',null,['class'=>'form-control']) }}
                            @if($errors->has('number'))
                                <div class="alert alert-danger">{{$errors->first('number')}}</div>
                                @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="control-label col-md-3">شعار البنك  </label>
                        <div class="col-md-9">
                            {{Form::file('image',array('class'=>'form-control','style'=>'min-height:50px;'))}}
                            @if($errors->has('image'))
                                <div class="alert alert-danger">{{$errors->first('image')}}</div>
                                @endif
                                                        </div>
                    </div>

                       
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.add')}}</button>
                <a href="{{URL::to('admin/countries')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection