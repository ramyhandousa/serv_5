@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.site')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.site_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.site_edit')}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/site/1')}}" method="post">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label for="input_1" class="col-sm-2 col-form-label">{{trans('layout.site_title_ar')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('ar_title',  $site['ar_title'], ['class'=>'form-control','id'=>'input_1']) }}
                                @if($errors->has('ar_title'))
                                <div class="alert alert-danger">{{$errors->first('ar_title')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_2" class="col-sm-2 col-form-label">{{trans('layout.site_title_en')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('en_title',  $site['en_title'], ['class'=>'form-control','id'=>'input_2']) }}
                                @if($errors->has('en_title'))
                                <div class="alert alert-danger">{{$errors->first('en_title')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_3" class="col-sm-2 col-form-label">{{trans('layout.site_desc_ar')}}</label>
                            <div class="col-sm-10">
                                {{ Form::textarea('ar_desc',  $site['ar_desc'], ['class'=>'form-control','id'=>'input_3','rows'=>3]) }}
                                @if($errors->has('ar_desc'))
                                <div class="alert alert-danger">{{$errors->first('ar_desc')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_4" class="col-sm-2 col-form-label">{{trans('layout.site_desc_en')}}</label>
                            <div class="col-sm-10">
                                {{ Form::textarea('en_desc',  $site['en_desc'], ['class'=>'form-control','id'=>'input_4','rows'=>3]) }}
                                @if($errors->has('en_desc'))
                                <div class="alert alert-danger">{{$errors->first('en_desc')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_5" class="col-sm-2 col-form-label">{{trans('layout.site_tags')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('tags',  $site['tags'], ['class'=>'form-control','id'=>'input_5']) }}
                                @if($errors->has('tags'))
                                <div class="alert alert-danger">{{$errors->first('tags')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_6" class="col-sm-2 col-form-label">{{trans('layout.phone')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('phone',  $site['phone'], ['class'=>'form-control','id'=>'input_6']) }}
                                @if($errors->has('phone'))
                                <div class="alert alert-danger">{{$errors->first('phone')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_7" class="col-sm-2 col-form-label">{{trans('interface.customer_service')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('mobile',  $site['mobile'], ['class'=>'form-control','id'=>'input_7']) }}
                                @if($errors->has('mobile'))
                                <div class="alert alert-danger">{{$errors->first('mobile')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_8" class="col-sm-2 col-form-label">{{trans('layout.fax')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('fax',  $site['fax'], ['class'=>'form-control','id'=>'input_8']) }}
                                @if($errors->has('fax'))
                                <div class="alert alert-danger">{{$errors->first('fax')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_9" class="col-sm-2 col-form-label">{{trans('layout.email')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('email',  $site['email'], ['class'=>'form-control','id'=>'input_9']) }}
                                @if($errors->has('email'))
                                <div class="alert alert-danger">{{$errors->first('email')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_10" class="col-sm-2 col-form-label">{{trans('layout.facebook')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('facebook',  $site['facebook'], ['class'=>'form-control','id'=>'input_10']) }}
                                @if($errors->has('facebook'))
                                <div class="alert alert-danger">{{$errors->first('facebook')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_11" class="col-sm-2 col-form-label">{{trans('layout.twitter')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('twitter',  $site['twitter'], ['class'=>'form-control','id'=>'input_11']) }}
                                @if($errors->has('tags'))
                                <div class="alert alert-danger">{{$errors->first('twitter')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_12" class="col-sm-2 col-form-label">{{trans('layout.instagram')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('instagram',  $site['instagram'], ['class'=>'form-control','id'=>'input_12']) }}
                                @if($errors->has('instagram'))
                                <div class="alert alert-danger">{{$errors->first('instagram')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_13" class="col-sm-2 col-form-label">{{trans('layout.youtube')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('youtube',  $site['youtube'], ['class'=>'form-control','id'=>'input_13']) }}
                                @if($errors->has('youtube'))
                                <div class="alert alert-danger">{{$errors->first('youtube')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_14" class="col-sm-2 col-form-label">{{trans('layout.snapchat')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('snapchat',  $site['snapchat'], ['class'=>'form-control','id'=>'input_14']) }}
                                @if($errors->has('snapchat'))
                                <div class="alert alert-danger">{{$errors->first('snapchat')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_15" class="col-sm-2 col-form-label">{{trans('layout.google')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('google',  $site['google'], ['class'=>'form-control','id'=>'input_15']) }}
                                @if($errors->has('google'))
                                <div class="alert alert-danger">{{$errors->first('google')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_17" class="col-sm-2 col-form-label">{{trans('layout.android')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('android',  $site['android'], ['class'=>'form-control','id'=>'input_17']) }}
                                @if($errors->has('android'))
                                <div class="alert alert-danger">{{$errors->first('android')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_16" class="col-sm-2 col-form-label">{{trans('layout.ios')}}</label>
                            <div class="col-sm-10">
                                {{ Form::text('ios',  $site['ios'], ['class'=>'form-control','id'=>'input_16']) }}
                                @if($errors->has('ios'))
                                <div class="alert alert-danger">{{$errors->first('ios')}}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection