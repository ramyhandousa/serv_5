@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.car_city')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/cars_city')}}"> {{trans('layout.car_city')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.car_city_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

                 

        <form action="{{URL::to('admin/cars_city/'.$CityDifferent['id'])}}" method="post" enctype="multipart/form-data">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif
                           
                        <div class="form-group row">
                            <label for="input_1" class="col-sm-3 col-form-label">{{trans('layout.car_city_from')}}</label>
                            <div class="col-sm-9">
                                {{ Form::select('from',$city,$CityDifferent['from'], ['class'=>'form-control','id'=>'from']) }}
                                @if($errors->has('from'))
                                <div class="alert alert-danger">{{$errors->first('from')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.car_city_to')}}</label>
                            <div class="col-sm-9">
                                {{ Form::select('to',$city, $CityDifferent['to'], ['class'=>'form-control','id'=>'to']) }}
                                @if($errors->has('to'))
                                <div class="alert alert-danger">{{$errors->first('to')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="input_2" class="col-sm-3 col-form-label">{{trans('layout.car_city_price')}}</label>
                            <div class="col-sm-9">
                                {{ Form::text('price', $CityDifferent['price'], ['class'=>'form-control','id'=>'price']) }}
                                @if($errors->has('price'))
                                <div class="alert alert-danger">{{$errors->first('price')}}</div>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/cars_city')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection