@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.car_booking_add')}}
    </h1>
    
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/cars_booking')}}"> {{trans('layout.car_booking')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.car_booking_add')}}</li>
    </ol>

</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.car_booking_add')}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/cars_booking')}}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif



 
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">طريقة الدفع</label>
                            <div class="col-sm-9">
                                <select name="pay_method" id="pay_method" class="form-control">
                                    <option value="bank">
                                        {{trans('layout.car_booking_bank')}}
                                    </option>
                                    <option value="cash"> 
                                    {{trans('layout.car_booking_cash')}}

                                    </option>
                                    <option value="paypal">
                                    {{trans('layout.car_booking_paypal')}}
                                    </option>
                                </select>
                                @if($errors->has('pay_method'))
                                     <div class="alert alert-danger">{{$errors->first('pay_method')}}</div>
                                @endif   
                            </div>
                        </div>


                        {{--<div class="form-group row">--}}
                        {{--<label class="col-sm-3 control-label">الحجز بأسم</label>--}}
                        {{--<div class="col-sm-9">--}}
                        {{--{{Form::text('username', null,['class'=>'form-control','id'=>"mark_id"]) }}--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car')}}</label>
                            <div class="col-sm-9">

                                {{--{{Form::text('mark_id',$booking['Car']['Brand'][$lang.'_title'] .' - '.$booking['CaR']['Models']['ar_title'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}--}}

                                <select name="car_id" id="car_id" class="form-control">
                                    <option selected disabled> {{trans('layout.choose_car')}}</option>

                                    @if(count($data['cars']) > 0)
                                        @foreach($data['cars'] as $car)
                                            <?php $model = \App\CarModel::where('id', $car->model_id)->first(); ?>
                                            <option value="{{ $car->id }}">{{$model[$lang.'_title'] }}</option>
                                        @endforeach
                                    @endif

                                </select>
                                    @if($errors->has('car_id'))
                                        <div class="alert alert-danger">{{$errors->first('car_id')}}</div>
                                    @endif                            
                                </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">مدينة الاستلام</label>
                            <div class="col-sm-9">

                                <select name="city_id" id="car_id" class="form-control">
                                    <option selected disabled>مدينة الاستلام</option>

                                    @if(count($data['cities']) > 0)
                                        @foreach($data['cities'] as $city)
                                            <option value="{{ $city->id }}">{{ $city[$lang.'_name'] }}</option>
                                        @endforeach
                                    @endif

                                </select>
                                @if($errors->has('city_id'))
                                     <div class="alert alert-danger">{{$errors->first('city_id')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_booking_location_dev')}} </label>
                            <div class="col-sm-9">

                                <select name="airport_id" id="car_id" class="form-control">
                                    <option selected disabled>{{trans('layout.car_booking_location_dev')}}</option>

                                    @if(count($data['airports']) > 0)
                                        @foreach($data['airports'] as $airport)
                                            <option value="{{ $airport->id }}">{{ $airport[$lang.'_title'] }}</option>
                                        @endforeach
                                    @endif

                                </select>
                                @if($errors->has('airport_id'))
                                     <div class="alert alert-danger">{{$errors->first('airport_id')}}</div>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_choose_driver')}} </label>
                            <div class="col-sm-9">

                                <select name="driver_id" id="car_id" class="form-control">
                                    <option selected disabled>{{trans('layout.car_choose_driver')}}  </option>

                                    @if(count($data['drivers']) > 0)
                                        @foreach($data['drivers'] as $driver)
                                            <option value="{{ $driver->id }}">{{ $driver['name'] }}</option>
                                        @endforeach
                                    @endif

                                </select>
                                @if($errors->has('driver_id'))
                                     <div class="alert alert-danger">{{$errors->first('driver_id')}}</div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_booking_car_price_today')}}</label>
                            <div class="col-sm-9">
                                {{Form::text('price',null ,['class'=>'form-control','id'=>"mark_id"]) }}
                                 @if($errors->has('price'))
                                     <div class="alert alert-danger">{{$errors->first('price')}}</div>
                                 @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_booking_plus_price')}}</label>
                            <div class="col-sm-9">
                                {{Form::text('plus_price',null ,['class'=>'form-control','id'=>"mark_id"]) }}
                                @if($errors->has('plus_price'))
                                     <div class="alert alert-danger">{{$errors->first('plus_price')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_booking_days_count')}}</label>
                            <div class="col-sm-9">
                                {{Form::text('days',null ,['class'=>'form-control','id'=>"mark_id"]) }}
                                @if($errors->has('days'))
                                     <div class="alert alert-danger">{{$errors->first('days')}}</div>
                                @endif
                            </div>
                        </div>

                      

                        {{--<div class="form-group row">--}}
                        {{--<label class="col-sm-3 control-label">موقع الإستلام</label>--}}
                        {{--<div class="col-sm-9">--}}
                        {{--{{Form::text('mark_id',$booking['AirportData']['ar_title'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_booking_car_delivery')}}</label>
                            <div class="col-sm-9">
                                <div class="checkbox">
                                 
                                    {{ Form::checkbox('permissions[]','car_delivery',true,['id'=>'car_delivery']) }}
                                    <label for="car_delivery">{{trans('layout.car_delivery_place')}}</label>

                                    <span class="fa fa-warning text-danger fa-1x tagb" style="cursor: pointer;"></span>
                                    @if($errors->has('car_delivery'))
                                     <div class="alert alert-danger">{{$errors->first('car_delivery')}}</div>
                                @endif
                                </div>
                                
                            </div>
                        </div>

                        <div class=" car_delivery_dev form-group row  col-md-12">
                            <div class="col-md-3">
                            <span class="input-group-addon"> <span class="glyphicon glyphicon-map-marker"> </span>{{trans('layout.city')}}</span>
                            </div>
                            <div class="col-md-9">
                                <select class="form-control" id="seach_cities_dev" name="city_dev">
                                    <option value="">{{trans('layout.choose_city')}}</option>
                                    @foreach($data['cities'] as $city)
                                        <option value="{{$city->id}}"> {{$city['ar_name']}} </option>
                                    @endforeach
                                </select>
                                @if($errors->has('city_dev'))
                                     <div class="alert alert-danger">{{$errors->first('city_dev')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="car_delivery_dev form-group row col-md-12">
                            <div class="col-md-3">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-plane"> </span>{{trans('layout.location')}}</span>
                            </div>
                            <div class="col-md-9 ">
                                <select class="form-control " id="seach_airports_dev" name="airport_dev">
                                    <option value="">{{trans('layout.location')}}</option>
                                    @foreach($data['airports'] as $airport)
                                        <option value="{{$airport->id}}"> {{$airport[$lang.'_title']}} </option>
                                    @endforeach
                                </select>
                                @if($errors->has('airport_dev'))
                                     <div class="alert alert-danger">{{$errors->first('airport_dev')}}</div>
                                @endif
                            </div>
                        </div>


                        {{--<div class="take-time">--}}
                        {{--<div class="bfh-datepicker" data-format="y-m-d" data-date="today"--}}
                        {{--data-name="date_from"></div>--}}
                        {{--<div class="bfh-timepicker" data-mode="12h" data-name="time_from"></div>--}}
                        {{--</div>--}}
                        {{--<div class="return-time">--}}
                        {{--<div class="bfh-datepicker" data-format="y-m-d" data-name="date_to"></div>--}}
                        {{--<div class="bfh-timepicker" data-mode="12h" data-name="time_to"></div>--}}
                        {{--</div>--}}

                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_booking_date_from')}}</label>
                            <div class="col-sm-9">
                                {{Form::text('date_from',null,['class'=>'form-control','id'=>"mark_id"]) }}
                                @if($errors->has('date_from'))
                                     <div class="alert alert-danger">{{$errors->first('date_from')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_booking_time_from')}} </label>
                            <div class="col-sm-9">
                                {{Form::text('time_from',null,['class'=>'form-control','id'=>"mark_id"]) }}
                                @if($errors->has('time_from'))
                                     <div class="alert alert-danger">{{$errors->first('time_from')}}</div>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_booking_date_to')}}</label>
                            <div class="col-sm-9">
                                {{Form::text('date_to',null ,['class'=>'form-control','id'=>"mark_id"]) }}
                                @if($errors->has('date_to'))
                                     <div class="alert alert-danger">{{$errors->first('date_to')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_booking_time_to')}}</label>
                            <div class="col-sm-9">
                                {{Form::text('time_to',null,['class'=>'form-control','id'=>"mark_id"]) }}
                                @if($errors->has('time_to'))
                                     <div class="alert alert-danger">{{$errors->first('time_to')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">عمر السائق بين 30-65</label>
                            <div class="col-sm-9">
                            {{ Form::checkbox('permissions[]','driver_overage',false,['id'=>'driver_overage']) }}
                            <label for="driver_overage">{{trans('layout.driver_overage')}}</label>

                            
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-3 control-label">رقم الرحلة</label>
                            <div class="col-sm-9">
                                {{Form::text('flight_no',null,['class'=>'form-control','id'=>"mark_id"]) }}
                                @if($errors->has('flight_no'))
                                     <div class="alert alert-danger">{{$errors->first('flight_no')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">شركة الطيران</label>
                            <div class="col-sm-9">
                                {{Form::text('flight_name',null, ['class'=>'form-control','id'=>"mark_id"]) }}
                                @if($errors->has('flight_name'))
                                     <div class="alert alert-danger">{{$errors->first('flight_name')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">تاريخ الرحلة</label>
                            <div class="col-sm-9">
                                {{Form::text('flight_date',null,['class'=>'form-control','id'=>"mark_id"]) }}
                                @if($errors->has('flight_date'))
                                     <div class="alert alert-danger">{{$errors->first('flight_date')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">موعد الرحلة</label>
                            <div class="col-sm-9">
                                {{Form::text('flight_time',null,['class'=>'form-control','id'=>"mark_id"]) }}
                                @if($errors->has('flight_time'))
                                     <div class="alert alert-danger">{{$errors->first('flight_time')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">جهة المغادرة</label>
                            <div class="col-sm-9">
                                {{Form::text('flight_from',null,['class'=>'form-control','id'=>"mark_id"]) }}
                                @if($errors->has('flight_from'))
                                     <div class="alert alert-danger">{{$errors->first('flight_from')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">جهة الوصول</label>
                            <div class="col-sm-9">
                                {{Form::text('flight_to',null,['class'=>'form-control','id'=>"mark_id"]) }}
                                @if($errors->has('flight_to'))
                                     <div class="alert alert-danger">{{$errors->first('flight_to')}}</div>
                                @endif
                            </div>
                        </div>

                        


                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> الحمايه الكامله</label>
                            <div class="col-sm-9">
                                <select name="insurances" id="insurances" class="form-control">
                                    <option value="0">لا</option>
                                    <option value="1">نعم</option>
                                </select>
                                @if($errors->has('insurances'))
                                     <div class="alert alert-danger">{{$errors->first('insurances')}}</div>
                                @endif
                            </div>
                        </div>




<hr>
                      

                
                        
                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.add')}}</button>
            </div>

        </form>

    </div>

</section>

@endsection
@section('scripts')
<script>
        
        $('#brand_id').change(function (e) {
            var val = $(this).val();
            var model = $('#model').val();
            $.ajax({
                type: "GET",
                url: "{{asset('/')}}" + "admin/get_models?id=" + val + "&model=" + model,
                success: function (datas) {
                    $('#models').html(datas);
                }
            });
            return false;
        });


        $('#car_delivery').change(function () {
            if ($(this).is(':checked')) {
           
                $('.car_delivery_dev').slideUp();
            } else {
                $('.car_delivery_dev').slideDown();
            }
        });

        if ($('#car_delivery').is(':checked')) {
            $('.car_delivery_dev').slideUp();
        } else {
            $('.car_delivery_dev').slideDown();
        }
        </script>
@endsection

