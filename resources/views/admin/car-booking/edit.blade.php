@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.car_booking')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/cars_booking')}}"> {{trans('layout.car_booking')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.car_booking_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.car_booking_edit')}} : {{$booking[$lang.'_name']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/cars_booking/'.$booking['id'])}}" method="post" enctype="multipart/form-data">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-success alert-dismissible">
                                    {{ Session::get('message')}}
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                </div>
                            </div>
                        </div>
                        @endif


                        <div class="form-group row">
                            <label class="col-sm-3 control-label">التفعيل</label>
                            <div class="col-sm-9">
                                {{Form::select('active',['yet'=>'فى انتظار الدفع','wait'=>'فى انتظار تأكيد الدفع','yes'=>'حجز مؤكد','end'=>'منتهى','no'=>'إلغاء من الإدارة','cancel'=>'إلغاء من العميل'],$booking->active,['class'=>'form-control']) }}
                                {{ $errors->first('active','<div class="alert alert-danger">:message</div>')}}

                                <br>
                                <a href="/admin/cars/car/{{  $booking['Car']['id']  }}/edit" class="btn btn-success">بيانات
                                    السيارة</a>

                            </div>
                        </div>

                        <?php
                        $count_day = ceil(abs((strtotime($booking['date_to']) - strtotime($booking['date_from'])) / 86400));
                        $total_price1 = ($booking['price'] + $booking['security_price']) * $booking['days'];
                        $total_price2 = $total_price1 + $booking['addons_price'] + $booking['plus_price'];
                        $total_price = round($total_price2, 2);
                        $comission = $total_price * $booking['car']['comision'] / 100;
                        $rest = $total_price - $comission;
                        ?>
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">التكلفة الكلية</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$total_price.' ',['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">طريقة الدفع</label>
                            <div class="col-sm-9">
                                <select name="pay_method" id="pay_method" class="form-control">
                                    <option @if($booking['pay_method'] == "bank") selected @endif value="bank">تحويل
                                             بنكى
                                </option>
                                <option @if($booking['pay_method'] == "cash") selected @endif value="cash">الدفع عند
                                         الاستلام
                                </option>
                                <option @if($booking['pay_method'] == "paypal") selected @endif value="paypal">الدفع
                                     بالباى بال
                                </option>
                                <option @if($booking['pay_method'] !== "bank" && $booking['pay_method'] !== "cash" && $booking['pay_method'] !== "paypal") selected
                                 @endif value="">لم يتم الدفع
                                </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">المبلغ المدفوع</label>
                            <div class="col-sm-9">
                                @if($booking['pay_method'] == "cash")
                                {{Form::text('mark_id','0 ',['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                                @else
                                {{Form::text('mark_id',$comission.' ',['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">المبلغ المتبقى</label>
                            <div class="col-sm-9">
                                @if($booking['pay_method'] == "cash")
                                {{Form::text('mark_id',$total_price.' ',['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                                @else
                                {{Form::text('mark_id',$rest.' ',['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">رقم العضو</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['user']['id'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">العضو</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['user']['name'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">السيارة</label>
                            <div class="col-sm-9">

                                {{Form::text('mark_id',$booking['Car']['Mark']['ar_title'] .' - '.$booking['car']['Models']['ar_title'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}


                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">سعر السياره باليوم</label>
                            <div class="col-sm-9 row">
                                {{Form::text('price',$booking['price'] ,['class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">رسوم إتجاه واحد</label>
                            <div class="col-sm-9">
                                {{Form::text('plus_price',$booking['plus_price'] ,['class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">مدينة الإستلام</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['CityData']['ar_title'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">موقع الإستلام</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['AirportData']['ar_title'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">تاريخ الإستلام</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['date_from'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">ميعاد الإستلام</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['time_from'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">مدينة التسليم</label>
                            <div class="col-sm-9">
                                @if($booking->car_delivery == "off")
                                {{Form::text('mark_id',$booking['CityDataDev']['ar_title'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                                @else
                                {{Form::text('mark_id',$booking['CityData']['ar_title'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">موقع التسليم</label>
                            <div class="col-sm-9">
                                @if($booking->car_delivery == "off")
                                {{Form::text('mark_id',$booking['AirportDataDev']['ar_title'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                                @else
                                {{Form::text('mark_id',$booking['AirportData']['ar_title'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">تاريخ التسليم</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['date_to'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">ميعاد التسليم</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['time_to'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">عمر السائق بين 30-65</label>
                            <div class="col-sm-9">
                                @if($booking['driver_overage'] == "on")
                                {{Form::text('mark_id','نعم',['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                                @else
                                {{Form::text('mark_id','لا',['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">رقم الرحلة</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['flight_no'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">شركة الطيران</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['flight_name'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">تاريخ الرحلة</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['flight_date'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">موعد الرحلة</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['flight_time'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">جهة المغادرة</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['flight_from'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">جهة الوصول</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['flight_to'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">لقب السائق</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['Driver']['nickname'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">إسم السائق</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['Driver']['name'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">دولة السائق</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['driver_country'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">إيميل السائق</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['Driver']['email'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">هاتف السائق</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['Driver']['phone'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group hidden row">
                            <label class="col-sm-3 control-label">رقم جواز السائق</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['Driver']['passport'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group hidden row">
                            <label class="col-sm-3 control-label">رقم رخصة السائق</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['Driver']['licence'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> الحمايه الكامله 0 -> لا / 1 -> نعم</label>
                            <div class="col-sm-9">
                                @if($booking['insurances'] == "1")
                                {{Form::text('insurances','1',['class'=>'form-control','id'=>"mark_id"]) }}
                                @else
                                {{Form::text('insurances','0',['class'=>'form-control','id'=>"mark_id"]) }}
                                @endif
                            </div>
                        </div>

                        @if($booking['insurances'] == "1")
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">الحماية</label>
                            <div class="col-sm-9">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>الحماية</th>
                                            <th>السعر</th>
                                        </tr>
                                        <?php $i = 1; ?>
                                        @foreach($booking['Security'] as $secure)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{ $secure['CarSecurity']['Security']['ar_title'] }}</td>
                                            <td>{{ $secure['price'] * $count_day }} {{ $site['ar_currency'] }}</td>
                                        </tr>
                                        <?php $i++; ?>
                                        @endforeach
                                    </thead>
                                </table>
                            </div>
                        </div>
                        @endif

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">الإضافات</label>
                            <div class="col-sm-9">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>الإضافة</th>
                                            <th>السعر</th>
                                        </tr>
                                        <?php $i = 1; ?>
                                        @foreach($booking['Addon'] as $add)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{ $add['Addon']['ar_title'] }}</td>
                                            <td>{{ $add['price'] }} {{ $site['ar_currency'] }}</td>
                                        </tr>
                                        <?php $i++; ?>
                                        @endforeach
                                        @if(COUNT($booking['Addon']) == 0)
                                        <tr class="alert alert-danger">
                                            <td colspan="3">لا توجد إضافات</td>
                                        </tr>
                                        @endif
                                    </thead>
                                </table>


                            </div>
                        </div>

                        @if(isset($booking['extras']))
                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> طلبات اضافيه لصاحب الحجز </label>
                            <div class="col-sm-9">
                                <textarea name="extras" id="extras" class="form-control" cols="30" disabled
                                          rows="10"><?php echo $booking['extras']; ?></textarea>
                            </div>
                        </div>
                        @endif

                        @if($booking['pay_method'] == "bank")
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">البنك المحول إليه</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['Bank']['ar_title'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">صاحب الحساب المحول</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['owner_account'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">الدولة</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['country'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">رقم التحويل</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['ipan'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>
                        @elseif($booking['pay_method'] == "paypal")
                        <div class="form-group row">
                            <label class="col-sm-3 control-label">TXN</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['txn_id'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">نتيجة التحويل</label>
                            <div class="col-sm-9">
                                {{Form::text('mark_id',$booking['paypal_result'],['readonly','class'=>'form-control','id'=>"mark_id"]) }}
                            </div>
                        </div>
                        @endif


                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/cars_booking?active='.$booking['active'])}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection