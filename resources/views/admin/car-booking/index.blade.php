@extends('admin.layouts.table')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('layout.car_booking')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.car_booking')}}</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            @if (Session::has('message'))
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissible">
                        {{ Session::get('message')}}
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    </div>
                </div>
            </div>
            @endif

            <div class="box">
                <div class="box-header">
                    <a href="{{URL::to('admin/cars_booking/create')}}" class="btn btn-lg bg-fuchsia">{{trans('layout.car_booking_add')}}</a>
                </div>
                <div class="box-body">
                    <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 table-responsive">
                        <thead>
                            <tr>
                                <th class="{{$text}}">#</th>
                        
                                <th class="{{$text}}">الحجز</th>
                                <th class="{{$text}}">العضو</th>
                                <th class="{{$text}}">السيارة</th>
                                <th class="{{$text}}">التكلفة</th>
                                <th class="{{$text}}">الفترة</th>
                                <th class="{{$text}}">الحالة</th>
                                <th class="{{$text}}">التفاصيل</th>
                                <th class="{{$text}}">{{trans('layout.delete')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($bookings as $booking)
                            <tr>
                            <td>#</td>

                                <td>{{$booking['id']}}</td>
                                
                                <td>
                                    @if(isset( $booking->User->name)) 
                                       {{ $booking->user_id }} - {{ $booking->User->name }}
                                     @else
                                        من قبل الاداره 
                                    @endif
                                </td>

                                <td>
                                    {{ $booking['Car']['Mark']['ar_title'] .' - '.$booking['car']['Models']['ar_title'] }}
                                </td>                                

                                <td>
                                    <?php
                                    $count_day = ceil(abs((strtotime($booking['date_to']) - strtotime($booking['date_from'])) / 86400));
                                    $total_price1 = ($booking['price'] + $booking['security_price']) * $booking['days'];
                                    $total_price2 = $total_price1 + $booking['addons_price'] + $booking['plus_price'];
                                    $total_price = round($total_price2, 2);
                                    ?>
                                    {{ $total_price }} 
                                </td>
                                <td>{{$booking->date_from.'&nbsp;&nbsp;&nbsp;&nbsp;'.$booking->date_to}}</td>
                                <td>
                                    @if($booking->active == "yet")
                                        <span class="btn btn-xs btn-info">فى انتظار الدفع</span>
                                    @elseif($booking->active == "wait")
                                        <span class="btn btn-xs btn-warning">فى انتظار تأكيد الدفع</span>
                                    @elseif($booking->active == "no")
                                        <span class="btn btn-xs btn-danger">ملغى من الإدارة</span>
                                    @elseif($booking->active == "yes")
                                        <span class="btn btn-xs btn-success">حجز مؤكد</span>
                                    @elseif($booking->active == "end")
                                        <span class="btn btn-xs btn-primary">منتهى</span>
                                    @elseif($booking->active == "cancel")
                                        <span class="btn btn-xs btn-warning">مكنسل</span>
                                    @elseif($booking->active == "cancel_admin")
                                        <span class="btn btn-xs btn-danger">مكنسل من الإدارة</span>
                                    @endif
                                </td>
                                <td>
                                    {{ Form::open(array('url' =>'admin/cars_booking/'.$booking->id.'/edit', 'method' => 'GET')) }}
                                    <button  type="submit" class="btn default btn-sm btn-info"><i class="fa fa-edit"></i> {{trans('layout.edit')}} </button>
                                    {{ Form::close() }}
                               
                                </td>
                                <td>
                                    {{ Form::open(array('url' =>'admin/cars_booking/'.$booking->id, 'method' => 'DELETE')) }}
                                    <button  type="submit" class="btn default btn-sm btn-danger"><i class="fa fa-trash-o"></i> {{trans('layout.delete')}} </button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->          
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

@endsection