{{ Form::select('governate_id',$governates, null, ['class'=>'form-control select3','id'=>'governate_id']) }}

<script>
    // Select 2
    $('.select3').select2();
</script>
<script>
    $('#governate_id').change(function () {
        var val = $(this).val();
        var base_url = $('#base-url').val();
        if (val == "") {
            val = 0;
        }
        $.ajax({
            type: "GET",
            url: base_url + "/admin/activities/" + val + "?from=city",
            success: function (data) {
                console.log(data)
                $('#cities').html(data);
            }
        });
    });
</script>

@if($from == "branch")
<script>
    $('#governate_id').change(function () {
        var val = $(this).val();
        var base_url = $('#base-url').val();
        if (val == "") {
            val = 0;
        }
        $.ajax({
            type: "GET",
            url: base_url + "/admin/branches/" + val,
            success: function (data) {
                $('#cities').html(data);
            }
        });
    });
</script>

@endif