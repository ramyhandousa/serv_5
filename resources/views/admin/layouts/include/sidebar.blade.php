<?php
$lang = App::getLocale();
$float = "right";
if ($lang == "en") {
    $float = "left";
}
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" dir="{{$dir}}">
        <!-- Sidebar user panel -->
        <div class="user-panel" style="background-size: 100% 100%; padding-top: 120px;">

            <div class="clearfix"></div>
            <div class="image float-{{$float}}">
                <img src="{{URL::to('admin_panel')}}/images/user2-160x160.jpg" class="rounded" alt="User Image">
            </div>
            <div class="info float-right">
                <p>{{Auth::user()->username}}</p>
                <a><i class="fa fa-circle text-success"></i> {{trans('layout.online')}}</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree" style="padding-bottom: 50px;">
            <?php
            $url = URL::current();
            $agents_categories = $branches_employees = $branches = $agents_employees = $agents = $suppliers = $rooms = $hotels = $room_views = $food_types = $room_features = $room_types = $parking = $facilities = $faq = $model = $cars_booking = $banks = $jobs = $offices = $addon = $offer = $car_city = $security = $driver = $airport = $cars = $customer_service = $brand = $contact = $market_place_role = $market_place = $restaurant_category = $restaurants = $activity_type = $activities = $continent_cites = $continentCountries = $slider = $continents = $admins = $visitors = $users = $site = $groups = $pages = $countries = $governates = $cities = $newsletter = $admin = "";
            if (strpos($url, 'agents_categories') !== false) {
                $agents_categories = "active";
            } elseif (strpos($url, 'branches_employees') !== false) {
                $branches_employees = "active";
            } elseif (strpos($url, 'branches') !== false) {
                $branches = "active";
            } elseif (strpos($url, 'agents_employees') !== false) {
                $agents_employees = "active";
            } elseif (strpos($url, 'agents') !== false) {
                $agents = "active";
            } elseif (strpos($url, 'suppliers') !== false) {
                $suppliers = "active";
            } elseif (strpos($url, 'rooms') !== false) {
                $rooms = "active";
            } elseif (strpos($url, 'room_views') !== false) {
                $room_views = "active";
            } elseif (strpos($url, 'food_types') !== false) {
                $food_types = "active";
            } elseif (strpos($url, 'room_features') !== false) {
                $room_features = "active";
            } elseif (strpos($url, 'room_types') !== false) {
                $room_types = "active";
            } elseif (strpos($url, 'parking') !== false) {
                $parking = "active";
            } elseif (strpos($url, 'facilities') !== false) {
                $facilities = "active";
            } elseif (strpos($url, 'site') !== false) {
                $site = "active";
            } elseif (strpos($url, 'groups') !== false) {
                $groups = "active";
            } elseif (strpos($url, 'faq') !== false) {
                $faq = "active";
            } elseif (strpos($url, 'pages') !== false) {
                $pages = "active";
            } elseif (strpos($url, 'countries') !== false) {
                $countries = "active";
            } elseif (strpos($url, 'governates') !== false) {
                $governates = "active";
            } elseif (strpos($url, 'cities') !== false) {
                $cities = "active";
            } elseif (strpos($url, 'newsletter') !== false) {
                $newsletter = "active";
            } elseif (strpos($url, 'users') !== false) {
                $users = "active";
            } elseif (strpos($url, 'visitors') !== false) {
                $visitors = "active";
            } elseif (strpos($url, 'admins') !== false) {
                $admins = "active";
            } elseif (strpos($url, 'slider') !== false) {
                $slider = "active";
            } elseif (strpos($url, 'contact') !== false) {
                $contact = "active";
            } elseif (strpos($url, 'customer_service') !== false) {
                $customer_service = "active";
            } elseif (strpos($url, 'continents') !== false) {
                $continents = "active";
            } elseif (strpos($url, 'continent-country') !== false) {
                $continentCountries = "active";
            } elseif (strpos($url, 'continent-city') !== false) {
                $continent_cites = "active";
            } elseif (strpos($url, 'activity-type') !== false) {
                $activity_type = "active";
            } elseif (strpos($url, 'activities') !== false) {
                $activities = "active";
            } elseif (strpos($url, 'restaurant-categories') !== false) {
                $restaurant_category = "active";
            } elseif (strpos($url, 'restaurants') !== false) {
                $restaurants = "active";
            } elseif (strpos($url, 'market-places') !== false) {
                $market_place = "active";
            } elseif (strpos($url, 'brand') !== false) {
                $brand = "active";
            } elseif (strpos($url, 'airport') !== false) {
                $airport = "active";
            } elseif (strpos($url, 'model') !== false) {
                $model = "active";
            } elseif (strpos($url, 'driver') !== false) {
                $driver = "active";
            } elseif (strpos($url, 'addon') !== false) {
                $addon = "active";
            } elseif (strpos($url, 'security') !== false) {
                $security = "active";
            } elseif (strpos($url, 'cars/city') !== false) {
                $car_city = "active";
            } elseif (strpos($url, 'offer') !== false) {
                $offer = "active";
            } elseif (strpos($url, 'cars') !== false) {
                $cars = "active";
            } elseif (strpos($url, 'cars_booking') !== false) {
                $cars_booking = "active";
            } elseif (strpos($url, 'bank') !== false) {
                $banks = "active";
            } elseif (strpos($url, 'office') !== false) {
                $offices = "active";
            } elseif (strpos($url, 'job') !== false) {
                $jobs = "active";
            } elseif (strpos($url, 'hotels') !== false) {
                $hotels = "active";
            } else {
                $admin = "active";
            }

            $agents_categories_role = $branches_employees_role = $branches_role = $agents_employees_role = $agents_role = $suppliers_role = $rooms_role = $hotels_role = $room_views_role = $food_types_role = $room_features_role = $room_types_role = $parking_role = $facilities_role = $faq_role = $cars_booking_role = $jobs_role = $banks_role = $offices_role = $offer_role = $driver_role = $car_city_role = $security_role = $airport_role = $model_role = $cars_role = $brand_role = $customer_service_role = $restaurant_category_role = $restaurant_role = $activities_role = $activity_type_role = $continent_countries_role = $continent_cites_role = $contact_role = $continents_role = $slider_role = $admins_role = $visitors_role = $users_role = $site_role = $groups_role = $pages_role = $countries_role = $governates_role = $cities_role = $newsletter_role = $admin_role = FALSE;
            foreach (Auth::User()->Group->Permissions as $permission) {
                if ($permission['permission'] == 'agents_categories') {
                    $agents_categories_role = TRUE;
                } elseif ($permission['permission'] == 'branches_employees') {
                    $branches_employees_role = TRUE;
                } elseif ($permission['permission'] == 'branches') {
                    $branches_role = TRUE;
                } elseif ($permission['permission'] == 'agents_employees') {
                    $agents_employees_role = TRUE;
                } elseif ($permission['permission'] == 'agents') {
                    $agents_role = TRUE;
                } elseif ($permission['permission'] == 'suppliers') {
                    $suppliers_role = TRUE;
                } elseif ($permission['permission'] == 'rooms') {
                    $rooms_role = TRUE;
                } elseif ($permission['permission'] == 'hotels') {
                    $hotels_role = TRUE;
                } elseif ($permission['permission'] == 'room_views') {
                    $room_views_role = TRUE;
                } elseif ($permission['permission'] == 'food_types') {
                    $food_types_role = TRUE;
                } elseif ($permission['permission'] == 'room_features') {
                    $room_features_role = TRUE;
                } elseif ($permission['permission'] == 'room_types') {
                    $room_types_role = TRUE;
                } elseif ($permission['permission'] == 'parking') {
                    $parking_role = TRUE;
                } elseif ($permission['permission'] == 'facilities') {
                    $facilities_role = TRUE;
                } elseif ($permission['permission'] == 'site') {
                    $site_role = TRUE;
                } elseif ($permission['permission'] == 'groups') {
                    $groups_role = TRUE;
                } elseif ($permission['permission'] == 'faq') {
                    $faq_role = TRUE;
                } elseif ($permission['permission'] == 'pages') {
                    $pages_role = TRUE;
                } elseif ($permission['permission'] == 'countries') {
                    $countries_role = TRUE;
                } elseif ($permission['permission'] == 'governates') {
                    $governates_role = TRUE;
                } elseif ($permission['permission'] == 'cities') {
                    $cities_role = TRUE;
                } elseif ($permission['permission'] == 'newsletter') {
                    $newsletter_role = TRUE;
                } elseif ($permission['permission'] == 'users') {
                    $users_role = TRUE;
                } elseif ($permission['permission'] == 'visitors') {
                    $visitors_role = TRUE;
                } elseif ($permission['permission'] == 'admins') {
                    $admins_role = TRUE;
                } elseif ($permission['permission'] == 'slider') {
                    $slider_role = TRUE;
                } elseif ($permission['permission'] == 'contact') {
                    $contact_role = TRUE;
                } elseif ($permission['permission'] == 'customer_service') {
                    $customer_service_role = TRUE;
                } elseif ($permission['permission'] == 'continents') {
                    $continents_role = TRUE;
                } elseif ($permission['permission'] == 'continent_countries') {
                    $continent_countries_role = true;
                } elseif ($permission['permission'] == 'continent_cites') {
                    $continent_cites_role = true;
                } elseif ($permission['permission'] == 'activity_type') {
                    $activity_type_role = true;
                } elseif ($permission['permission'] == 'activities') {
                    $activities_role = true;
                } elseif ($permission['permission'] == 'restaurant_category') {
                    $restaurant_category_role = true;
                } elseif ($permission['permission'] == 'market_place') {
                    $market_place_role = true;
                } elseif ($permission['permission'] == 'restaurants') {
                    $restaurant_role = true;
                } elseif ($permission['permission'] == 'brand') {
                    $brand_role = true;
                } elseif ($permission['permission'] == 'model') {
                    $model_role = true;
                } elseif ($permission['permission'] == 'airport') {
                    $airport_role = true;
                } elseif ($permission['permission'] == 'driver') {
                    $driver_role = true;
                } elseif ($permission['permission'] == 'addon') {
                    $addon_role = true;
                } elseif ($permission['permission'] == 'car') {
                    $cars_role = true;
                } elseif ($permission['permission'] == 'security') {
                    $security_role = true;
                } elseif ($permission['permission'] == 'car_city') {
                    $car_city_role = true;
                } elseif ($permission['permission'] == 'offer') {
                    $offer_role = true;
                } elseif ($permission['permission'] == 'cars_booking') {
                    $cars_booking_role = true;
                } elseif ($permission['permission'] == 'bank') {
                    $banks_role = true;
                } elseif ($permission['permission'] == 'job') {
                    $jobs_role = true;
                } elseif ($permission['permission'] == 'office') {
                    $offices_role = true;
                }
            }
            ?>
            <li class="">
                <a href="{{URL::to('lang')}}">
                    <i class="fa fa-language"></i>
                    @if($lang == "ar")
                    <span>English Version</span>
                    @else
                    <span>النسخة العربية</span>
                    @endif
                </a>
            </li>

            <li class="">
                <a href="{{URL::to('/')}}">
                    <i class="fa fa-home"></i>
                    <span>{{trans('layout.website')}}</span>
                </a>
            </li>

            <li class="{{$admin}}">
                <a href="{{URL::to('admin')}}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{trans('layout.admin')}}</span>
                </a>
            </li>

            @if ($site_role)
            <li class="{{$site}}">
                <a href="{{URL::to('admin/site')}}">
                    <i class="fa fa-th"></i>
                    <span>{{trans('layout.site')}}</span>
                </a>
            </li>
            @endif

            @if ($continents_role)
            <li class="treeview  {{$continents}}">
                <a href="#">
                    <i class="fa fa-map-marker"></i>
                    <span>{{trans('layout.continents')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/continents')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/continents/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.continent_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($countries_role)
            <li class="treeview  {{$countries}}">
                <a href="#">
                    <i class="fa fa-map-marker"></i>
                    <span>{{trans('layout.countries')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/countries')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/countries/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.country_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($governates_role)
            <li class="treeview {{$governates}}">
                <a href="#">
                    <i class="fa fa-map"></i>
                    <span>{{trans('layout.governates')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>

                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/governates')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/governates/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.governate_add')}}</a></li>
                </ul>

            </li>
            @endif

            @if ($cities_role)
            <li class="treeview {{$cities}}">
                <a href="#">
                    <i class="fa fa-road"></i>
                    <span>{{trans('layout.cities')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/cities')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/cities/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.city_add')}}</a></li>
                </ul>
            </li>
            @endif

            <hr style='background: #fff;'>

            @if ($groups_role)
            <li class="treeview {{$groups}}">
                <a href="#">
                    <i class="fa fa-android"></i>
                    <span>{{trans('layout.groups')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/groups')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/groups/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.group_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($admins_role)
            <li class="treeview {{$admins}}">
                <a href="#">
                    <i class="fa fa-user-secret"></i>
                    <span>{{trans('layout.admins')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/admins')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/admins/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.admin_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($branches_role)
            <li class="treeview {{$branches}}">
                <a href="#">
                    <i class="fa fa-sitemap"></i>
                    <span>{{trans('layout.branches')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/branches')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/branches/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.branch_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($branches_employees_role)
            <li class="treeview {{$branches_employees}}">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>{{trans('layout.branches_employees')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/branches_employees')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/branches_employees/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.branch_employee_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($agents_categories_role)
            <li class="treeview {{$agents_categories}}">
                <a href="#">
                    <i class="fa fa-signal"></i>
                    <span>{{trans('layout.agents_categories')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/agents_categories')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/agents_categories/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.agent_category_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($agents_role)
            <li class="treeview {{$agents}}">
                <a href="#">
                    <i class="fa fa-briefcase"></i>
                    <span>{{trans('layout.agents')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/agents')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/agents/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.agent_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($agents_employees_role)
            <li class="treeview {{$agents_employees}}">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>{{trans('layout.agents_employees')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/agents_employees')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/agents_employees/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.agent_employee_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($users_role)
            <li class="{{$users}}">
                <a href="{{URL::to('admin/users')}}">
                    <i class="fa fa-user-circle-o"></i>
                    <span>{{trans('layout.users')}}</span>
                </a>
            </li>
            @endif

            @if ($visitors_role)
            <li class="{{$visitors}}">
                <a href="{{URL::to('admin/visitors')}}">
                    <i class="fa fa-users"></i>
                    <span>{{trans('layout.visitors')}}</span>
                </a>
            </li>
            @endif

            <hr style='background: #fff;'>

            @if ($facilities_role)
            <li class="treeview {{$facilities}}">
                <a href="#">
                    <i class="fa fa-apple"></i>
                    <span>{{trans('layout.facilities')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/facilities')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/facilities/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.facility_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($parking_role)
            <li class="treeview {{$parking}}">
                <a href="#">
                    <i class="fa fa-truck"></i>
                    <span>{{trans('layout.parking')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/parking')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/parking/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.parking_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($room_types_role)
            <li class="treeview {{$room_types}}">
                <a href="#">
                    <i class="fa fa-windows"></i>
                    <span>{{trans('layout.room_types')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/room_types')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/room_types/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.room_type_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($room_features_role)
            <li class="treeview {{$room_features}}">
                <a href="#">
                    <i class="fa fa-plus-square-o"></i>
                    <span>{{trans('layout.room_features')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/room_features')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/room_features/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.room_feature_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($food_types_role)
            <li class="treeview {{$food_types}}">
                <a href="#">
                    <i class="fa fa-coffee"></i>
                    <span>{{trans('layout.food_types')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/food_types')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/food_types/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.food_type_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($room_views_role)
            <li class="treeview {{$room_views}}">
                <a href="#">
                    <i class="fa fa-server"></i>
                    <span>{{trans('layout.room_views')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/room_views')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/room_views/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.room_view_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($hotels_role)
            <li class="treeview {{$hotels}}">
                <a href="#">
                    <i class="fa fa-university"></i>
                    <span>{{trans('layout.hotels')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/hotels')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/hotels/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.hotel_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($rooms_role)
            <li class="treeview {{$rooms}}">
                <a href="#">
                    <i class="fa fa-bed"></i>
                    <span>{{trans('layout.rooms')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/rooms')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/rooms/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.room_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($suppliers_role)
            <li class="treeview {{$suppliers}}">
                <a href="#">
                    <i class="fa fa-user-plus"></i>
                    <span>{{trans('layout.suppliers')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/suppliers')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/suppliers/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.supplier_add')}}</a></li>
                </ul>
            </li>
            @endif



            <hr style='background: #fff;'>

            @if ($offices_role)
            <li class="treeview {{$offices}}">
                <a href="#">
                    <i class="fa fa-desktop"></i>
                    <span>{{trans('layout.offices')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/office')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/office/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.office_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($banks_role)
            <li class="treeview {{$banks}}">
                <a href="#">
                    <i class="fa fa-bank"></i>
                    <span>{{trans('layout.banks')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/bank')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/bank/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.bank_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($cars_role)
            <li class="treeview {{$cars}}">
                <a href="#">
                    <i class="fa fa-car"></i> <span>{{trans('layout.cars')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{asset('/admin/cars/car')}}"><i class="fa fa-circle-o"></i>{{trans('layout.show_all')}}</a></li>
                    <li><a href="{{asset('/admin/cars/car/create')}}"><i class="fa fa-circle-o"></i>{{trans('layout.car_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($airport_role)
            <li class="treeview {{$airport}} ">
                <a href="#"><i class="fa fa-plane"></i> {{trans('layout.car_airport')}}
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{asset('/admin/cars/airport')}}"><i class="fa fa-circle-o"></i>{{trans('layout.show_all')}}</a></li>
                    <li><a href="{{asset('/admin/cars/airport/create')}}"><i class="fa fa-circle-o"></i>{{trans('layout.car_airport_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($driver_role)
            <li class="treeview {{$driver}} ">
                <a href="#"><i class="fa fa-drivers-license"></i> {{trans('layout.car_driver')}}
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{asset('/admin/cars/driver')}}"><i class="fa fa-circle-o"></i>{{trans('layout.show_all')}}</a></li>
                    <li><a href="{{asset('/admin/cars/driver/create')}}"><i class="fa fa-circle-o"></i>{{trans('layout.car_driver_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($car_city_role)
            <li class="treeview {{$car_city}} ">
                <a href="#"><i class="fa fa-usd"></i> {{trans('layout.car_city')}}
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{asset('/admin/cars/city')}}"><i class="fa fa-circle-o"></i>{{trans('layout.show_all')}}</a></li>
                    <li><a href="{{asset('/admin/cars/city/create')}}"><i class="fa fa-circle-o"></i>{{trans('layout.car_city_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($brand_role)
            <li class="treeview {{$brand}} ">
                <a href="#"><i class="fa fa-diamond"></i> {{trans('layout.car_brands')}}
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{asset('/admin/cars/brand')}}"><i class="fa fa-circle-o"></i>{{trans('layout.show_all')}}</a></li>
                    <li><a href="{{asset('/admin/cars/brand/create')}}"><i class="fa fa-circle-o"></i>{{trans('layout.car_brand_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($model_role)
            <li class="treeview {{$model}} ">
                <a href="#"><i class="fa fa-sitemap"></i> {{trans('layout.car_models')}}
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{asset('/admin/cars/model')}}"><i class="fa fa-circle-o"></i>{{trans('layout.show_all')}}</a></li>
                    <li><a href="{{asset('/admin/cars/model/create')}}"><i class="fa fa-circle-o"></i>{{trans('layout.car_model_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($offer_role)
            <li class="treeview {{$offer}} ">
                <a href="#"><i class="fa fa-certificate"></i> {{trans('layout.car_offer')}}
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{asset('/admin/cars/offer')}}"><i class="fa fa-circle-o"></i>{{trans('layout.show_all')}}</a></li>
                    <li><a href="{{asset('/admin/cars/offer/create')}}"><i class="fa fa-circle-o"></i>{{trans('layout.car_offer_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($cars_booking_role)
            <li class="treeview {{$cars_booking}} ">
                <a href="#"><i class="fa fa-shopping-bag"></i> {{trans('layout.car_booking')}}
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{asset('/admin/cars_booking')}}"><i class="fa fa-circle-o"></i>{{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('/admin/cars_booking?active=yes')}}"><i class="fa fa-circle-o"></i>  حجوزات جديدة</a></li>
                    <li><a href="{{URL::to('/admin/cars_booking?active=yet')}}"><i class="fa fa-circle-o"></i>  حجوزات جديدة فى انتظار تأكيد العميل</a></li>
                    <li><a href="{{URL::to('/admin/cars_booking?active=wait')}}"><i class="fa fa-circle-o"></i>   حجوزات جديدة فى انتظار تأكيد الإدارة</a></li>
                    <li><a href="{{URL::to('/admin/cars_booking?active=cancel')}}"><i class="fa fa-circle-o"></i>   حجوزات ملغاة من قبل العميل</a></li>
                    <li><a href="{{URL::to('/admin/cars_booking?active=end')}}"><i class="fa fa-circle-o"></i> حجوزات منتهية</a></li>
                    <li><a href="{{URL::to('/admin/cars_booking?active=no')}}"> <i class="fa fa-circle-o"></i> حجوزات ملغاة من الإدارة</a></li>
                    <li><a href="{{asset('/admin/cars_booking/create')}}"><i class="fa fa-circle-o"></i>{{trans('layout.car_booking_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($addon_role)
            <li class="treeview {{$addon}} ">
                <a href="#"><i class="fa fa-plus-circle"></i> {{trans('layout.car_addon')}}
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{asset('/admin/cars/addon')}}"><i class="fa fa-circle-o"></i>{{trans('layout.show_all')}}</a></li>
                    <li><a href="{{asset('/admin/cars/addon/create')}}"><i class="fa fa-circle-o"></i>{{trans('layout.car_addon_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($security_role)
            <li class="treeview {{$security}}  ">
                <a href="#"><i class="fa fa-lock"></i> {{trans('layout.car_security')}}          
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{asset('/admin/cars/security')}}"><i class="fa fa-circle-o"></i>{{trans('layout.show_all')}}</a></li>
                    <li><a href="{{asset('/admin/cars/security/create')}}"><i class="fa fa-circle-o"></i>{{trans('layout.car_security_add')}}</a></li>
                </ul>

            </li>
            @endif

            <hr style='background: #fff;'>

            @if ($activity_type_role)
            <li class="treeview  {{$activity_type}}">
                <a href="#">
                    <i class="fa fa-cube"></i>
                    <span>{{trans('layout.activity_types')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/activity-types')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/activity-types/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.activity_type_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($activities_role)
            <li class="treeview  {{$activities}}">
                <a href="#">
                    <i class="fa fa-money"></i>
                    <span>{{trans('layout.activities')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/activities')}}"><i
                                class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/activities/create')}}"><i
                                class="fa fa-circle-o"></i> {{trans('layout.activity_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($restaurant_category_role)
            <li class="treeview  {{$restaurant_category}}">
                <a href="#">
                    <i class="fa fa-coffee"></i>
                    <span>{{trans('layout.restaurant_category')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/restaurant-categories')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/restaurant-categories/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.restaurant_category_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($restaurant_role)
            <li class="treeview  {{$restaurants}}">
                <a href="#">
                    <i class="fa fa-star-half-full"></i>
                    <span>{{trans('layout.restaurants')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/restaurants')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/restaurants/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.restaurant_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($market_place_role)
            <li class="treeview  {{$market_place}}">
                <a href="#">
                    <i class="fa fa-shopping-bag"></i>
                    <span>{{trans('layout.market_place')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/market-places')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/market-places/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.market_place_add')}}</a></li>
                </ul>
            </li>
            @endif

            <hr style='background: #fff;'>

            @if ($customer_service_role)
            <li class="treeview {{$customer_service}}">
                <a href="#">
                    <i class="fa fa-headphones"></i>
                    <span>{{trans('layout.customer_service')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/customer_service')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/customer_service/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.customer_service_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($jobs_role)
            <li class="treeview {{$jobs}}">
                <a href="#">
                    <i class="fa fa-briefcase"></i>
                    <span>{{trans('layout.jobs')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/job')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/job/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.job_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($pages_role)
            <li class="treeview {{$pages}}">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>{{trans('layout.pages')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/pages')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/pages/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.page_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($faq_role)
            <li class="treeview {{$faq}}">
                <a href="#">
                    <i class="fa fa-question-circle"></i>
                    <span>{{trans('layout.faq')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/faq')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/faq/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.faq_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($slider_role)
            <li class="treeview {{$slider}}">
                <a href="#">
                    <i class="fa fa-photo"></i>
                    <span>{{trans('layout.slider')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/slider')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/slider/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.slider_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($newsletter_role)
            <li class="treeview {{$newsletter}}">
                <a href="#">
                    <i class="fa fa-envelope"></i>
                    <span>{{trans('layout.newsletter')}}</span>
                    <span class="pull-{{$arrow}}-container">
                        <i class="fa fa-angle-{{$arrow}} pull-{{$arrow}}"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{URL::to('admin/newsletter')}}"><i class="fa fa-circle-o"></i> {{trans('layout.show_all')}}</a></li>
                    <li><a href="{{URL::to('admin/newsletter/create')}}"><i class="fa fa-circle-o"></i> {{trans('layout.newsletter_add')}}</a></li>
                </ul>
            </li>
            @endif

            @if ($contact_role)
            <li class="{{$contact}}">
                <a href="{{URL::to('admin/contact')}}">
                    <i class="fa fa-phone-square"></i>
                    <span>{{trans('layout.contact')}}</span>
                </a>
            </li>
            @endif

            <hr style='background: #fff;'>

            <li class="">
                <a href="{{URL::to('admin/logout')}}">
                    <i class="fa fa-sign-out"></i>
                    <span>{{trans('layout.logout')}}</span>
                </a>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
    <div class="sidebar-footer">
        <!-- item-->
        <a href="{{URL::to('admin/site')}}" class="link" data-toggle="tooltip" title=""
           data-original-title="{{trans('layout.site')}}"><i class="fa fa-cog fa-spin"></i></a>
        <!-- item-->
        <a href="{{URL::to('admin/contact')}}" class="link" data-toggle="tooltip" title=""
           data-original-title="{{trans('layout.contact')}}"><i class="fa fa-envelope"></i></a>
        <!-- item-->
        <a href="{{URL::to('admin/logout')}}" class="link" data-toggle="tooltip" title=""
           data-original-title="{{trans('layout.logout')}}"><i class="fa fa-power-off"></i></a>
    </div>
</aside>