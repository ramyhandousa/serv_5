@extends('admin.layouts.form')

@section('content')
<?php
$lang = App::getLocale();
$text = "text-left";
$pull = "pull-left";
if ($lang == "ar") {
    $text = "text-right";
    $pull = "pull-right";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
    {{trans('layout.car_driver')}}
    </h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin')}}"> {{trans('layout.home')}}</a></li>
        <li class="breadcrumb-item {{$pull}}"><a href="{{URL::to('admin/drivers')}}"> {{trans('layout.car_driver')}}</a></li>
        <li class="breadcrumb-item active {{$pull}}">{{trans('layout.car_driver_edit')}}</li>
    </ol>
</section>

<section class="content">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">{{trans('layout.car_driver_edit')}} : {{$driver['name']}}</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>

        <form action="{{URL::to('admin/drivers/'.$driver['id'])}}" method="post" enctype="multipart/form-data">
            {{ Form::hidden('_method','PATCH') }}
            {{ csrf_field() }}

            <div class="box-body">
                <div class="row">
                    <div class="col-12">
                        @if (Session::has('message'))
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="alert alert-success alert-dismissible">
                                        {{ Session::get('message')}}
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    </div>
                                </div>
                            </div>
                        @endif

         
                        <div class="form-group row">
                            <label for="country_id" class="col-sm-3 col-form-label">{{trans('layout.country')}}</label>
                            <div class="col-sm-9">
                                {{ Form::select('country_id',$countries, null, ['class'=>'form-control select2','id'=>'country_id']) }}
                                @if($errors->has('country_id'))
                                <div class="alert alert-danger">{{$errors->first('country_id')}}</div>
                                @endif
                            </div>
                        </div>

                            <div class="form-group row">
                            <label for="governate_id" class="col-sm-3 col-form-label">{{trans('layout.governate')}}</label>
                            <div class="col-sm-9">
                                <div id="gover">
                                    {{ Form::select('governate_id',[''=>trans('layout.select_country_first')], null, ['class'=>'form-control select2','id'=>'governate_id']) }}
                                </div>
                                @if($errors->has('governate_id'))
                                <div class="alert alert-danger">{{$errors->first('governate_id')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="country_id" class="col-sm-3 col-form-label">{{trans('layout.city')}}</label>
                            <div class="col-sm-9">
                                <div id="cities">
                                {{ Form::select('city_id',[''=>trans('layout.select_country_first')], null, ['class'=>'form-control select2','id'=>'city_id']) }}
                                </div>
                                @if($errors->has('city_id'))
                                <div class="alert alert-danger">{{$errors->first('city_id')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label">{{trans('layout.car_driver_nickname')}} </label>
                            <div class="col-sm-9">
                                {{Form::text('nickname',$driver['nickname'],['class'=>'form-control']) }}
                                @if($errors->has('nickname'))
                                <div class="alert alert-danger">{{$errors->first('nickname')}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_driver_name')}} </label>
                            <div class="col-sm-9">
                                {{Form::text('name',$driver['name'],['class'=>'form-control']) }}
                                @if($errors->has('name'))
                                <div class="alert alert-danger">{{$errors->first('name')}}</div>
                                @endif

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_driver_email')}} </label>
                            <div class="col-sm-9">
                                {{Form::email('email',$driver['email'],['class'=>'form-control']) }}
                               
                                @if($errors->has('email'))
                                <div class="alert alert-danger">{{$errors->first('email')}}</div>
                                @endif

                           </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_driver_phone')}} </label>
                            <div class="col-sm-9">
                                {{Form::text('phone',$driver['phone'],['class'=>'form-control']) }}
                               
                                @if($errors->has('phone'))
                                <div class="alert alert-danger">{{$errors->first('phone')}}</div>
                                @endif

                           </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_driver_licence')}} </label>
                            <div class="col-sm-9">
                                {{Form::text('licence',$driver['licence'],['class'=>'form-control']) }}
                               
                                @if($errors->has('licence'))
                                <div class="alert alert-danger">{{$errors->first('licence')}}</div>
                                @endif

                           </div>
                        </div>

                         <div class="form-group row">
                            <label class="col-sm-3 control-label"> {{trans('layout.car_driver_passport')}} </label>
                            <div class="col-sm-9">
                                {{Form::text('passport',$driver['passport'],['class'=>'form-control']) }}
                                @if($errors->has('passport'))
                                <div class="alert alert-danger">{{$errors->first('passport')}}</div>
                                @endif

                           </div>
                        </div>
                        


                    </div>
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-lg" style="font-size: 16px;">{{trans('layout.save')}}</button>
                <a href="{{URL::to('admin/drivers')}}" class="btn btn-default btn-lg" style="font-size: 16px;">{{trans('layout.back')}}</a>
            </div>

        </form>

    </div>

</section>

@endsection

@section('scripts')
<script>
    $('#country_id').change(function () {
        var val = $(this).val();
        var base_url = $('#base-url').val();
        if (val == "") {
            val = 0;
        }
        $.ajax({
            type: "GET",
            url: base_url + "/admin/cities/" + val + "?from=city",
            success: function (data) {
                $('#gover').html(data);
            }
        });
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9uAy1z9dxw9aHx6_NJPRA0ih0UD-dCOU&callback=initMap&language=ar&region=EG"></script>
<script type="text/javascript">
    var marker;
    var lat;
    var lng;
    var map;
    function updateMarkerPosition(latLng) {
        document.getElementById('lat').value = latLng.lat();
        document.getElementById('lng').value = latLng.lng();
    }

    function placeMarker(location) {
        if (marker) {
            marker.setPosition(location);
        } else {
            marker = new google.maps.Marker({
                position: location,
                map: map
            });
        }
    }

    function initialize() {
        var lat = document.getElementById('lat').value;
        var lng = document.getElementById('lng').value;
        if (!lat && !lng) {
            var latLng = new google.maps.LatLng(24.598411724742483, 46.7138671875);
        } else {
            var latLng = new google.maps.LatLng(lat, lng);
        }

        map = new google.maps.Map(document.getElementById('mapCanvas'), {
            zoom: 5,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        marker = new google.maps.Marker({
            position: latLng,
            map: map
        });
        marker.set(map);
        updateMarkerPosition(latLng);
        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
            updateMarkerPosition(event.latLng);
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection