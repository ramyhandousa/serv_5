{{ Form::select('governate_id',$governates, $agent['governate_id'], ['class'=>'form-control','id'=>'governate_id']) }}

@if($agent['id'])
<label class="agent_id hide" val='{{$agent['id']}}'></label>
@else
<label class="agent_id hide" val='0'></label>
@endif
<script>
    // Select 2

    $('#governate_id').change(function () {
        var val = $(this).val();
        var agent_id = $('.agent_id').attr('val');
        var base_url = $('#base-url').val();
        if (val == "") {
            val = 0;
        }
        $.ajax({
            type: "GET",
            url: base_url + "/admin/get_cities_agent/" + val + '?agent_id=' + agent_id,
            success: function (data) {
                $('#cities').html(data);
            }
        });
    });
</script>