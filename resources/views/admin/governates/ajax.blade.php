{{ Form::select('governate_id',$governates, $branch['governate_id'], ['class'=>'form-control','id'=>'governate_id']) }}

@if($branch['id'])
<label class="branch_id hide" val='{{$branch['id']}}'></label>
@else
<label class="branch_id hide" val='0'></label>
@endif
<script>
    // Select 2

    $('#governate_id').change(function () {
        var val = $(this).val();
        var branch_id = $('.branch_id').attr('val');
        var base_url = $('#base-url').val();
        if (val == "") {
            val = 0;
        }
        $.ajax({
            type: "GET",
            url: base_url + "/admin/get_cities/" + val + '?branch_id=' + branch_id,
            success: function (data) {
                $('#cities').html(data);
            }
        });
    });
</script>