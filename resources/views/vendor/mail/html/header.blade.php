<tr>
    <td class="header" style="background:#7ec244;">
        <a href="{{ $url }}" style="color: #fff;">
            {{ $slot }}
        </a>
    </td>
</tr>
