<!DOCTYPE html>
<?php $lang = App::getLocale(); ?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>{{$data['site'][$lang.'_title']}} | @yield('title')</title>
        <meta name="description" content="{{$data['site'][$lang.'_desc']}}"/>
        <meta name="keywords" content="{{$data['site']['tags']}}"/>
        <meta name="generator" content="Spark-cloud">
        <link rel="canonical" href="{{URL::to('/')}}" />
        <meta property="og:locale" content="{{$lang}}_{{strtoupper($lang)}}" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="{{$data['site'][$lang.'_title']}}" />
        <meta property="og:description" content="{{$data['site'][$lang.'_desc']}}" />
        <meta property="og:url" content="{{URL::to('/')}}" />
        <meta property="og:site_name" content="{{$data['site'][$lang.'_title']}}" />
        <meta name="twitter:card" content="summary"/>
        <meta name="twitter:description" content="{{$data['site'][$lang.'_desc']}}"/>
        <meta name="twitter:title" content="{{$data['site'][$lang.'_title']}}"/>
        <meta name="twitter:domain" content="{{$data['site'][$lang.'_title']}}"/>

        <!-- CSS -->
        <link href="{{URL::to('interface')}}/css/bootstrap.min.css" rel="stylesheet">
        @if($lang == "ar")
        <link href="{{URL::to('interface')}}/css/bootstrap-rtl.min.css" rel="stylesheet">        
        @endif
        <link href="{{URL::to('interface')}}/fonts/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{URL::to('interface')}}/fonts/Stroke-Gap-Icons-Webfont/style.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{URL::to('interface')}}/css/extralayers.css" media="screen">	
        <link rel="stylesheet" type="text/css" href="{{URL::to('interface')}}/js/rs-plugin/css/settings.css" media="screen">
        <link href="{{URL::to('interface')}}/css/owl.carousel.min.css" rel="stylesheet">
        <link href="{{URL::to('interface')}}/css/owl.theme.default.min.css" rel="stylesheet">
        <link href="{{URL::to('interface')}}/css/prettyPhoto.css" rel="stylesheet">
        <link href="{{URL::to('interface')}}/css/animate.css" rel="stylesheet">
        <link href="{{URL::to('interface')}}/js/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet">
        <link href="{{URL::to('interface')}}/js/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="{{URL::to('interface')}}/css/main.css" rel="stylesheet">
        @if($lang == "ar")
        <link href="{{URL::to('interface')}}/css/main-rtl.css" rel="stylesheet">
        @endif
        <link href="{{URL::to('interface')}}/css/responsive.css" rel="stylesheet">
    </head>
    <body>


        <div style="min-height:800px;">
            <section class="section" style="padding: 10px 0 !important;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <!-- JavaScript -->
        <script src="{{URL::to('interface')}}/js/jquery.min.js"></script>
        <script src="{{URL::to('interface')}}/js/bootstrap.min.js"></script>
        <!-- BEGIN RevolutionSlider -->
        <script type="text/javascript" src="{{URL::to('interface')}}/js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>   
        <script type="text/javascript" src="{{URL::to('interface')}}/js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
        <!-- END RevolutionSlider -->
        <script src="{{URL::to('interface')}}/js/owl.carousel.min.js"></script>
        <!--<script src="js/pace.min.js"></script>-->
        <script src="{{URL::to('interface')}}/js/jquery.prettyPhoto.js"></script>
        <script src="{{URL::to('interface')}}/js/jquery.slimscroll.min.js"></script>
        <script src="{{URL::to('interface')}}/js/wow.min.js"></script>
        <script src="{{URL::to('interface')}}/js/bootstrap-select/js/bootstrap-select.min.js"></script>
        <script src="{{URL::to('interface')}}/js/main.js"></script>
        <script src="{{URL::to('interface')}}/js/jquery.bpopup.min.js"></script>
        <script src="{{URL::to('interface')}}/js/scripts.js"></script>

    </body>
</html>

