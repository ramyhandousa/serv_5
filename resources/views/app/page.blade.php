@extends('app.layout')

@section('title')
{{$data['page']['title']}}
@endsection

@section('content')

<h1>{{$data['page']['title']}}</h1>
<div class="section-header">
    {!! $data['page']['desc'] !!}
</div>

@endsection

@section('scripts')
@endsection