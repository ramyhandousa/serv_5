<?php

return [
    'site' => 'بيانات الموقع',
    'site_edit' => 'تعديل بيانات الموقع',
    'site_title_ar' => 'إسم الموقع بالعربي',
    'site_title_en' => 'إسم الموقع بالإنجليزي',
    'site_desc_ar' => 'وصف الموقع بالعربي',
    'site_desc_en' => 'وصف الموقع بالإنجليزي',
    'site_tags' => 'الكلمات الدلالية',
    'phone' => 'رقم الهاتف',
    'mobile' => 'رقم الموبيل',
    'fax' => 'الفاكس',
    'email' => 'البريد الإلكتروني',
    'facebook' => 'صفحة الفيس بوك',
    'twitter' => 'حساب تويتر',
    'instagram' => 'حساب إنستجرام',
    'snapchat' => 'حساب ستاب شات',
    'google' => 'حساب جوجل بلس',
    'youtube' => 'قناة اليوتيوب',
    'android' => 'رابط جوجل بلاي',
    'ios' => 'رابط الايفون',
    'save' => 'حفظ التعديلات',
    'back' => 'الرجوع',
    'groups' => 'مجموعات المشرفين',
    'group_name_ar' => 'إسم المجموعة بالعربي',
    'group_name_en' => 'إسم المجموعة بالإنجليزي',
    'group_power' => 'قوة المجموعة',
    'group_country' => 'نطاق المجموعة',
    'group_add' => 'إضافة مجموعة جديدة',
    'group_edit' => 'تعديل مجموعة حالية',
    'all_country' => 'كل الدول',
    'one_country' => 'دولة واحدة',
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'add' => 'إضافة',
    'add_suc' => 'تمت الإضافة بنجاح',
    'edit_suc' => 'تم حفظ التعديل بنجاح',
    'permissions' => 'الصلاحيات',
    'newsletter' => 'النشرة البريدية',
    'continents' => 'القارات',
    'countries' => 'الدول',
    'governates' => 'المحافظات',
    'cities' => 'المدن',
    'pages' => 'صفحات الموقع',
    'admins' => 'المشرفين والموظفين',
    'users' => 'الأعضاء',
    'visitors' => 'الزائرين',
    'show' => 'عرض',
    'delete_suc' => 'تمت عملية الحذف بنجاح',
    'yes_active' => 'فعال',
    'no_active' => 'غير فعال',
    'choose_category' => 'إختر التصنيف',
    'home' => 'الرئيسية',
    'page_add' => 'إضافة صفحة جديدة',
    'page_edit' => 'تعديل صفحة حالية',
    'page_title_ar' => 'عنوان الصفحة بالعربي',
    'page_title_en' => 'عنوان الصفحة بالإنجليزي',
    'page_desc_ar' => 'محتوي الصفحة بالعربي',
    'page_desc_en' => 'محتوي الصفحة بالإنجليزي',
    'continent_add' => 'إضافة قارة جديدة',
    'continent_edit' => 'تعديل قارة حالية',
    'continent_name_ar' => 'إسم القاره بالعربي',
    'continent_name_en' => 'إسم القارة بالإنجليزي',
    'continent_active' => 'تفعيل القارة',
    'continent_code' => 'كود القارة',
    'continent_image' => 'صورة القارة',
    'continent_image_current' => 'صورة القارة الحالية',
    'continent_description_ar' => 'وصف القارة  بالعربي ',
    'continent_description_en' => 'وصف القارة  بالإنجليزية',
    'continent' => 'القارة',
    'choose_continent' => 'إختر القارة',
    'country_description_ar' => 'وصف الدولة  بالعربي ',
    'country_description_en' => 'وصف الدولة  بالإنجليزية',
    'country_add' => 'إضافة دولة جديدة',
    'country_edit' => 'تعديل دولة حالية',
    'country_name_ar' => 'إسم الدولة بالعربي',
    'country_name_en' => 'إسم الدولة بالإنجليزي',
    'country_active' => 'تفعيل الدولة',
    'country_image' => 'صورة الدولة',
    'country_image_current' => 'صورة الدولة الحالية',
    'country_currency_ar' => 'إسم العملة بالعربي',
    'country_currency_en' => 'إسم العملة بالإنجليزي',
    'country' => 'الدولة',
    'choose_country' => 'إختر الدولة',
    'governate_add' => 'إضافة محافظة جديدة',
    'governate_edit' => 'تعديل محافظة حالية',
    'governate_name_ar' => 'إسم المحافظة بالعربي',
    'governate_name_en' => 'إسم المحافظة بالإنجليزي',
    'governate_description_ar' => 'وصف المحافظة بالعربي',
    'governate_description_en' => 'وصف المحافظة بالإنجليزي',
    'governate_code' => 'كود المحافظة',
    'governate_image' => 'صورة المحافظة',
    'governate_active' => 'تفعيل المحافظة',
    'governate' => 'المحافظة',
    'choose_governate' => 'إختر المحافظة',
    'city_add' => 'إضافة مدينة جديدة',
    'city_edit' => 'تعديل مدينة حالية',
    'city_name_ar' => 'إسم المدينة بالعربي',
    'city_name_en' => 'إسم المدينة بالإنجليزي',
    'city_code' => 'كود المدينة',
    'city_image' => 'صورة المدينة',
    'city_image_current' => 'صورة المدينة الحالية',
    'city_description_ar' => 'وصف المدينة  بالعربي ',
    'city_description_en' => 'وصف المدينة  بالإنجليزية',
    'city_overview_ar' => 'نظره عامة عن  المدينة  بالعربي ',
    'city_overview_en' => 'نظره عامة عن المدينة  بالإنجليزية ',
    'choose_city' => 'إختر المدينة',
    'city_active' => 'تفعيل المدينة',
    'select_country_first' => 'إختر الدولة أولاً',
    'choose_governate' => 'إختر المحافظة',
    'activity_type_add' => 'إضافة نوع نشاط جديد  ',
    'activity_type_edit' => 'تعديل نوع نشاط  ',
    'activity_type_name_ar' => 'اسم نوع النشاط بالعربى',
    'activity_type_name_en' => 'اسم نوع النشاط بالانجليزي',
    'activity_type_active' => 'تفعيل نوع النشاط ',
    'activity_types' => 'أنواع الأنشطة',
    'activity_type' => 'نوع النشاط',
    'choose_activity_type' => 'اختر نوع النشاط',
    'activity_add' => ' إضافة نشاط',
    'activity_phone' => 'هاتف النشاط',
    'activity_email' => 'ايميل النشاط',
    'activity_address_ar' => 'عنوان النشاط بالعربى ',
    'activity_address_en' => 'عنوان النشاط بالإنجليزي ',
    'activity_description_ar' => 'وصف النشاط بالعربي ',
    'activity_description_en' => 'وصف النشاط بالإنجليزي ',
    'activities' => 'الأنشطة ',
    'choose_activity' => 'أختر النشاط',
    'activity_from' => 'متاح من ',
    'activity_to' => 'متاح إلى ',
    'restaurant_category_add' => 'اضافة تصنيف مطعم',
    'restaurant_category_edit' => 'تعديل نصنيف المطعم الحالى  ',
    'restaurant_category_name_ar' => 'اسم تصنيف المطعم بالعربي',
    'restaurant_category_name_en' => 'اسم تصنيف المطعم بالانجليزي',
    'restaurant_category_active' => 'تفعيل تصنيف المطعم ',
    'restaurant_category' => 'تصنيفات المطعم ',
    'choose_restaurant_category' => 'اختر تصنيف المطعم',
    'restaurant_add' => 'اضافة مطعم',
    'restaurant_edit' => 'تعديل مطعم حالى ',
    'restaurant_name_ar' => 'اسم المطعم بالعربي',
    'restaurant_name_en' => 'اسم المطعم بالانجليزي',
    'restaurant_active' => 'تفعيل المطعم ',
    'restaurant_phone' => 'هاتف المطعم',
    'restaurant_email' => 'ايميل المطعم',
    'restaurant_image' => 'شعار المطعم',
    'restaurant_image_current' => 'شعار المطعم الحالى',
    'delivery' => 'توطيل الطلبات',
    'restaurant_overview_ar' => 'نظرة عامة عن المطعم بالعربى ',
    'restaurant_overview_en' => 'نظرة عامة عن المطعم بالعربى ',
    'restaurant_address_ar' => 'عنوان المطعم بالإنجليزي ',
    'restaurant_address_en' => 'عنوان المطعم بالإنجليزي ',
    'restaurant_work_from' => 'العمل من ',
    'restaurant_work_to' => 'العمل إلى ',
    'restaurants' => 'المطاعم ',
    'choose_restaurant' => 'اختر المطعم',
    'market_place_add' => 'اضافة سيارة جديدة',
    'market_place_edit' => 'تعديل  سيارة ',
    'market_place_name_ar' => 'اسم المول التجارى بالعربي',
    'market_place_name_en' => 'اسم المول التجارى بالانجليزي',
    'market_place_active' => 'تفعيل المول التجارى ',
    'market_place_phone' => 'هاتف امول التجارى',
    'market_place_email' => 'ايميل المول التجارى',
    'market_place_image' => 'صورة المول التجارى',
    'market_place_current' => 'صورة المول التجارى الحالية',
    'market_place_address_ar' => 'عنوان المول التجارى بالعربي',
    'market_place_address_en' => 'عنوان المول التجارى بالانجليزي',
    'market_place_overview_ar' => 'نظره عامة عن المول التجارى بالعربى',
    'market_place_overview_en' => 'نظرة عامة عن المول التجارى بالإنجليزي',
    'market_place' => 'المولات التجارية ',
    'choose_market_place' => 'اختر المول التجارى',
// to translate be to english
    'car_add' => '  اضافة سيارة',
    'car_edit' => 'تعديل  سيارة ',
    'car_name_ar' => ' اسم السيارة بالعربى',
    'car_name_en' => 'اسم السيارة بالانجليزي',
    'car_active' => 'تفعيل السيارة   ',
    'car_seat' => 'عدد المقاعد',
    'car_brand' => 'الماركة ',
    'car_model' => 'الموديل ',
    'car_city' => 'المدينه (يمكنك اختيار اكثر من مدينه)',
    'car_count' => 'عدد المتوفر',
    'car_seats' => 'عدد المقاعد',
    'car_doors' => 'عدد الابواب',
    'car_bags' => 'عدد الحقائب',
    'car_air' => ' مكيف الهواء',
    'car_vetis' => ' ناقل الحركة',
    'car_fuel' => ' نوع الوقود',
    'car_size' => '  حجم السيارة',
    'car_price' => '  سعر اليوم',
    'car_comision' => '(%) النسبة',
    'car_details_ar' => ' التفاصيل بالعربى',
    'car_details_en' => ' التفاصيل بالانجليزى',
    'car_conditions_ar' => ' شروط اضافية بالعربى',
    'car_conditions_en' => ' شروط اضافية بالانجليزى',
    'car_paypal' => 'الدفع باى بال',
    'car_bank' => 'الدفع بتحويل بنكى',
    'car_cash' => 'الدفع كاش',
    'car_admin_sure' => ' انتظار تأكيد الأدمن للحجز',
    'car_brand_add' => '  اضافة ماركه',
    'car_brand_edit' => 'تعديل  ماركة ',
    'car_brand_name_ar' => ' اسم الماركة بالعربى',
    'car_brand_name_en' => 'اسم الماركة بالانجليزي',
    'car_brand_active' => 'تفعيل  الماركة   ',
    'car_brand_image' => 'صورة ماركة السيارة',
    'car_brands' => ' ماركات السيارات ',
    'choose_brand' => '   أختر الماركة ',
    'car_model_add' => '  اضافة موديل',
    'car_model_edit' => 'تعديل  موديل ',
    'car_model_name_ar' => ' اسم الموديل بالعربى',
    'car_model_name_en' => 'اسم الموديل بالانجليزي',
    'car_model_active' => 'تفعيل  الموديل   ',
    'car_models' => ' موديلات السيارات ',
    'car_model_year' => '  سنة الاصدار ',
    'choose_model' => '   أختر الموديل ',
    'car_airport_add' => '  اضافة مطار',
    'car_airport_edit' => 'تعديل  مطار ',
    'car_airport_name_ar' => ' اسم المطار بالعربى',
    'car_airport_name_en' => 'اسم المطار بالانجليزي',
    'car_airport' => '  المطارات ',
    'choose_airport' => ' أختر المطار ',
    'car_driver_add' => '  اضافة سائق',
    'car_driver_edit' => 'تعديل  سائق ',
    'car_driver_nickname' => ' اسم السائق المستعار',
    'car_driver_name' => 'اسم السائق ',
    'car_driver_phone' => 'هاتف السائق ',
    'car_driver_email' => 'ايميل السائق ',
    'car_driver_passport' => 'رقم جواز السائق ',
    'car_driver_licence' => 'رقم رخصة السائق ',
    'car_driver' => '  السائقين ',
    'choose_driver' => ' أختر السائق ',
    'car_image' => 'صورة  السيارة',
    'cars' => 'ادارة السيارات ',
    'car' => ' السيارات ',
    'last_cars' => 'أخر السيارات المضافة',
    'all_cars' => 'شاهد جميع السيارات المتوفرة',
    'current_cars' => 'السيارات المتاحة',
    'no_cars' => 'لا توجد سيارة متاحة الأن',
    'avaliable_cars' => 'السيارات المتاحة فى',
    'get_car' => 'احصل علي سيارتك الان',
    'search_car' => 'إبحث عن تأجير سيارة',
    'car_one' => 'السيارة',
    'car_addon_add' => '  اضافة خدمة',
    'car_addon_edit' => 'تعديل  الخدمة ',
    'car_addon_name_ar' => ' اسم الخدمة بالعربى',
    'car_addon_name_en' => 'اسم الخدمة بالانجليزي',
    'car_addon_details_ar' => 'تفاصيل الخدمة بالعربى',
    'car_addon_details_en' => 'تفاصيل  الخدمة  بالانجليزي',
    'car_addon_price' => 'سعر الخدمة الإضافية ',
    'car_addon_active' => 'تفعيل الخدمة الاضافية',
    'car_addon_image' => 'صورة الخدمة الاضافية',
    'car_addon' => '  الخدمات الإضافية    ',
    'choose_addon' => ' أختر الخدمة الإضافية ',
    'car_security_add' => '  اضافة تامين وحماية',
    'car_security_edit' => 'تعديل  تامين وحماية ',
    'car_security_name_ar' => ' الاسم  بالعربى',
    'car_security_name_en' => 'الاسم  بالانجليزي',
    'car_security_price' => 'سعر التامين والحماية  ',
    'car_security_active' => 'تفعيل  التامين',
    'car_security' => '   التامين ووالحماية    ',
    'choose_security' => ' أختر التامين والحماية ',
    'car_city_add' => 'اضافة سعر اختلاف مدينة ',
    'car_city_edit' => ' تعديل سعر اختلاف مدينة',
    'car_city_from' => ' من مدينة',
    'car_city_to' => 'إلى مدينة  ',
    'car_city_price' => 'السعر',
    'car_city' => 'اسعار اختلاف المدن',
    'car_offer_add' => '  اضافة عرض',
    'car_offer_edit' => 'تعديل  عرض ',
    'car_offer_name_ar' => ' العنوان  بالعربى',
    'car_offer_name_en' => ' العنوان بالانجليزي',
    'car_offer_details_ar' => 'تفاصيل  بالعربى',
    'car_offer_details_en' => 'تفاصيل    بالانجليزي',
    'car_offer_active' => 'تفعيل العرض الاضافية',
    'car_offer_image' => 'صورة العرض الاضافية',
    'car_offer' => '   العروض ',
    'choose_offer' => ' أختر الخدمة الإضافية ',
    'car_booking' => 'الحجوزات ',
    'car_booking_add' => 'اضافة حجز ',
    'car_booking_edit' => 'تعديل حجز ',
    'car_booking_pay_method' => ' طريقة الدفع ',
    'car_booking_bank' => ' تحويل بنكى ',
    'car_booking_cash' => 'الدفع عند الاستلام  ',
    'car_booking_paypal' => 'الدفع باى بال',
    'car_choose_driver' => '  اختر السائق ',
    'car_booking_car_price_today' => ' سعر السياره باليوم ',
    'car_booking_plus_price' => ' رسوم إتجاه واحد ',
    'car_booking_days_count' => ' عدد الايام ',
    'car_booking_location_dev' => 'موقع الاستلام ',
    'car_booking_car_delivery' => 'موقع التسليم ',
    'car_booking_date_from' => ' تاريخ الاستلام ',
    'car_booking_date_to' => 'تاريخ التسليم',
    'car_booking_time_from' => ' معاد الاستلام ',
    'car_booking_time_to' => '  معاد التسليم ',
    'car_booking_add' => 'اضافة حجز ',
    'car_booking_add' => 'اضافة حجز ',
    'car_booking_add' => 'اضافة حجز ',
    'car_booking_add' => 'اضافة حجز ',
    'car_booking_add' => 'اضافة حجز ',
    'car_booking_add' => 'اضافة حجز ',
    'car_booking_add' => 'اضافة حجز ',
    'choose_car' => 'اختر  السيارة',
    'market_place_no_of_shops' => 'عدد المتاجر',
    'choose_city' => 'إختر المدينة',
    'location' => ' الموقع',
    'car_delivery_place' => 'إعادة السيارة إلى نفس المكان المستلم منه',
    'driver_overage' => 'هل عمر السائق بين 30-65',
    'jobs' => 'الوظائف  ',
    'job_name' => 'الاسم  ',
    'job_edit' => 'عرض  ',
    'job_phone' => 'الهاتف  ',
    'job_email' => 'الايميل  ',
    'job_date' => 'تاريخ  ',
    'banks' => 'الحسابات البنكية',
    'bank_add' => 'اضافة حساب بنكى',
    'bank_edit' => 'تعديل حساب بنكى',
    'bank_name_ar' => ' الاسم بالعربى',
    'bank_name_en' => ' الاسم بالانجليزى',
    'offices' => 'المكاتب',
    'office_add' => 'اضافة مكتب',
    'office_edit' => 'تعديل مكتب',
    'office_name_ar' => 'الاسم بالعربى',
    'office_name_en' => 'الاسم بالانجليزى',
/////////////////////////////////////////////////////
    'sign_in' => 'سجل دخول وإبدء بتنفيذ مهامك',
    'username' => 'إسم المستخدم',
    'password' => 'كلمة المرور',
    'remember' => 'تذكرني',
    'forget_password' => 'نسيت كلمة المرور ؟',
    'login' => 'تسجيل الدخول',
    'admin' => 'لوحة الإدارة',
    'not_allow' => 'غير مسموح لك بالدخول هذه المنطقة',
    'wrong_login' => 'اسم المستخدم او كلمه المرور غير صحيحه',
    'logout' => 'تسجيل الخروج',
    'website' => 'زيارة الموقع',
    'online' => 'متاح الأن',
    'show_all' => 'عرض الكل',
    'rights' => 'جميع الحقوق محفوظة',
    'city' => 'المنطقة',
    'select_governate_first' => 'إختر المحافظة اولاً',
    'choose_city' => 'إختر المنطقة',
    'send' => 'إرسال',
    'newsletter_add' => 'إرسال رسالة جديدة',
    'newsletter_title' => 'عنوان الرسالة',
    'newsletter_message' => 'محتوى الرسالة',
    'select_all' => 'تحديد الكل',
    'send_suc' => 'تم إرسال الرسالة بنجاح',
    'depts' => 'تصنيف الموردين',
    'dept_add' => 'إضافة تصنيف مورد جيد',
    'dept_edit' => 'تعديل تصنيف مورد حالي',
    'dept_name_ar' => 'إسم تصنيف مورد بالعربي',
    'dept_name_en' => 'إسم تصنيف مورد بالإنجليزي',
    'dept_active' => 'تفعيل تصنيف مورد',
    'dept_image' => 'صورة تصنيف مورد',
    'dept_image_current' => 'صورة تصنيف مورد الحالية',
    'yes' => 'نعم',
    'no' => 'لا',
    'name' => 'الإسم',
    'user_active' => 'تفعيل العضو',
    'user_edit' => 'تعديل عضو حالي',
    'admin_add' => 'إضافة موظف جديد',
    'admin_edit' => 'تعديل موظف حالي',
    'choose_group' => 'إختر المجموعة',
    'slider' => 'عارض صور الموقع',
    'slider_add' => 'إضافة صورة جديدة',
    'slider_edit' => 'تعديل صورة حالية',
    'slider_title_ar' => 'إسم الصورة بالعربي',
    'slider_title_en' => 'إسم الصورة بالإنجليزي',
    'slider_image' => 'صورة الموقع',
    'slider_image_current' => 'صورة الموقع الحالية',
    'slider_image_mobile' => 'صورة التطبيق',
    'slider_image_mobile_current' => 'صورة التطبيق الحالية',
    'contact' => 'التواصل معانا',
    'contact_active' => 'حالة الرسالة',
    'seen_reply' => 'تم الرد',
    'seen_reply_not' => 'تمت المشاهدة ولم يتم الرد',
    'seen_not' => 'لم تمت المشاهدة',
    'contact_edit' => 'الرد على رسالة',
    'reply' => 'الرد',
    'suc_register' => 'تم التسجيل بنجاح',
    'suc_register2' => 'تم التسجيل بنجاح في انتظار تاكيد التفعيل',
    'profile' => 'حسابي',
    'contact_count' => 'لديك عدد رسائل جديدة',
    'users_count' => 'لديك عدد عضو جديد',
    'orders_count' => 'لديك عدد طلب جديد',
    'orders_today' => 'طلبات اليوم',
    'visitors_today' => 'زائرين اليوم',
    'device' => 'الجهاز',
    'language' => 'اللغة',
    'countries_analysis' => 'تحليل الدول ',
    'phone_platform' => 'تحليل انظمة الهواتف',
    'language_analysis' => 'تحليل اللغة',
    'most_sale' => 'الأكثر ربحاًً',
    'year_report' => 'التقرير السنوي',
    'reset_password' => 'إستعادة كلمة المرور',
    'password_confirmation' => 'تأكدي كلمة المرور',
    'contact_message' => 'شكرا لتواصلكم معنا تم استلام رسالتكم',
    'message' => 'الرسالة',
    'reset_suc' => 'تم إسترجاع كلمة المرور بنجاح',
    'measure' => 'وحدات القياس',
    'measure_add' => 'إضافة وحدة قياس جديدة',
    'measure_edit' => 'تعديل وحدة قياس حالية',
    'measure_name_ar' => 'إسم وحدة القياس بالعربي',
    'measure_name_en' => 'إسم وحدة القياس بالإنجليزي',
    'menu_category' => 'تصنيفات المنيو',
    'menu_category_add' => 'إضافة تصنيف منيو جديد',
    'menu_category_edit' => 'تعديل تصنيف منيو حالي',
    'menu_category_name_ar' => 'إسم تصنيف المنيو بالعربي',
    'menu_category_name_en' => 'إسم تصنيف المنيو بالإنجليزي',
    'menu_category_active' => 'تفعيل تصنيف المنيو',
    'suppliers_account' => 'حسابات الموردين',
    'suppliers_account_add' => 'إضافة حساب مورد جديد',
    'suppliers_account_edit' => 'تعديل حساب مورد حالي',
    'suppliers_account_name_ar' => 'إسم المكان بالعربي',
    'suppliers_account_name_en' => 'إسم المكان بالإنجليزي',
    'suppliers_account_active' => 'تفعيل حساب المورد',
    'choose_dept' => 'إختر التصنيف',
    'activity_name_ar' => 'إسم النشاط بالعربي',
    'activity_name_en' => 'إسم النشاط بالإنجليزي',
    'delivery_allow' => 'مسئولية التوصيل',
    'delivery_barista' => 'باريستا',
    'delivery_no' => 'لا يوجد توصيل',
    'delivery_supplier' => 'المورد',
    'delivery_cost' => 'تكلفة الخدمة',
    'date_sub' => 'تاريخ إنتهاء الإشتراك',
    'amount_sub' => 'تكلفة الإشتراك',
    'comission' => 'العمولة',
    'comission_catering' => 'عمولة التجهيزات',
    'no_branch' => 'عدد الفروع',
    'no_account' => 'عدد حسابات الموظفين لكل فرع',
    'activity' => 'إسم النشاط',
    'activity_active' => 'حالة النشاط',
    'activity_info' => 'في إنتظار إكمال البيانات',
    'activity_yet' => 'في إنتظار التفعيل',
    'activity_yes' => 'فعالة',
    'activity_no' => 'غير فعالة',
    'activity_data' => 'بيانات النشاط',
    'activity_edit' => 'تعديل ببيانات النشاط',
    'activity_owner' => 'مالك النشاط',
    'activity_desc_ar' => 'وصف النشاط بالعربي',
    'activity_desc_en' => 'وصف النشاط بالإنجليزي',
    'activity_image' => 'صورة النشاط',
    'activity_image_current' => 'صورة النشاط الحالية',
    'activity_time' => 'زمن التوصيل',
    'minute' => 'دقيقة',
    'activity_limit' => 'الحد الأدنى للطلب',
    'branches' => 'الفروع',
    'branch_add' => 'إضافة فرع جديد',
    'branch_edit' => 'تعديل فرع حالي',
    'address' => 'العنوان',
    'later' => 'إستلام طلبات لاحقاً',
    'pick_up' => 'إستلام من الفرع',
    'cash' => 'الدفع عند التسليم',
    'knet' => 'كي نت',
    'credit_card' => 'بطاقات إئتمانية',
    'map' => 'الموقع على الخريطة',
    'branch_active' => 'تفعيل الفرع',
    'employee_account' => 'حسابات الموظفين',
    'employee_account_add' => 'إضافة حساب موظف جديد',
    'employee_account_edit' => 'تعديل حساب موظف حالي',
    'work' => 'مواعيد العمل',
    'sat' => 'السبت',
    'sun' => 'الأحد',
    'mon' => 'الأثنين',
    'tue' => 'الثلاثاء',
    'wed' => 'الأربعاء',
    'thu' => 'الخميس',
    'fri' => 'الجمعة',
    'vacancy' => 'أجازة',
    'day' => 'اليوم',
    'start' => 'بداية الفترة الأولي',
    'end' => 'نهاية الفترة الأولي',
    'start_1' => 'بداية الفترة الثانية',
    'end_1' => 'نهاية الفترة الثانية',
    'start_2' => 'بداية الفترة الثالثة',
    'end_2' => 'نهاية الفترة الثالثة',
    'work_edit' => 'تعديل ميعاد دوام',
    'employee_active' => 'تفعيل الموظف',
    'no_branch_allow' => 'عدد الفروع المسموح بها',
    'no_account_allow' => 'عدد حسابات الموظفين الفعالين المسموح لكل فرع',
    'no_branch_limit' => 'تجاوزت عدد الفروع المسموح بها',
    'no_account_limit' => 'تجاوزت عدد حسابات الموظفين المسموح لكل فرع',
    'branch' => 'الفرع',
    'choose_branch' => 'إختر الفرع',
    'branch_name_ar' => 'اسم الفرع بالعربي',
    'branch_name_en' => 'اسم الفرع بالإنجليزي',
    'addons' => 'الإضافات الجانبية',
    'addons_add' => 'إضافة طبق جانبي جديد',
    'addons_edit' => 'تعديل طبق جانبي حالي',
    'addons_name_ar' => 'إسم الإضافة بالعربي',
    'addons_name_en' => 'إسم الإضافة بالإنجليزي',
    'addons_active' => 'تفعيل الإضافة',
    'menu' => 'قائمة الأصناف',
    'menu_add' => 'إضافة صنف جديد',
    'menu_edit' => 'تعديل صنف حالي',
    'menu_name_ar' => 'اسم الصنف بالعربي',
    'menu_name_en' => 'اسم الصنف بالإنجليزي',
    'menu_active' => 'تفعيل الصنف',
    'menu_desc_ar' => 'وصف الصنف بالعربي',
    'menu_desc_en' => 'وصف الصنف بالإنجليزي',
    'menu_image' => 'صورة الصنف',
    'menu_image_current' => 'صورة الصنف الحالية',
    'choose_addon' => 'اختر الإضافة',
    'price_type' => 'نوع السعر',
    'price_fix' => 'سعر ثابت',
    'price_choice' => 'حسب إختيار وحدة القياس',
    'more' => 'المزيد',
    'measure_one' => 'وحدة القياس',
    'count' => 'الكمية',
    'orders' => 'طلبات الشراء',
    'orders_yet' => 'طلبات في إنتظار التأكيد',
    'orders_new' => 'طلبات قيد التحضير',
    'orders_unpaid' => 'طلبات لم يكتمل الدفع',
    'orders_delivery' => 'طلبات قيد التوصيل',
    'orders_done' => 'طلبات تم تسليها',
    'orders_cancel' => 'طلبات ملغاة',
    'orders_archive' => 'أرشيف الطلبات',
    'orders_archeive' => 'أرشيف الطلبات',
    'customer' => 'العميل',
    'date' => 'التاريخ',
    'time' => 'الساعة',
    'total' => 'الإجمالي',
    'pay_type' => 'طريقة الدفع',
    'time_type' => 'التوصيل',
    'order_type' => 'نوع الطلب',
    'choose_order_type' => 'إختر نوع الطلب',
    'type_pickup' => 'إستلام من الفرع',
    'type_delivery' => 'التوصيل للمنزل',
    'user_type' => 'نوع العضو',
    'search' => 'بحث',
    'now' => 'اليوم',
    'order_status' => 'حالة الطلب',
    'order_yet' => 'منتظر التأكيد',
    'order_new' => 'جاري التحضير',
    'order_unpaid' => 'عملية دفع فاشلة',
    'order_delivery' => 'قيد التوصيل',
    'order_done' => 'تم التسليم',
    'order_cancel' => 'ملغي',
    'order_search' => 'بحث في الطلبات',
    'choose_status' => 'إختر الحالة',
    'choose_pay_type' => 'إختر طريقة الدفع',
    'date_start' => 'بداية التاريخ',
    'date_end' => 'نهاية التاريخ',
    'orders_details_no' => 'تفاصيل الطلب رقم ',
    'user' => 'عضو',
    'address_title' => 'إسم العنوان',
    'block' => 'القطعة',
    'street' => 'الشارع',
    'jada' => 'الجادة',
    'building' => 'المبنى',
    'floor' => 'الطابق',
    'flat' => 'الشقة',
    'more_address' => 'تفاصيل العنوان',
    'delivery' => 'سعر التوصيل',
    'pricee' => 'تكلفة الطلب',
    'created_at' => 'توقيت الطلب',
    'count' => 'العدد',
    'notes' => 'الملاحظات',
    'menus' => 'المطلوب',
    'menu_price' => 'سعر الصنف',
    'meal' => 'الصتف',
    'ar' => 'اللغة العربية',
    'en' => 'اللغة الإنجليزية',
    'rate' => 'التقييم',
    'feedback' => 'رأي العميل',
    'platform' => 'المنصة',
    'voice' => 'التسجيل الصوتي',
    'price' => 'السعر',
    'discount' => 'نسبة الخصم',
    'offers' => 'العروض',
    'offer_add' => 'إضافة عرض جديد',
    'offer_edit' => 'تعديل عرض حالي',
    'offers_today' => 'عروض جارية',
    'offers_archieve' => 'أرشيف العروض',
    'offers_search' => 'البحث في العروض',
    'offer_name_ar' => 'إسم العرض بالعربي',
    'offer_name_en' => 'إسم العرض بالإنجليزي',
    'offer_start' => 'تاريخ بداية العرض',
    'offer_end' => 'تاريخ نهاية العرض',
    'offer_image' => 'صورة العرض',
    'offer_image_current' => 'صورة العرض الحالية',
    'choose_menu' => 'إختر الصنف',
    'offers_active' => 'تفعيل العرض',
    'end_active' => 'منتهي',
    'menu_one' => 'الوجبة',
    'places' => 'مناطق التوصيل',
    'place_edit' => 'تعديل مناطق التوصيل',
    'activities' => 'الأنشطة التجارية',
    'activity_end' => 'منتهية',
    'supplier' => 'المورد',
    'offer_name' => 'إسم العرض',
    'all' => 'الكل',
    'offer_name' => 'إسم العرض',
    'special_cities' => 'مناطق الطلب عبر باريستا',
    'city_image' => 'صورة المدينة',
    'city_image_current' => 'صورة المدينة الحالية',
    'financial' => 'التقارير المالية',
    'financial_add' => 'تحويل مستحق',
    'finanical_search' => 'البحث في التقارير المالية',
    'financial_subscribe' => 'دفع الإشتراكات',
    'financial_no' => 'إيصال دفع مستحق رقم ',
    'transcations_allow' => 'غير مسموح بتعديل الإيصال الا المشرف المنفذ ليه فقط',
    'financial_supplier' => 'المبلغ لدي المورد',
    'financial_barista' => 'المبلغ لدي بارستا',
    'transactions_done' => 'تحويلات تم تحويلها',
    'money_for' => 'المبلغ المستحق',
    'money_to' => 'المبلغ المستلم',
    'money_rest' => 'المبلغ المتبقي',
    'money_get' => 'المبلغ المحصل',
    'money_percent' => 'العمولة',
    'choose_supplier' => 'إختر المورد',
    'admins_one' => 'المشرف',
    'money_get_barista' => 'المحصل لدي باريستا',
    'money_get_supplier' => 'المحصل لدي المورد',
    'money_get_supplier_delivery' => 'مصاريف توصيل لمورد',
    'money_get_barista_delivery' => 'مصاريف توصيل لباريستا',
    'special_orders_today' => 'طلبات اليوم عبر باريستا',
    'more_sale' => 'الأعلى مبيعاً',
    'notifications' => 'إرسال إشعارات',
    'notification' => 'التنبيه',
    'all_platform' => 'كل أنظمة التشغيل',
    'all_users' => 'الأعضاء والزوار',
    'platform' => 'نظام التشغيل',
    'sender' => ' المرسل',
    'notification_add' => 'إرسال إشعار جديد',
    'notification_ar' => 'التنبيه بالعربي',
    'notification_en' => 'التنبيه بالإنجليزي',
    'notification_suc' => 'تم إرسال التنبيه بنجاح',
    'customer_service' => 'خدمة العملاء',
    'customer_service_add' => 'فتح تذكرة جديدة',
    'customer_service_edit' => 'تعديل تذكرة حالية',
    'customer_service_search' => 'البحث في تذاكر الدعم الفني',
    'ticket_title' => 'عنوان التذكرة',
    'ticket_note' => 'تفاصيل التذكرة',
    'ticket_status' => 'حالة التذكرة',
    'ticket_yet' => 'معقلة',
    'ticket_new' => 'جديدة',
    'ticket_done' => 'مغلقة',
    'activity_cover' => 'صورة الغلاف',
    'activity_cover_current' => 'صورة الغلاف الحالية',
    'addresses' => 'العناوين',
    'address_suc' => 'تم إضافة العنوان بنجاح',
    'address_edit' => 'تم تعديل العنوان بنجاح',
    'activity_index' => 'الظهور في الرئيسية',
    'faq' => 'الدعم الفني',
    'faq_add' => 'إضافة سؤال جديد',
    'faq_edit' => 'تعديل سؤال حالي',
    'faq_title_ar' => 'عنوان السؤال بالعربي',
    'faq_title_en' => 'عنوان السؤال بالإنجليزي',
    'faq_details_ar' => 'إجابة السؤال بالعربي',
    'faq_details_en' => 'إجابة السؤال بالإنجليزي',
    'flat' => 'شقة',
    'house' => 'منزل',
    'office' => 'مكتب',
    'address_type' => 'نوع العنوان',
    'birth_date' => 'تاريخ الميلاد',
    'sex' => 'النوع',
    'female' => 'أنثى',
    'male' => 'ذكر',
    'address_new' => 'إضافة عنوان جديد',
    'address_delete' => 'تم حذف العنوان بنجاح',
    'checkout_suc' => 'تتسطيع إرسال طلب الشراء بنجاح',
    'blog_depts' => 'أقسام المدونة',
    'blog_dept_add' => 'إضافة قسم جديد',
    'blog_dept_edit' => 'تعديل قسم حالي',
    'blog_dept_name_ar' => 'إسم القسم بالعربي',
    'blog_dept_name_en' => 'إسم القسم بالإنجليزي',
    'blog_dept_active' => 'تفعيل القسم',
    'blog' => 'المدونة',
    'choose_deptt' => 'إختر القسم',
    'blog_dept' => 'القسم',
    'blog_add' => 'إضافة موضوع جديد',
    'blog_edit' => 'تعديل موضوع حالي',
    'blog_title_ar' => 'إسم الموضوع بالعربي',
    'blog_title_en' => 'إسم الموضوع بالإنجليزي',
    'blog_desc_ar' => 'محتوي الموضوع بالعربي',
    'blog_desc_en' => 'محتوي الموضوع بالإنجليزي',
    'blog_image' => 'صورة الموضوع',
    'blog_image_current' => 'صورة الموضوع الحالية',
    'ship_time' => 'وقت وصول السائق',
    'visitor' => 'زائر',
    'PayTxnID' => 'كود الدفع',
    'financial_myfatora' => 'تقرير My Fatora',
    'financial_report' => 'تقرير عملاء',
    'myfatora_count' => 'عدد العمليات',
    'pickup_place' => 'مكان تسليم بالفرع',
    'pickup_placee' => 'مكان التسليم ',
    'in' => 'من داخل الفرع',
    'out' => 'من خارج الفرع بسيارتك',
    'car_notes' => 'مواصفات السيارة',
    'catering' => 'التجهيزات',
    'catering_category' => 'تصنيفات التجهيزات',
    'catering_category_add' => 'إضافة تصنيف تجيهزات جديد',
    'catering_category_edit' => 'تعديل تصنيف تجيهزات حالي',
    'catering_category_name_ar' => 'إسم تصنيف التجيهزات بالعربي',
    'catering_category_name_en' => 'إسم تصنيف التجيهزات بالإنجليزي',
    'catering_category_active' => 'تفعيل تصنيف التجيهزات',
    'catering_menu' => 'قائمة التجهيزات',
    'catering_menu_add' => 'إضافة صنف جديد',
    'catering_menu_edit' => 'تعديل صنف حالي',
    'catering_menu_name_ar' => 'إسم الصنف بالعربي',
    'catering_menu_name_en' => 'إسم الصنف بالإنجليزي',
    'catering_menu_active' => 'تفعيل الصنف',
    'catering_menu_desc' => 'المأكولات',
    'catering_menu_desc_ar' => 'المأكولات بالعربي',
    'catering_menu_desc_en' => 'المأكولات بالإنجليزي',
    'catering_number' => 'عدد الأفراد',
    'enable_increase' => 'يسمح بزيادة الأفراد',
    'price_person' => 'سعر الفرد الزيادة',
    'catering_menu_image' => 'صورة الصنف',
    'catering_menu_service' => 'الخدمة والتوصيل',
    'catering_menu_service_ar' => 'الخدمة والتوصيل بالعربي',
    'catering_menu_service_en' => 'الخدمة والتوصيل بالإنجليزي',
    'catering_menu_requires' => 'التجهيزات المطلوبة',
    'catering_menu_requires_ar' => 'التجهيزات المطلوبة بالعربي',
    'catering_menu_requires_en' => 'التجهيزات المطلوبة بالإنجليزي',
    'catering_menu_notes_ar' => 'ملاحظات أخري بالعربي',
    'catering_menu_notes_en' => 'ملاحظات أخري بالإنجليزي',
    'time_max' => 'أقصي وقت',
    'time_setup' => 'وقت التجهيز',
    'choose_time' => 'إختر التوقيت المناسب',
    'minute_15' => '١٥ دقيقة',
    'minute_30' => '٣٠ دقيقة',
    'minute_45' => '٤٥ دقيقة',
    'minute_60' => 'ساعة واحدة',
    'minute_75' => 'ساعة وربع',
    'minute_90' => 'ساعة ونصف',
    'minute_105' => 'ساعتين إلا ربع',
    'minute_120' => 'ساعتين',
    'minute_150' => 'ساعتين ونصف',
    'minute_180' => '٣ ساعات',
    'minute_210' => '٣ ساعات ونصف',
    'minute_240' => '٤ ساعات',
    'minute_270' => '٤ ساعات ونصف',
    'minute_300' => '٥ ساعات',
    'minute_330' => '٥ ساعات ونصف',
    'minute_360' => '٦ ساعات',
    'name_ar' => 'الإسم بالعربي',
    'name_en' => 'الإسم بالإنجليزي',
    'menu_name' => 'اسم الصنف',
    'admin_wait' => 'موافقة الإدارة',
    'no_wait' => 'موافق',
    'add_wait' => 'إنتظار الاضافة',
    'edit_wait' => 'إنتظار التعديل',
    'adding' => 'طلبات الإضافة',
    'editing' => 'طلبات التعديل',
    'active_code' => 'كود التفعيل الخاص بك بباريستا ',
    'suc_active' => 'مرحبا بك تم تفعيل حسابك بنجاح',
    'error_active' => 'كود التفعيل الخاص بك غير صحيح',
    'username_phone' => 'إسم المستخدم أو رقم الجوال',
    'user_active2' => 'تفعيل حسابك',
    'submit' => 'تأكيد',
    'active_code2' => 'كود التفعيل',
    'pickup_place_desc' => 'عند وصولك عند مكان الإستلام إضغط هنا ليخرج لك طلبك',
    'suc_arrived' => 'سيخرج لك طلبك في الحال',
    'arrived_orders_count' => 'عدد الطلبات بالانتظار خارج الفرع',
    'suc_register3' => 'يرجي تأكيد كود التفعيل المرسل على هاتفك لاكمال الطلب',
    'resend_code' => 'إعادة إرسال الكود',
    'suc_register4' => 'تم إرسال الكود مرة أخري بنجاح',
    'suc_register5' => 'تم التسجيل رقمك تم تفعليه من قبل يمكنك إستكمال طلبك',
    'send_code' => 'إرسال كود التفعيل',
    'block_number' => 'عفوا رقم هاتفك في قائمة الحظر',
    'block_list' => 'قائمة الحظر',
    'block_list_add' => 'إضافة رقم جديد',
    'block_list_edit' => 'تعديل رقم حالي',
    'type_catering' => 'التجهيزات',
    'ar_address' => 'العنوان بالعربي',
    'en_address' => 'العنوان بالإنجليزي',
    'sort' => 'ترتيب الظهور',
    'place_calculator' => 'حاسبة المسافات',
    'calc' => 'إحسب',
    'enter_data' => 'يرجي إدحال  كافة الحقول',
    'activity_add' => 'إضافة نشاط تجاري',
    'owner' => 'المالك',
    'choose_owner' => 'إختر المالك',
    'kwd' => 'دينار',
    'activity_alert_ar' => 'التنبيه بالعربي',
    'activity_alert_en' => 'التنبيه بالإنجليزي',
    'now_allow' => 'إستقبال طلبات في نقس اليوم',
    'supplier_seen' => 'المورد إستقبل',
    'supplier_seen_user' => 'الموظف المستقبل',
    'supplier_seen_time' => 'تاريخ المستقبل',
    'driver_status' => 'حالة السائق',
    'orders_seen_count' => 'طلبات لم يتم إستقبالها',
    'track_order' => 'تتبع طلبك',
    'order_id' => 'رقم الطلب',
    'no_order' => 'لا بوجد طلب بهذه البيانات',
    'become_seller' => 'إنضم لحبايبنا',
    'seller_edit' => 'طلب الانضمام لنا',
    'country_code' => 'كود الهاتف',
    'translate' => 'كود الترجمة',
    'facilities' => 'مرافق الفنادق',
    'facility_add' => 'إضافة مرفق جديد',
    'facility_edit' => 'تعديل مرفق حالي',
    'facility_name_ar' => 'إسم المرفق بالعربي',
    'facility_name_en' => 'إسم المرفق بالإنجليزي',
    'icon' => 'الأيقونة',
    'parking' => 'طرق الوصول للفندق',
    'parking_add' => 'إضافة طريقة وصول جديدة',
    'parking_edit' => 'تعديل طريقة وصول حالية',
    'parking_name_ar' => 'إسم الطريقة بالعربي',
    'parking_name_en' => 'إسم الطريقة بالإنجليزي',
    'room_types' => 'أنواع الغرف',
    'room_type_add' => 'إضافة نوع غرفة جديد',
    'room_type_edit' => 'تعديل نوع غرفة حالي',
    'room_type_name_ar' => 'إسم النوع بالعربي',
    'room_type_name_en' => 'إسم النوع بالإنجليزي',
    'room_type_notes_ar' => 'ملاحظات النوع بالعربي',
    'room_type_notes_en' => 'ملاحظات النوع بالإنجليزي',
    'room_features' => 'مميزات الغرف',
    'room_feature_add' => 'إضافة ميزة غرفة جديدة',
    'room_feature_edit' => 'تعديل ميزة غرفة حالية',
    'room_feature_name_ar' => 'إسم الميزة بالعربي',
    'room_feature_name_en' => 'إسم الميزة بالإنجليزي',
    'food_types' => 'أنواع الوجبات',
    'food_type_add' => 'إضافة نوع وجبة جديد',
    'food_type_edit' => 'تعديل نوع وجبة حالي',
    'food_type_name_ar' => 'إسم نوع الوجبة بالعربي',
    'food_type_name_en' => 'إسم نوع الوجبة بالإنجليزي',
    'food_type_notes_ar' => 'تفاصيل نوع الوجبة بالعربي',
    'food_type_notes_en' => 'تفاصيل نوع الوجبة بالإنجليزي',
    'room_views' => 'إطلالات الغرف',
    'room_view_add' => 'إضافة إطلالة جديدة',
    'room_view_edit' => 'تعديل إطلالة حالية',
    'room_view_name_ar' => 'إسم الإطلالة بالعربي',
    'room_view_name_en' => 'إسم الإطلالة بالإنجليزي',
    'room_view_notes_ar' => 'تفاصيل الإطلالة بالعربي',
    'room_view_notes_en' => 'تفاصيل الإطلالة بالإنجليزي',
    'hotels' => 'الفنادق',
    'hotel_add' => 'إضافة فندق جديد',
    'hotel_edit' => 'تعديل فندق حالي',
    'rooms' => 'الغرف',
    'room_add' => 'إضافة غرفة جديدة',
    'room_edit' => 'تعديل غرفة حالية',
    'suppliers' => 'الموردين',
    'supplier_add' => 'إضافة مورد جديد',
    'supplier_edit' => 'تعديل مورد حالي',
    'options' => 'الإختيارات',
    'option_ar' => 'الإختيار بالعربي',
    'option_en' => 'الإختيار بالإنجليزي',
    'agents_categories' => 'تصنيفات الوكالات السياحية',
    'agent_category_add' => 'إضافة تصنيف وكالات جديد',
    'agent_category_edit' => 'تعديل تصنيف وكالات حالي',
    'category_name_ar' => 'إسم التصنيف بالعربي',
    'category_name_en' => 'إسم التصنيف بالإنجليزي',
    'agents' => 'الوكالات السياحية',
    'agent_add' => 'إضافة وكيل جديد',
    'agent_edit' => 'تعديل وكيل حالي',
    'agents_employees' => 'موظفين الوكالات',
    'agent_employee_add' => 'إضافة موظف بالوكالة',
    'branches_employees' => 'موظفين الفروع',
    'branch_employee_add' => 'إضافة موظف بالفرع',
    'agent_category' => 'تصنيف الوكالة السياحية',
    'agent_name_ar' => 'إسم الوكالة بالعربي',
    'agent_name_en' => 'إسم الوكالة بالإنجليزي',
    'agent' => 'الوكالة السياحية',
    'agent_active' => 'تفعيل الوكالة',
];
