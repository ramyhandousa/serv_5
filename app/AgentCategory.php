<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;

class AgentCategory extends Model {

    //
    protected $table = 'agents_categories';

    public function Agents() {
        return $this->hasMany(Agent::class, 'category_id', 'id');
    }

    public function getList() {
        $lang = Config::get('app.locale');
        return $this->pluck($lang . '_name', 'id')->toArray();
    }

    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        return $this->save();
    }

    public function edit($id, $data) {
        $governate = $this->find($id);
        $governate->ar_name = $data['ar_name'];
        $governate->en_name = $data['en_name'];
        return $governate->save();
    }

    public function remove($id) {
        $governate = $this->find($id);
        return $governate->delete();
    }

}
