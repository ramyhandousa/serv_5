<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
class CarBooking extends Model
{
  

    public function User()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function Car()
    {
        return $this->hasOne('App\Car', 'id', 'car_id');
    }

    public function Bank()
    {
        return $this->hasOne('App\Bank', 'id', 'bank_id');
    }

    public function CityData()
    {
        return $this->hasOne('App\City', 'id', 'city');
    }

    public function CityDataDev()
    {
        return $this->hasOne('App\City', 'id', 'city_dev');
    }

    public function AirportData()
    {
        return $this->hasOne('App\Airport', 'id', 'airport_id');
    }

    public function AirportDataDev()
    {
        return $this->hasOne('App\Airport', 'id', 'airport_dev');
    }

    public function Security()
    {
        return $this->hasMany('App\CarBookingSecurity', 'booking_id', 'id');
    }

    public function Driver()
    {
        return $this->hasOne('App\Driver','id','driver_id');
    }

    public function Addon()
    {
        return $this->hasMany('App\CarBookingAddon', 'booking_id', 'id');
    }

    public function getAll()
    {
        return $this->all();
    }

    public function getActive($active)
    {
        if ($active == "") {
            return $this->orderBy('id', 'desc')->get();
        }
        return $this->where('active', $active)->orderBy('id', 'desc')->get();
    }

    public function getById($id)
    {
        return $this->find($id);
    }

    public function add($data)
    {
        $this->car_id = strip_tags($data['car_id']);
        $this->user_id = strip_tags($data['user_id']);
        $this->city_id = strip_tags($data['city_id']);
        $this->airport_id = strip_tags($data['airport_id']);
        $this->driver_id = $data['driver_id'];
        $this->date_from = strip_tags($data['date_from']);
        $this->time_from = strip_tags($data['time_from']);
        $this->date_to = strip_tags($data['date_to']);
        $this->time_to = strip_tags($data['time_to']);
        $this->car_delivery = strip_tags($data['car_delivery']);
        if ($data['car_delivery'] == "off") {
            $this->city_dev = strip_tags($data['city_dev']);
            $this->airport_dev = strip_tags($data['airport_dev']);
        }
        $this->driver_overage = strip_tags($data['driver_overage']);
        $this->flight_no= strip_tags($data['flight_no']);
        $this->flight_name = $data['flight_name'];
        $this->flight_date = $data['flight_date'];
        $this->flight_time = $data['flight_time'];
        $this->flight_from = $data['flight_from'];
        $this->flight_to = $data['flight_to'];
        $this->insurances = $data['insurances'];
        $this->price = $data['price'];
        $this->plus_price = $data['plus_price'];
        $this->security_price = $data['security_price'];
        $this->addons_price = $data['addons_price'];
        $this->extras = $data['extras'];
        $this->days = $data['days'];
        $this->active = 'yet';
        $this->save();
        return $this->id;
    }

    public function addPrice($id, $security_price, $addons_price)
    {
        $car = $this->find($id);

        return $car->save();
    }

    public function payCash($id, $active)
    {
        $car = $this->find($id);
        $car->active = $active;
        $car->pay_method = 'cash';
        $car->seen = 'no';
        return $car->save();
    }

    public function payBank($data)
    {
        $car = $this->find($data['id']);
        $car->active = $data['active'];
        $car->pay_method = 'bank';
        $car->bank_id = $data['bank_id'];
        $car->owner_account = $data['owner_account'];
        $car->ipan = $data['ipan'];
        $car->country = $data['country'];
        $car->seen = 'no';
        return $car->save();
    }

    public function getTXN($id)
    {
        return $this->where('txn_id', '=', $id)->count();
    }

    public function seen($id)
    {
        $car = $this->find($id);
        $car->seen = 'yes';
        return $car->save();
    }

    public function payPaypal($id, $data)
    {
        $car = $this->find($id);
        if ($data['status'] == "Completed") {
            $car->active = $data['active'];
        } else {
            $car->active = 'yet';
        }
        $car->txn_id = $data['txn'];
        $car->paypal_result = $data['status'];
        $car->seen = 'no';
        return $car->save();
    }

    public function cancel($id)
    {
        $car = $this->find($id);
        $car->active = 'cancel';
        $car->seen = 'no';
        return $car->save();
    }

    public function edit($id, $data)
    {
        $car = $this->find($id);
        $car->pay_method = strip_tags($data['pay_method']);
        $car->insurances = $data['insurances'];
        $car->price = $data['price'];
        $car->plus_price = $data['plus_price'];
        $car->active = strip_tags($data['active']);
        return $car->save();
    }

    public function remove($id)
    {
        $car = $this->find($id);
        $filename = $car->image;
        $path = public_path() . '/images/cars/';
        File::delete($path . $filename);
        return $car->delete();
    }

    public function getNum()
    {
        return $this->count();
    }

    public function search_fast($data)
    {
        if ($data['country'] != "") {
            $v1 = $this->where('city_id', $data['country']);
        } else {
            $v1 = $this;
        }

        if ($data['brand'] != "") {
            $v2 = $v1->where('brand_id', $data['brand']);
        } else {
            $v2 = $v1;
        }

        if ($data['model'] != "") {
            $v3 = $v2->where('brand_id', $data['brand']);
        } else {
            $v3 = $v2;
        }

        return $v3->get();
    }

    public function getFound($from, $to, $car)
    {
        return $this->where('car_id', $car)->where('date_from', '<=', $to)->where('date_to', '>=', $from)->where('active', 'yes')->count();
    }

    public function getFoundExcept($from, $to, $car, $id)
    {
        return $this->where('id', '!=', $id)->where('car_id', $car)->where('date_from', '<=', $to)->where('date_to', '>=', $from)->where('active', 'yes')->count();
    }

}
