<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;

class Agent extends Model {

    protected $table = 'agents';

    //
    public function Branch() {
        return $this->hasOne(Branch::class, 'id', 'branch_id');
    }

    public function Category() {
        return $this->hasOne(AgentCategory::class, 'id', 'category_id');
    }

    public function City() {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function Governate() {
        return $this->hasOne(Governate::class, 'id', 'governate_id');
    }

    public function Employees() {
        return $this->hasMany(User::class, 'agent_id', 'id');
    }

    public function getList($id) {
        $lang = Config::get('app.locale');
        return $this->pluck($lang . '_name', 'id')->where('active', '!=', 'delete')->toArray();
    }

    public function getAll() {
        return $this->where('active', '!=', 'delete')->get();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->category_id = $data['category_id'];
        $this->branch_id = $data['branch_id'];
        $this->governate_id = $data['governate_id'];
        $this->city_id = $data['city_id'];
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->phone = $data['phone'];
        $this->mobile = $data['mobile'];
        $this->email = $data['email'];
        $this->ar_address = $data['ar_address'];
        $this->en_address = $data['en_address'];
        $this->lat = $data['lat'];
        $this->lng = $data['lng'];
        return $this->save();
    }

    public function edit($id, $data) {
        $agent = $this->find($id);
        $agent->active = $data['active'];
        $agent->category_id = $data['category_id'];
        $agent->branch_id = $data['branch_id'];
        $agent->governate_id = $data['governate_id'];
        $agent->city_id = $data['city_id'];
        $agent->ar_name = $data['ar_name'];
        $agent->en_name = $data['en_name'];
        $agent->phone = $data['phone'];
        $agent->mobile = $data['mobile'];
        $agent->email = $data['email'];
        $agent->ar_address = $data['ar_address'];
        $agent->en_address = $data['en_address'];
        $agent->lat = $data['lat'];
        $agent->lng = $data['lng'];
        return $agent->save();
    }

    public function remove($id) {
        $agent = $this->find($id);
        $agent->active = 'delete';
        return $agent->save();
    }

}
