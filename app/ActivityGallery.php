<?php

namespace App;
use File;

use Illuminate\Database\Eloquent\Model;

class ActivityGallery extends Model
{
    //

    public function Activity()
    {
        return $this->belongsTo('App\Activity', 'activity_id','id');
    }


    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data, $file) {
        $this->activity_id = $data['activity_id'];
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->url = $file;
        $this->type = $data['type'];
        return $this->save();
    }

    public function edit($id, $data,$file = false) {
        $activityGallery = $this->find($id);
        $activityGallery->ar_name = $data['ar_name'];
        $activityGallery->en_name = $data['en_name'];
        if ($file) {
            $path = public_path('upload/activity/');
            $filename = $activityGallery->image;
            File::Delete($path . $filename);
            $activityGallery->url = $file;
        }

        $activityGallery->type = $data['type'];
        return $activityGallery->save();
    }

    public function remove($id) {
        $activityGallery = $this->find($id);
        $path = public_path('upload/activity/');
        $filename = $activityGallery['image'];
        File::Delete($path . $filename);
        return $activityGallery->delete();
    }

}
