<?php
       
       namespace App\Http\Helpers;

	// steps:
       // =1= to use this class
       //  =2=  composer require stichoza/google-translate-php
       //    and Easley calling this class in controller or route
       // Like
       //   =3=  $result = \App\Http\Helpers\translate::translateLang('ar','en','انا بحب مصر');
       //    =4=   return $result;
       use Stichoza\GoogleTranslate\GoogleTranslate;
       
       class translate{
       
       
       public static function translateLang($from ,$to , $text){
       
	    $tr = new GoogleTranslate();
	    
	    $result =  $tr->setSource($from)->setTarget($to)->translate($text);
     
	    return $result;
       }
       
       }