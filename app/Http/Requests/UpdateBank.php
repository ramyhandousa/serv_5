<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBank extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "country_id" => "required",
            "ar_title" => "required",
            "en_title" => "required", 
            "number" => "required",
            "owner" => "required",
            "image" => "mimes:jpeg,jpg,png,bmp,gif,svg"
        ];
    }

}
