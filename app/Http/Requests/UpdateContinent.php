<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateContinent extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            //
            "ar_name" => "required|string",
            "en_name" => "required|string",
            "ar_description" => "required|string",
            "en_description" => "required|string",
            "image" => "image|mimes:jpeg,png,jpg,|max:3048",
            "code" => "required|numeric",
            "active" => "required|string",
        ];
    }

}
