<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDriver extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            //
            
            "nickname" => "required|string",
            "city_id" => "required|string",
            "name" => "required|string",
            "email" => "required|string",
            "phone" => "required|string",
            "passport" => "required|string",
            "licence" => "required|string",
            
        ];
    }

}
