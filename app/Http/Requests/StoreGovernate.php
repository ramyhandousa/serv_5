<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreGovernate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "country_id" => "required",
            "ar_name" => "required|unique:governates,ar_name",
            "en_name" => "required|unique:governates,en_name",
            "ar_description" => "required",
            "en_description" => "required",
            "code" => "required",
            "image" => "required|image|mimes:jpeg,jpg,png,bmp,gif,svg",

        ];
    }
}
