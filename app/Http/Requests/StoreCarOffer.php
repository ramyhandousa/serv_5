<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCarOffer extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
    */


    public function rules() {
        return [
            
            "ar_title" => "required|string",
            "en_title" => "required|string",
            "ar_details" => "required|string",
            "en_details" => "required|string",
            "active" => "string",
            "image" => "required|image|mimes:jpeg,png,jpg,|max:3048",

        ];
    }

}
