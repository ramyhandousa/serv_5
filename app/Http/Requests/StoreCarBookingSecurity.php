<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCarBookingSecurity extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
    */

    public function rules() {
        return [
            
            "booking_id" => "required|string",
            "car_security_id" => "required|string",
            "price" => "required|string",

        ];
    }

}
