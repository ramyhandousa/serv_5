<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBookingCar extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
    */

    public function rules() {
        return [
            
            "user_id" => "required|string",
            "car_id" => "required|string",
            "city_id" => "required|string",
            "country_id" => "required|string",
            "flight_id" => "required|string",
            "bank_id" => "required|string",
            "txn_id" => "required|string",
            "airport_id" => "required|string",
            "driver_id" => "required|string",
            "date_from" => "required|string",
            "time_from" => "required|string",
            "date_to" => "string",
            "time_to" => "required|string",
            "car_delivery" => "required|string",
            "flight_no" => "required|string",
            "flight_name" => "required|string",
            "flight_date" => "required|string",
            "flight_time" => "required|string",
            "flight_from" => "required|string",
            "insurances" => "required|string",
            "price" => "required|numeric",
            "plus_price" => "required|numeric",
            "seen" => "required|string",
            "security_price" => "required|numeric",
            "addons_price" => "required|numeric",
            "extras" => "required|string",
            "days" => "required|string",
            "active" => "required|string",
            "pay_method" => "required|string",
            "owner_account" => "required|string",
            "ipan" => "required|string",
            "paypal_result" => "required|string",

        ];
    }

}
