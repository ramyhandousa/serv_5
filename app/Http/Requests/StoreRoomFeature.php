<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRoomFeature extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            //
            "ar_name" => "required|string|unique:room_features,ar_name",
            "en_name" => "required|string|unique:room_features,en_name",
            "icon" => "required",
        ];
    }

}
