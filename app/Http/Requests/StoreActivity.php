<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreActivity extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            //
            "ar_name" => "required|string",
            "en_name" => "required|string",
            "ar_description" => "required|string",
            "en_description" => "required|string",
            "lat" => "required|string",
            "lng" => "required|string",
            "ar_address" => "required|string",
            "en_address" => "required|string",
            "image" => "required|image|mimes:jpeg,png,jpg,|max:3048",
            "type_id" => "required|string",
            "city_id" => "required|string",
            "open_from" => "required|string",
            "open_to" => "required|string",
            "phone" => "required|numeric|unique:activities,phone",
            "email" => "required|email|unique:activities,email",
        ];
    }

}
