<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployee extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            //
            "first_name" => "required|string",
            "second_name" => "required|string",
            "gender" => "required|string",
            "address1" => "required|string",
            "address2" => "string",
            "address3" => "string",
            "phone" => "required|numeric|unique:employees,phone",
            "email" => "required|email|unique:employees,email",
            "date_of_birth" => "required|string",
            "dept_id" => "required|numeric",
        ];
    }

}
