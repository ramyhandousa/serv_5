<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCar extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
    */

    public function rules() {
        return [
            
            "brand_id" => "required|string",
            "model_id" => "required|string",
            "city_id" => "required",
            "count" => "required|string",
            "seats" => "required|string",
            "doors" => "required|string",
            "bags" => "required|string",
            "air" => "required|string",
            "vetis" => "required|string",
            "fuel" => "required|string",
            "size" => "required|string",
            "active" => "string",
            "paypal" => "required|string",
            "bank" => "required|string",
            "cash" => "required|string",
            "admin_sure" => "required|string",
            "price" => "required|string",
            "comision" => "required|string",
            "ar_details" => "required|string",
            "en_details" => "required|string",
            "ar_conditions" => "required|string",
            "en_conditions" => "required|string",
            "image" => "required|image|mimes:jpeg,png,jpg,|max:3048",


        ];
    }

}
