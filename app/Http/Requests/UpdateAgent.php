<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAgent extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            //
            "active" => "required",
            "branch_id" => "required",
            "category_id" => "required",
            "governate_id" => "required",
            "city_id" => "required",
            "ar_name" => "required",
            "en_name" => "required",
            "phone" => "required",
            "ar_address" => "required",
            "en_address" => "required",
            "lat"=>"required",
            "lng"=>"required",
            "email"=>"required|email",
        ];
    }

}
