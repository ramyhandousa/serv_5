<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCountry extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            //
            "ar_name" => "required|unique:countries,ar_name",
            "en_name" => "required|unique:countries,en_name",
            "ar_description" => "required",
            "en_description" => "required",
            "iso_code" => "required",
            "ar_currency" => "required",
            "en_currency" => "required",
            "code" => "required",
            "image" => "required|image|mimes:jpeg,jpg,png,bmp,gif,svg",
        ];
    }

}
