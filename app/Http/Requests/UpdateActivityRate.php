<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateActivityRate extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            //
            "rate_title" => "required|string",
            "rate_description" => "required|string",
            "user_id" => "required|string",
            "activity_id" => "required|string",
            "rate_no" => "required|numeric",
        ];
    }

}
