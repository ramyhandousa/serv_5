<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRestaurant extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $id = $this->segment(3);

        return [
            //
            "active" => "required|string",
            "city_id" => "required|string",
            "category_id" => "required|string",
            "ar_name" => "required|string",
            "en_name" => "required|string",
            "latitude" => "required|string",
            "longitude" => "required|string",
            "ar_overview" => "required|string",
            "en_overview" => "required|string",
            "image" => "image|mimes:jpeg,png,jpg,|max:3048",
            "phone" => "required|numeric",
            "ar_address" => "required|string",
            "en_address" => "required|string",
            "facebook" => "required|string",
            "website" => "required|string",
            "delivery" => "required|string",
            "work_from" => "required|string",
            "work_to" => "required|string",
            "email" => "required|email|unique:restaurants,email,$id",
        ];
    }

}
