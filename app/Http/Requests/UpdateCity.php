<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCity extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        return [
            //
            "active" => "required",
            "country_id" => "required",
            "governate_id" => "required",
            "ar_name" => "required",
            "en_name" => "required",
            "ar_description" => "required|string",
            "en_description" => "required|string",
            "lat" => "required|string",
            "lng" => "required|string",
            "ar_overview" => "required|string",
            "en_overview" => "required|string",
            "image" => "image|mimes:jpeg,png,jpg,|max:3048",
            "code" => "required",
        ];
    }
}
