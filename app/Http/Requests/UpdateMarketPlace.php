<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMarketPlace extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $id = $this->segment(3);

        return [
            //
            "active" => "required|string",
            "ar_name" => "required|string",
            "en_name" => "required|string",
            "latitude" => "required|string",
            "longitude" => "required|string",
            "ar_overview" => "required|string",
            "en_overview" => "required|string",
            "city_id" => "required|string",
            "image" => "image|mimes:jpeg,png,jpg,|max:3048",
            "phone" => "required|numeric",
            "ar_address" => "required|string",
            "en_address" => "required|string",
            "email" => "required|string|unique:market_places,email,$id",
            "no_of_shops" => "required|numeric|unique:market_places,phone,$id",
        ];
    }

}
