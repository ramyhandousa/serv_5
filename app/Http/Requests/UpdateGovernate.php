<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateGovernate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        return [
            //
            "active" => "required",
            "country_id" => "required",
            "ar_name" => "required|unique:governates,ar_name,$id",
            "en_name" => "required|unique:governates,en_name,$id",
            "ar_description" => "required",
            "en_description" => "required",
            "code" => "required",
            "image" => "image|mimes:jpeg,jpg,png,bmp,gif,svg",

        ];
    }
}
