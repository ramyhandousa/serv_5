<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Config;
use URL;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller {

    //
    public function __construct() {
        $lang = Config::get('app.locale');
        $this->data['site'] = \App\Site::first();
        $this->data['pages'] = \App\Page::all();
    }

    public function language() {
        $lang = Config::get('app.locale');
        if ($lang == "ar") {
            Request()->session()->put('locale', 'en');
        } else {
            Request()->session()->put('locale', 'ar');
        }
        return back();
    }

    public function index() {

    }

}
