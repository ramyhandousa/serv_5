<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Country;
use App\Continent;
use App\City;
use App\Http\Requests\StoreContinent;
use App\Http\Requests\UpdateContinent;

class ContinentController extends Controller {

    //
    function __construct(Continent $Continent, Country $Country, City $City) {
        $this->continent = $Continent;
        $this->country = $Country;
        $this->city = $City;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $continents = $this->continent->getAll();
        return view('admin.continents.index', ['continents' => $continents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('admin.continents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(StoreContinent $request) {
        $file = $request['image'];
        $filename = str_random(5) . '.' . $file->getClientOriginalName();
        $destinationPath = public_path('upload/continents/');
        $file->move($destinationPath, $filename);

        $this->continent->add($request, $filename);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $continents = $this->continent->getById($id);
        return view('admin.continents.show', ['continents' => $continents]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $continent = $this->continent->getById($id);
        return view('admin.continents.edit', ['continent' => $continent]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateContinent $request, $id) {
        //
        $filename = FALSE;
        if ($request->hasFile('image')) {
            $file = $request['image'];
            $filename = str_random(5) . '.' . $file->getClientOriginalName();
            $destinationPath = public_path('upload/continents/');
            $file->move($destinationPath, $filename);
        }
        $this->continent->edit($id, $request, $filename);
        $message = trans('layout.edit_suc');
        return redirect('admin/continents')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $continent = $this->continent->getById($id);

        $continent->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

}
