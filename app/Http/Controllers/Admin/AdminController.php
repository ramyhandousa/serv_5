<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Forecast\Forecast;

class AdminController extends Controller {

    //

    public function getLogin() {
        if (Auth::Check()) {
            return redirect('admin');
        }
        return view('admin.home.login');
    }

    public function postLogin() {
        $inputs = Request()->all();

        $remember = FALSE;
        if (isset($inputs['remember'])) {
            $remember = TRUE;
        }
        if (Auth::attempt(['username' => $inputs['username'], 'password' => $inputs['password']], $remember)) {
            $active = Auth::user()->active;
            $type = Auth::user()->type;
            if ($active == 'yes' && $type == "admin") {
                return redirect()->intended('admin');
            } else {
                Auth::logout();
                return back()->withInput()->withError(trans('layout.not_allow'));
            }
        } else {
            return back()->withInput()->withError(trans('layout.wrong_login'));
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('admin/login');
    }

    public function index() {

        if (!Auth::Check()) {
            return redirect('admin/login');
        }
        $forecast = new Forecast('12134e5aceddaa239b03196e95e54cdc');
        $weather = $forecast->get('29.5', '47.75', null, ['units' => 'si']);
        $data['temperatue'] = round($weather->currently->temperature, 0, PHP_ROUND_HALF_UP);
        $data['summary'] = $weather->currently->summary;

        return view('admin.home.index', ['data' => $data]);
    }

    public function not_allow() {
        return view('admin.home.deny');
    }



}
