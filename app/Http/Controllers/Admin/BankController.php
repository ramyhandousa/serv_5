<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Bank;
use Illuminate\Http\Request;
use App\Http\Requests\StoreBank;
use App\Http\Requests\UpdateBank;
use App\Country;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct(Country $Country,Bank $Bank) {
        $this->country = $Country;

        $this->Bank = $Bank;
    }

    public function index()
    {
        $bank = $this->Bank->getAll();
        return View('admin.bank.index', ['banks' => $bank]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);
         return View('admin.bank.create',['countries' => $countries]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBank $request)
    {
            $file = $request['image'];
            $filename = str_random(5) . '.' . $file->getClientOriginalName();
            $destinationPath = public_path('upload/bank/');
            // Upload Original version
            $file->move($destinationPath, $filename);
            $this->Bank->add($request, $filename);
            $message = trans('layout.add_suc');
            return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function show(Bank $bank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bank = $this->Bank->getById($id);
        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);
        return View('admin.bank.edit', ['bank' => $bank,'countries'=>$countries]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBank $request,$id)
    {
        $filename=false;
        if ($request->hasFile('image')) {
                $file = $request['image'];
                $file = Input::file('image');
                $filename = str_random(5) . '.' . $file->getClientOriginalName();
                $destinationPath = public_path('upload/bank/');
                // Upload Original version
                $file->move($destinationPath, $filename);
            }
        $this->Bank->edit($id, $request, $filename);
        $message = trans('layout.edit_suc');
        return redirect('admin/bank')->with(['message' => $message]);
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank = $this->Bank->getById($id);
        $delete_status = $this->Bank->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
   
    }
}
