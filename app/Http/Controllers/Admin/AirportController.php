<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller; 
use App\Airport;
use Illuminate\Http\Request;
use App\Country;
use App\Governate;
use App\City;
use App\Http\Requests\StoreAirport;
use App\Http\Requests\UpdateAirport;


class AirportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct(Airport $airport,Country $Country, Governate $Governate, City $City) {
        
        $this->Airport = $airport;
        $this->country = $Country;
        $this->governate = $Governate;
        $this->city = $City;
       
    }

    public function index()
    {
        $airport = $this->Airport->getAll();
        return View('admin.airport.index', ['airport' => $airport]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Rairzesponse
     */
    public function create()
    {
        //
        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);
        return view('admin.airport.create', ['countries' => $countries]);
   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAirport $request)
    {
        //
        $this->Airport->add($request);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Airport  $airport
     * @return \Illuminate\Http\Response
     */
    public function show(Airport $airport)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Airport  $airport
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $airport = $this->Airport->getById($id);
        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);
        return view('admin.airport.edit', ['countries' => $countries,'airport'=>$airport]);
   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Airport  $airport
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAirport $request,$id)
    {
        //
        $this->Airport->edit($id, $request);
        $message = trans('layout.edit_suc');
        return redirect('admin/airports')->with(['message' => $message]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Airport  $airport
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $airport = $this->Airport->getById($id);
        $this->Airport->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }
}
