<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreMarketPlace;
use App\Http\Requests\UpdateMarketPlace;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MarketPlace;
use App\Country;
use App\City;

class MarketsController extends Controller {

    //
    function __construct(MarketPlace $MarketPlace,Country $country, City $City) {
        $this->MarketPlace = $MarketPlace;
        $this->city = $City;
        $this->country = $country;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $markets= $this->MarketPlace->getAll();
        return view('admin.markets.index', ['markets' => $markets]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //

        $countries = $this->country->getList();
        $countries= array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);

        return view('admin.markets.create', ['countries' => $countries ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(StoreMarketPlace $request) {
        $file = $request['image'];
        $filename = str_random(5) . '.' . $file->getClientOriginalName();
        $destinationPath = public_path('upload/markets/');
        $file->move($destinationPath, $filename);

        $this->MarketPlace->add($request, $filename);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $from = Request()->get('from');
        $cities = $this->MarketPlace->getListCity($id);
        $cities = array_add($cities, '', trans('layout.choose_city'));
        $cities = array_reverse($cities, TRUE);
        return view('admin.activities.show', ['cities' => $cities,'from' => $from]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $MarketPlace = $this->MarketPlace->getById($id);

        $countries = $this->country->getList();
        $countries= array_add($countries, '', trans('layout.choose_city'));
        $countries = array_reverse($countries, TRUE);

        $cities = $this->city->getList();
        $cities= array_add($cities, '', trans('layout.choose_city'));
        $cities = array_reverse($cities, TRUE);


        return view('admin.markets.edit', ['MarketPlace' => $MarketPlace,'countries' => $countries,'cities' => $cities]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMarketPlace $request, $id) {
        //
        $filename = FALSE;
        if ($request->hasFile('image')) {
            $file = $request['image'];
            $filename = str_random(5) . '.' . $file->getClientOriginalName();
            $destinationPath = public_path('upload/markets/');
            $file->move($destinationPath, $filename);
        }
        $this->MarketPlace->edit($id, $request, $filename);
        $message = trans('layout.edit_suc');
        return redirect('admin/market-places')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $Activity = $this->MarketPlace->getById($id);
        $Activity->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

}
