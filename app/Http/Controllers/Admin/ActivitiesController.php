<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreActivity;
use App\Http\Requests\UpdateActivity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Activity;
use App\ActivityType;
use App\Country;
use App\City;
use App\Http\Requests\UpdateActivityType;

class ActivitiesController extends Controller {

    //
    function __construct(Activity $Activity,ActivityType $ActivityType,Country $country, City $City) {
        $this->Activity = $Activity;
        $this->ActivityType = $ActivityType;
        $this->city = $City;
        $this->country = $country;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $Activities = $this->Activity->getAll();
        return view('admin.activities.index', ['Activities' => $Activities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //

        $countries = $this->country->getList();
        $countries= array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);
        $activity_types = $this->ActivityType->getList();
        $activity_types= array_add($activity_types, '', trans('layout.choose_activity_type'));
        $activity_types = array_reverse($activity_types, TRUE);
        return view('admin.activities.create', ['countries' => $countries,'activity_types' => $activity_types ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(StoreActivity $request) {
        $file = $request['image'];
        $filename = str_random(5) . '.' . $file->getClientOriginalName();
        $destinationPath = public_path('upload/activities/');
        $file->move($destinationPath, $filename);

        $this->Activity->add($request, $filename);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $from = Request()->get('from');
        $cities = $this->city->getListCity($id);
        $cities = array_add($cities, '', trans('layout.choose_city'));
        $cities = array_reverse($cities, TRUE);
        return view('admin.activities.show', ['cities' => $cities,'from' => $from]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $Activity = $this->Activity->getById($id);

        $countries = $this->country->getList();
        $countries= array_add($countries, '', trans('layout.choose_city'));
        $countries = array_reverse($countries, TRUE);

        $cities = $this->city->getList();
        $cities= array_add($cities, '', trans('layout.choose_city'));
        $cities = array_reverse($cities, TRUE);

        $activity_types = $this->ActivityType->getList();
        $activity_types= array_add($activity_types, '', trans('layout.choose_activity_type'));
        $activity_types = array_reverse($activity_types, TRUE);

        return view('admin.activities.edit', ['Activity' => $Activity,'countries' => $countries,'cities' => $cities,'activity_types' => $activity_types ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateActivity $request, $id) {
        //
        $filename = FALSE;
        if ($request->hasFile('image')) {
            $file = $request['image'];
            $filename = str_random(5) . '.' . $file->getClientOriginalName();
            $destinationPath = public_path('upload/activities/');
            $file->move($destinationPath, $filename);
        }
        $this->Activity->edit($id, $request, $filename);
        $message = trans('layout.edit_suc');
        return redirect('admin/activities')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $Activity = $this->Activity->getById($id);
        $Activity->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

}
