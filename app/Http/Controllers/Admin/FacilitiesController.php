<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Facility;
use App\FacilityOption;
use App\Http\Requests\StoreFacility;
use App\Http\Requests\UpdateFacility;

class FacilitiesController extends Controller {

    //
    function __construct(Facility $Facility, FacilityOption $FacilityOption) {
        $this->facility = $Facility;
        $this->facility_option = $FacilityOption;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $facilities = $this->facility->getAll();
        return view('admin.facilities.index', ['facilities' => $facilities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        return view('admin.facilities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFacility $request) {
        $new = $this->facility->add($request);
        if (isset($request['ar_options'])) {
            $i = 0;
            $ar_options = $request['ar_options'];
            $en_options = $request['en_options'];
            foreach ($ar_options as $ar_option) {
                $data['ar_option'] = $ar_option;
                $data['en_option'] = $en_options[$i];
                $this->facility_option->add($data, $new);
                $i++;
            }
        }
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $facility = $this->facility->getById($id);
        return view('admin.facilities.edit', ['facility' => $facility]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFacility $request, $id) {
        //
        $this->facility->edit($id, $request);
        $this->facility_option->delByFacility($id);
        if (isset($request['ar_options'])) {
            $i = 0;
            $ar_options = $request['ar_options'];
            $en_options = $request['en_options'];
            foreach ($ar_options as $ar_option) {
                $data['ar_option'] = $ar_option;
                $data['en_option'] = $en_options[$i];
                $this->facility_option->add($data, $id);
                $i++;
            }
        }
        $message = trans('layout.edit_suc');
        return redirect('admin/facilities')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $this->facility->remove($id);
        $this->facility_option->delByFacility($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

}
