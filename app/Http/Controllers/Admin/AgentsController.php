<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Agent;
use App\Branch;
use App\AgentCategory;
use App\Governate;
use App\City;
use App\Http\Requests\StoreAgent;
use App\Http\Requests\UpdateAgent;

class AgentsController extends Controller {

    //
    function __construct(Agent $Agent,Branch $Branch,  AgentCategory $AgentCategory, Governate $Governate, City $City) {
        $this->agent = $Agent;
        $this->branch = $Branch;
        $this->agent_category = $AgentCategory;
        $this->governate = $Governate;
        $this->city = $City;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $agents = $this->agent->getAll();
        return view('admin.agents.index', ['agents' => $agents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        $agent_categories = $this->agent_category->getList();
        $agent_categories = array_add($agent_categories, '', trans('layout.choose_category'));
        $agent_categories = array_reverse($agent_categories, TRUE);

        $branches = $this->branch->getList();
        $branches = array_add($branches, '', trans('layout.choose_branch'));
        $branches = array_reverse($branches, TRUE);

        return view('admin.agents.create', ['agent_categories' => $agent_categories, 'branches' => $branches]);
    }

    /**
     * AgentCategory a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAgent $request) {
        $this->agent->add($request);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $agent = $this->agent->getById($id);

        $agent_categories = $this->agent_category->getList();
        $agent_categories = array_add($agent_categories, '', trans('layout.choose_category'));
        $agent_categories = array_reverse($agent_categories, TRUE);

        $branches = $this->branch->getList();
        $branches = array_add($branches, '', trans('layout.choose_branch'));
        $branches = array_reverse($branches, TRUE);

        $governates = $this->governate->getListCountry($agent['Branch']['country_id']);
        $governates = array_add($governates, '', trans('layout.choose_governate'));
        $governates = array_reverse($governates, TRUE);

        $cities = $this->city->getListGovernate($agent['governate_id']);
        $cities = array_add($cities, '', trans('layout.choose_governate'));
        $cities = array_reverse($cities, TRUE);

        return view('admin.agents.edit', ['agent' => $agent, 'agent_categories' => $agent_categories, 'governates' => $governates, 'cities' => $cities, 'branches' => $branches]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAgent $request, $id) {
        //
        $this->agent->edit($id, $request);
        $message = trans('layout.edit_suc');
        return redirect('admin/agents')->with(['message' => $message]);
    }

    public function destroy($id) {
        $this->agent->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

}
