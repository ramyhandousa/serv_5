<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller; 
use App\CarModel;
use Illuminate\Http\Request;
use App\CarBrand;
use App\Car;
use App\City;
use App\CarSecurity;
use App\CarBooking;
use App\CarBookingAddon;
use App\CarBookingSecurity;
use App\Http\Requests\StoreModelCar;
use App\Http\Requests\UpdateModelCar;

class CarModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct(CarBrand $brand, CarModel $model, Car $car, CarSecurity $car_security, CarBooking $booking, CarBookingAddon $booking_addon, CarBookingSecurity $booking_security) {
        
        $this->Brand = $brand;
        $this->Model = $model;
        $this->Car = $car;
        $this->CarSecurity = $car_security;
        $this->Booking = $booking;
        $this->BookingAddon = $booking_addon;
        $this->BookingSecurity = $booking_security;
       
    }

    public function index()
    { 
        //
        $models = $this->Model->getAll();
        return View('admin.brand-model.index',['models' => $models]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = $this->Brand->getListLang();
        $brands = array_add($brands, '', trans('layout.choose_brand'));
        $brands = array_reverse($brands, TRUE);
        return View('admin.brand-model.create',['brands' => $brands]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreModelCar $request)
    {
        $this->Model->add($request);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarModel  $carModel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarModel  $carModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brands = $this->Brand->getListLang();
        $model = $this->Model->getById($id);
        return View('admin.brand-model.edit',['brands'=>$brands,'model' => $model]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarModel  $carModel
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateModelCar $request, $id)
    {
        $this->Model->edit($id,$request);
        $message = trans('layout.edit_suc');
        return redirect('admin/models')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarModel  $carModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->Model->getById($id);
            
            $this->Model->remove($id);
            foreach ($model->Cars as $car) {
                $this->Car->remove($car->id);
                $this->CarSecurity->removeByCar($car->id);
                foreach ($car->Bookings as $booking) {
                    $this->Booking->remove($booking->id);
                    $this->BookingSecurity->removeByBooking($booking->id);
                    $this->BookingAddon->removeByBooking($booking->id);
                }
            }
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }
}
