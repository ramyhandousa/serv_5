<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Country;
use App\Governate;
use App\City;
use App\Http\Requests\StoreGovernate;
use App\Http\Requests\UpdateGovernate;

class GovernateController extends Controller {

    //
    function __construct(Country $Country, Governate $Governate, City $City) {
        $this->country = $Country;
        $this->governate = $Governate;
        $this->city = $City;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $governates = $this->governate->getAll();
        return view('admin.governates.index', ['governates' => $governates]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);
        return view('admin.governates.create', ['countries' => $countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGovernate $request) {

        $file = $request['image'];
        $filename = str_random(5) . '.' . $file->getClientOriginalName();
        $destinationPath = public_path('upload/governates/');
        $file->move($destinationPath, $filename);
        $this->governate->add($request, $filename);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $governate = $this->governate->getById($id);
        return view('admin.governates.show', ['governate' => $governate]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);

        $governate = $this->governate->getById($id);
        return view('admin.governates.edit', ['governate' => $governate, 'countries' => $countries]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGovernate $request, $id) {
        //
        $filename = FALSE;
        if ($request->hasFile('image')) {
            $file = $request['image'];
            $filename = str_random(5) . '.' . $file->getClientOriginalName();
            $destinationPath = public_path('upload/countries/');
            $file->move($destinationPath, $filename);
        }
        $this->governate->edit($id, $request, $filename);
        $message = trans('layout.edit_suc');
        return redirect('admin/governates')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $governate = $this->governate->getById($id);
        foreach ($governate['Cities'] as $city) {
            $this->city->remove($city['id']);
        }
        $this->governate->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

    public function getGovernates(Request $request, $id) {
        $branch = \App\Branch::find($request['branch_id']);

        $governates = $this->governate->getListCountry($id);
        $governates = array_add($governates, '', trans('layout.choose_governate'));
        $governates = array_reverse($governates, TRUE);

        return View('admin.governates.ajax', ['governates' => $governates, 'branch' => $branch]);
    }

    public function getGovernatesAgent(Request $request, $id) {
        $agent = \App\Agent::find($request['agent_id']);

        $branch = \App\Branch::find($id);
        $governates = $this->governate->getListCountry($branch['country_id']);
        $governates = array_add($governates, '', trans('layout.choose_governate'));
        $governates = array_reverse($governates, TRUE);

        return View('admin.governates.ajax_agent', ['governates' => $governates, 'agent' => $agent]);
    }

}
