<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ActivityType;

use App\Http\Requests\StoreActivityType;
use App\Http\Requests\UpdateActivityType;

class ActivityTypeController extends Controller {

    //
    function __construct(ActivityType $ActivityType) {
        $this->ActivityType = $ActivityType;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $cities = $this->ActivityType->getAll();
        return view('admin.activity-type.index', ['cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //

        return view('admin.activity-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreActivityType $request) {
        $this->ActivityType->add($request);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $from = Request()->get('from');
        $governates = $this->ActivityType->getListCountry($id);
        $governates = array_add($governates, '', trans('layout.choose_governate'));
        $governates = array_reverse($governates, TRUE);
        return view('admin.activity-type.show', ['governates' => $governates, 'from' => $from]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $ActivityType = $this->ActivityType->getById($id);

        return view('admin.activity-type.edit', ['ActivityType' => $ActivityType,]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateActivityType $request, $id) {
        //
        $this->ActivityType->edit($id, $request);
        $message = trans('layout.edit_suc');
        return redirect('admin/activity-types')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $this->ActivityType->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

}
