<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller; 
use App\Driver;
use App\Country;
use App\Governate;
use App\City;
use Illuminate\Http\Request;
use App\Http\Requests\StoreDriver;
use App\Http\Requests\UpdateDriver;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct(Driver $driver,Country $Country, Governate $Governate, City $City) {
        
        $this->Driver = $driver;
        $this->country = $Country;
        $this->governate = $Governate;
        $this->city = $City;
       
    }

    public function index()
    {
        $drivers=$this->Driver->getAll();
        return view('admin.car-drivers.index', ['drivers' => $drivers]);

    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);
        return view('admin.car-drivers.create', ['countries' => $countries]);
   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDriver $request)
    {
        //
        $this->Driver->add($request);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {
        
       }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);
        $driver=$this->Driver->getById($id);

        return view('admin.car-drivers.edit', ['driver'=>$driver,'countries' => $countries]);    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDriver $request,$id)
    {
        $this->Driver->edit($id,$request);
        $message = trans('layout.edit_suc');
        return redirect('admin/drivers')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver $driver)
    {
        //
    }
}
