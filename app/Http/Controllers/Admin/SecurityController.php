<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Security;
use Illuminate\Http\Request;
use App\Http\Requests\StoreSecurity;
use App\Http\Requests\UpdateSecurity;

class SecurityController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(Security $Security) {
        $this->Security = $Security;
    }

    public function index() {
        $security = $this->Security->getAll();
        return View('admin.car-security.index', ['security' => $security]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return View('admin.car-security.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSecurity $request) {
        $this->Security->add($request);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarAddon  $carAddon
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarAddon  $carAddon
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $security = $this->Security->getById($id);

        return View('admin.car-security.edit', ['security' => $security]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarAddon  $carAddon
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSecurity $request, $id) {

        $this->Security->edit($id, $request);
        $message = trans('layout.edit_suc');
        return redirect('admin/security')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarAddon  $carAddon
     * @return \Illuminate\Http\Response
     */
    public function destroy(CarAddon $carAddon) {
        
    }

}
