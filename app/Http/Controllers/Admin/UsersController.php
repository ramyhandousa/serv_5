<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Country;
use App\Http\Requests\UpdateUser;

class UsersController extends Controller {

    //
    function __construct(User $User, Country $Country) {
        $this->user = $User;
        $this->country = $Country;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $users = $this->user->getType('user');
        return view('admin.users.index', ['users' => $users]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);

        $user = $this->user->getById($id);
        $this->user->edit_seen($id);
        return view('admin.users.edit', ['user' => $user, 'countries' => $countries]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser $request, $id) {
        //
        $this->user->edit($id, $request);
        $message = trans('layout.edit_suc');
        return redirect('admin/users')->with(['message' => $message]);
    }

}
