<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Car;
use App\City;
use App\CarBrand;
use App\CarSecurity;
use App\CarBooking;
use App\CarBookingAddon;
use App\CarBookingSecurity;
use App\Http\Requests\StoreCar;
use App\Http\Requests\UpdateCar;
use Illuminate\Http\Request;

class CarController extends Controller {

    function __construct(Car $car, City $city, CarBrand $brand, CarSecurity $car_security, CarBooking $booking, CarBookingAddon $booking_addon, CarBookingSecurity $booking_security) {
        $this->Car = $car;
        $this->City = $city;
        $this->Brand = $brand;
        $this->CarSecurity = $car_security;
        $this->Booking = $booking;
        $this->BookingAddon = $booking_addon;
        $this->BookingSecurity = $booking_security;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //view cars
        $cars = $this->Car->getAll();
        return View('admin.cars.index', ['cars' => $cars]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        $city = $this->City->getList();
        $city = array_reverse($city, TRUE);
        $brand = $this->Brand->getList();
        $brand = array_add($brand, '', 'إختر الماركة');
        $brand = array_reverse($brand, TRUE);
        $security = CarSecurity::where('active', 'yes')->get();

        return View('admin.cars.create', ['security' => $security, 'cities' => $city, 'brand' => $brand]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCar $request) {
        $file = $request['image'];
        $filename = time() . '.' . str_slug($file->getClientOriginalName());
        $destinationPath = public_path('upload/cars/');
        // Upload Original version
        $file->move($destinationPath, $filename);
        $cities = $request->input('city_id');

        foreach ($cities as $key => $city) {


            $car = new Car();
            $car->paypal = $request['paypal'];
            $car->cash = $request['cash'];
            $car->bank = $request['bank'];
            $car->admin_sure = $request['admin_sure'];
            $car->brand_id = $request['brand_id'];
            $car->model_id = $request['model_id'];
            $car->city_id = $city;
            $car->count = $request['count'];
            $car->seats = $request['seats'];
            $car->doors = $request['doors'];
            $car->bags = $request['bags'];
            $car->air = $request['air'];
            $car->vetis = $request['vetis'];
            $car->fuel = $request['fuel'];
            $car->size = $request['size'];
            $car->price = $request['price'];
            $car->comision = $request['comision'];
            $car->ar_details = $request['ar_details'];
            $car->en_details = $request['en_details'];
            $car->ar_conditions = $request['ar_conditions'];
            $car->en_conditions = $request['en_conditions'];
            $car->image = $filename;
            $car->save();
            $car_id = $car->id;
            $security_id = $request->input('security_id');

            if (isset($security_id)) {

                foreach ($security_id as $security) {
                    $d['car_id'] = $car_id;
                    $d['security_id'] = $security;
                    $d['price'] = $request->input('security_id');
                    $this->CarSecurity->add($d);
                }
            }
        }

        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $car = $this->Car->getById($id);
        return View('admin.cars.show', ['car' => $car]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $car = $this->Car->getById($id);

        $city = $this->City->getList();
        $city = array_add($city, '', 'إختر المدينة');
        $city = array_reverse($city, TRUE);

        $brand = $this->Brand->getList();
        $brand = array_add($brand, '', 'إختر الماركة');
        $brand = array_reverse($brand, TRUE);

        return View('admin.cars.edit', ['car' => $car, 'cities' => $city, 'brand' => $brand]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCar $request, $id) {
        $filename = False;

        if ($request->hasFile('image')) {
            $file = $request['image'];
            $filename = str_random(5) . '.' . $file->getClientOriginalName();
            $destinationPath = public_path('upload/cars/');
            $file->move($destinationPath, $filename);
        }

        $car_saved = $this->Car->edit($id, $request, $filename);

        if ($car_saved) {
            $this->CarSecurity->removeByCar($id);
            if (isset($inputs['security_id'])) {
                for ($i = 0; $i < count($inputs['security_id']); $i++) {
                    $d['car_id'] = $id;
                    $d['security_id'] = $inputs['security_id'][$i];
                    $d['price'] = $inputs['security_price'][$i];
                    $this->CarSecurity->add($d);
                }
            }
        }


        $message = trans('layout.edit_suc');
        return redirect('admin/cars')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $car = $this->Car->getById($id);
        $delete_status = $this->Car->remove($id);

        $this->CarSecurity->removeByCar($id);

        foreach ($car->Bookings as $booking) {
            $this->Booking->remove($booking->id);
            $this->BookingSecurity->removeByBooking($booking->id);
            $this->BookingAddon->removeByBooking($booking->id);
        }

        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

}
