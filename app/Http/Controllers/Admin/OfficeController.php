<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Office;
use Illuminate\Http\Request;
use App\Http\Requests\StoreOffice;
use App\Http\Requests\UpdateOffice;
class OfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */

    function __construct(Office $Office) {
        $this->Office = $Office;
    }

    public function index()
    {
        $office = $this->Office->getAll();
        return View('admin.offices.index', ['offices' => $office]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {//
        return View('admin.offices.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOffice $request)
    {
        $this->Office->add($request);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function show(Office $office)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $office = $this->Office->getById($id);
        return View('admin.offices.edit', ['office' => $office]);
   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOffice $request,$id)
    {
        //
        $this->Office->edit($id, $request);
        $message = trans('layout.edit_suc');
        return back()->with(['message' => $message]);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Office  $office
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Office = $this->Office->getById($id);
        $delete_status = $this->Office->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }
}
