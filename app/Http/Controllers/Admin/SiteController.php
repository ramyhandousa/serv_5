<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Site;
use App\Http\Requests\UpdateSite;

class SiteController extends Controller {

    //
    function __construct(Site $Site) {
        $this->site = $Site;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $site = $this->site->getData();
        return view('admin.site.index', ['site' => $site]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSite $request, $id) {
        $this->site->edit($request);
        $message = trans('layout.edit_suc');
        return back()->with(['message' => $message]);
    }

}
