<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\CityDifferent;
use App\City;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCityDifferent;
use App\Http\Requests\UpdateCityDifferent;
class CityDifferentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct(CityDifferent $CityDifferent, City $city) {
        $this->CityDifferent = $CityDifferent;
        $this->City= $city;
    }

    public function index()
    {
        $CityDifferent = $this->CityDifferent->getAll();
        return View('admin.city-different.index', ['CityDifferent' => $CityDifferent]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $city = $this->City->getList();
        $city = array_add($city, '', 'إختر المدينة');
        $city = array_reverse($city, TRUE);
        return View('admin.city-different.create',['city' => $city]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCityDifferent $request)
    {
        $this->CityDifferent->add($request);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CityDifferent  $cityDifferent
     * @return \Illuminate\Http\Response
     */
    public function show(CityDifferent $cityDifferent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CityDifferent  $cityDifferent
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {

        $CityDifferent = $this->CityDifferent->getById($id);
        $city = $this->City->getList();
        $city = array_add($city, '', 'إختر المدينة');
        $city = array_reverse($city, TRUE);
        return View('admin.city-different.edit', ['CityDifferent' => $CityDifferent, 'city' => $city]);
   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CityDifferent  $cityDifferent
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCityDifferent $request,$id)
    {
        $this->CityDifferent->edit($id,$request);
        $message = trans('layout.edit_suc');
        return redirect('admin/cars_city')->with(['message' => $message]);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CityDifferent  $cityDifferent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->CityDifferent->getById($id);
        $this->CityDifferent->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }
}
