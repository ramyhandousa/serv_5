<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Country;
use App\Group;
use App\Http\Requests\StoreAdmin;
use App\Http\Requests\UpdateAdmin;

class AdminsController extends Controller {

    //
    function __construct(User $User, Country $Country, Group $Group) {
        $this->user = $User;
        $this->country = $Country;
        $this->group = $Group;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $admins = $this->user->getType('admin');
        return view('admin.admins.index', ['admins' => $admins]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        $groups = $this->group->getList();
        $groups = array_add($groups, '', trans('layout.choose_group'));
        $groups = array_reverse($groups, TRUE);

        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);
        return view('admin.admins.create', ['groups' => $groups, 'countries' => $countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdmin $request) {
        $this->user->add_admin($request);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $admin = $this->user->getById($id);

        $groups = $this->group->getList();
        $groups = array_add($groups, '', trans('layout.choose_group'));
        $groups = array_reverse($groups, TRUE);

        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);
        return view('admin.admins.edit', ['admin' => $admin, 'groups' => $groups, 'countries' => $countries]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdmin $request, $id) {
        //
        $this->user->edit_admin($id, $request);
        $message = trans('layout.edit_suc');
        return redirect('admin/admins')->with(['message' => $message]);
    }

}
