<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\CarBooking;
use Illuminate\Http\Request;
use App\CarModel;
use App\CarSecurity;
use App\CarBookingAddon;
use App\CarBookingSecurity;
use App\CarBrand;
use App\Car;
use App\City;
use App\Airport;
use App\Driver;

class CarBookingController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(CarBrand $brand, Driver $Driver, Airport $Airport, City $city, CarModel $model, Car $car, CarSecurity $car_security, CarBooking $booking, CarBookingAddon $booking_addon, CarBookingSecurity $booking_security) {

        $this->Brand = $brand;
        $this->Models = $model;
        $this->Car = $car;
        $this->CarSecurity = $car_security;
        $this->City = $city;
        $this->Airport = $Airport;
        $this->Driver = $Driver;
        $this->Booking = $booking;
        $this->BookingAddon = $booking_addon;
        $this->BookingSecurity = $booking_security;
    }

    public function index(Request $request) {
        $active = $request['active'];
        $bookings = $this->Booking->getActive($active);
        return View('admin.car-booking.index', ['bookings' => $bookings, 'active' => $active]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $this->data['cars'] = $this->Car->getAll();
        $this->data['cities'] = $this->City->getAll();
        $this->data['airports'] = $this->Airport->getAll();
        $this->data['drivers'] = $this->Driver->getAll();

        return View('admin.car-booking.create', ['data' => $this->data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request['user_id'] = 0;
        // dd($inputs['driver_overage']);
        if (!$request->has('driver_overage')) {
            $request['driver_overage'] = 'off';
        }
        if (!$request->has('security_price')) {
            $request['security_price'] = 0;
        }
        if (!$request->has('addons_price')) {
            $request['addons_price'] = 0;
        }
        if ($request->has('extras') == null) {
            $request['extras'] = ' ';
        }
        $this->Booking->add($request);
        $message = trans('layout.edit_suc');
        return redirect('admin/cars_booking')->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarBooking  $carBooking
     * @return \Illuminate\Http\Response
     */
    public function show(CarBooking $carBooking) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarBooking  $carBooking
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $booking = $this->Booking->getById($id);
        $this->Booking->seen($id);
        return View('admin.car-booking.edit', ['booking' => $booking]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarBooking  $carBooking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $booking = $this->Booking->getById($id);
        $booking_saved = $this->Booking->edit($id, $inputs);
        $message = trans('layout.edit_suc');

        return redirect('admin/cars_booking?active=' . $booking['active'])->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarBooking  $carBooking
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $booking = $this->Booking->getById($id);
        $delete_status = $this->Booking->remove($id);
        if ($delete_status) {
            $this->BookingSecurity->removeByBooking($id);
            $this->BookingAddon->removeByBooking($id);
        }
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

}
