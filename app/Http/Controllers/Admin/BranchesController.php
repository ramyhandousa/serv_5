<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Branch;
use App\Country;
use App\Governate;
use App\City;
use App\Http\Requests\StoreBranch;
use App\Http\Requests\UpdateBranch;

class BranchesController extends Controller {

    //
    function __construct(Branch $Branch, Country $Country, Governate $Governate, City $City) {
        $this->branch = $Branch;
        $this->country = $Country;
        $this->governate = $Governate;
        $this->city = $City;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $branches = $this->branch->getAll();
        return view('admin.branches.index', ['branches' => $branches]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);

        return view('admin.branches.create', ['countries' => $countries]);
    }

    /**
     * Country a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBranch $request) {
        $this->branch->add($request);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $branch = $this->branch->getById($id);

        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);

        $governates = $this->governate->getListCountry($branch['country_id']);
        $governates = array_add($governates, '', trans('layout.choose_governate'));
        $governates = array_reverse($governates, TRUE);

        $cities = $this->city->getListGovernate($branch['governate_id']);
        $cities = array_add($cities, '', trans('layout.choose_governate'));
        $cities = array_reverse($cities, TRUE);

        return view('admin.branches.edit', ['branch' => $branch, 'countries' => $countries, 'governates' => $governates, 'cities' => $cities]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBranch $request, $id) {
        //
        $this->branch->edit($id, $request);        
        $message = trans('layout.edit_suc');
        return redirect('admin/branches')->with(['message' => $message]);
    }
    
    public function destroy($id){
        $this->branch->remove($id);        
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);        
    }

}
