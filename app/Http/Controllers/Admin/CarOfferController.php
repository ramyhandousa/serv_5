<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\CarOffer;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCarOffer;
use App\Http\Requests\UpdateCarOffer;

class CarOfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct(CarOffer $Offer) {
        
        $this->Offer = $Offer;
       
    }
    public function index()
    {
        $offers = $this->Offer->getAll();
        return View('admin.car-offers.index', ['offers' => $offers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.car-offers.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCarOffer $request)
    {
        $file =$request['image'];
        $filename = str_random(5) . '.' . $file->getClientOriginalName();
        $destinationPath = public_path('upload/offers/');
        // Upload Original version
        $file->move($destinationPath, $filename);
        $this->Offer->add($request, $filename);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarOffer  $carOffer
     * @return \Illuminate\Http\Response
     */
    public function show(CarOffer $carOffer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarOffer  $carOffer
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $offer = $this->Offer->getById($id);
        return View('admin.car-offers.edit', ['offer' => $offer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarOffer  $carOffer
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCarOffer $request, $id)
    {
        $filename = False;
        if ($request->hasFile('image')) {
                $file = $request['image'];
                $filename = str_random(5) . '.' . $file->getClientOriginalName();
                $destinationPath = public_path('upload/offers/');
                // Upload Original version
                $file->move($destinationPath, $filename);
        }
        $photos_saved = $this->Offer->edit($id, $request, $filename);
        $message = trans('layout.edit_suc');
        return redirect('admin/offers')->with(['message' => $message]);
           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarOffer  $carOffer
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        $offers = $this->Offer->getById($id);
        $delete_status = $this->Offer->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }
}
