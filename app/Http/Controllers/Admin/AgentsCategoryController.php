<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AgentCategory;
use App\Http\Requests\StoreAgentCategory;
use App\Http\Requests\UpdateAgentCategory;
use Illuminate\Support\Facades\Auth;

class AgentsCategoryController extends Controller {

    //
    function __construct(AgentCategory $AgentCategory) {
        $this->agent_category = $AgentCategory;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $agents_categories = $this->agent_category->getAll();
        return view('admin.agents_categories.index', ['agents_categories' => $agents_categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //            
        return view('admin.agents_categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAgentCategory $request) {
        $this->agent_category->add($request);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $agent_category = $this->agent_category->getById($id);
        return view('admin.agents_categories.edit', ['agent_category' => $agent_category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAgentCategory $request, $id) {
        //
        $this->agent_category->edit($id, $request);
        $message = trans('layout.edit_suc');
        return redirect('admin/agents_categories')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $this->agent_category->remove($id);       
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

}
