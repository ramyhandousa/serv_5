<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\RestaurantCategory;

use App\Http\Requests\StoreRestaurantCategory;
use App\Http\Requests\UpdateRestaurantCategory;

class RestaurantCategoriesController extends Controller {

    //
    function __construct(RestaurantCategory $RestaurantCategory) {
        $this->RestaurantCategory = $RestaurantCategory;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $restaurant_categories = $this->RestaurantCategory->getAll();
        return view('admin.restaurant-categories.index', ['restaurant_categories' => $restaurant_categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //

        return view('admin.restaurant-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRestaurantCategory $request) {
        $this->RestaurantCategory->add($request);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $from = Request()->get('from');
        $governates = $this->RestaurantCategory->getListCountry($id);
        $governates = array_add($governates, '', trans('layout.choose_governate'));
        $governates = array_reverse($governates, TRUE);
        return view('admin.restaurant-categories.show', ['governates' => $governates, 'from' => $from]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $ActivityType = $this->RestaurantCategory->getById($id);

        return view('admin.restaurant-categories.edit', ['ActivityType' => $ActivityType,]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRestaurantCategory $request, $id) {
        //
        $this->RestaurantCategory->edit($id, $request);
        $message = trans('layout.edit_suc');
        return redirect('admin/restaurant-categories')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $this->RestaurantCategory->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

}
