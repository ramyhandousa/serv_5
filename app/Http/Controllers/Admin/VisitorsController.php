<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Visitor;
use App\Country;

class VisitorsController extends Controller {

    //
    function __construct(Visitor $Visitor, Country $Country) {
        $this->visitor = $Visitor;
        $this->country = $Country;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $visitors = $this->visitor->getAll();
        return view('admin.visitors.index', ['visitors' => $visitors]);
    }

}
