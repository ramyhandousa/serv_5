<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\CarBrand;
use App\Car;
use App\City;
use App\CarModel;
use App\CarSecurity;
use App\CarBooking;
use App\CarBookingAddon;
use App\CarBookingSecurity;
use App\Http\Requests\StoreBrandCar;
use App\Http\Requests\UpdateBrandCar;
class CarBrandController extends Controller
{
 

    function __construct(CarBrand $brand, CarModel $model, Car $car, CarSecurity $car_security, CarBooking $booking, CarBookingAddon $booking_addon, CarBookingSecurity $booking_security) {
        
        $this->Brand = $brand;
        $this->Models = $model;
        $this->Car = $car;
        $this->CarSecurity = $car_security;
        $this->Booking = $booking;
        $this->BookingAddon = $booking_addon;
        $this->BookingSecurity = $booking_security;
       
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand = $this->Brand->getAll();
        return View('admin.car-brand.index', ['brand' => $brand]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.car-brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBrandCar $request)
    {
            $file = $request['image'];
            $filename = time() . '.' . $file->getClientOriginalName();
            $destinationPath = public_path('upload/brand/');
        
        // Upload Original version

            $file->move($destinationPath, $filename);
            $this->Brand->add($request, $filename);
            $message = trans('layout.add_suc');
            return back()->with(['message' => $message]);        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
     */
    public function show(CarBrand $carBrand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand=CarBrand::findOrFail($id);
        return View('admin.car-brand.edit',['brand'=>$brand]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
    */

    public function update(UpdateBrandCar $request, $id)
    {
        $filename=false;
        if ($request->hasFile('image')) {
            $file = $request['image'];
            $filename = time() . '.' . $file->getClientOriginalName();
            $destinationPath = public_path('upload/brand/');
            
        // Upload Original version
            $file->move($destinationPath, $filename);
        }

        $photos_saved = $this->Brand->edit($id,$request, $filename);
        $message = trans('layout.edit_suc');
        return redirect('admin/brands')->with(['message' => $message]);
    
    }

    /**
    
     * Remove the specified resource from storage.
     *
     * @param  \App\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
    
    */

    public function destroy( $id)
    {
        $Brand = $this->Brand->getById($id);
        $delete_status = $this->Brand->remove($id);
            foreach ($Brand->Cars as $car) {
                $this->Car->remove($car->id);
                $this->CarSecurity->removeByCar($car->id);
                foreach ($car->Bookings as $booking) {
                    $this->Booking->remove($booking->id);
                    $this->BookingSecurity->removeByBooking($booking->id);
                    $this->BookingAddon->removeByBooking($booking->id);
                }
            }
            foreach ($Brand->Cars as $car) {
                $this->Car->remove($car->id);
                $this->CarSecurity->removeByCar($car->id);
                foreach ($car->Bookings as $booking) {
                    $this->Booking->remove($booking->id);
                    $this->BookingSecurity->removeByBooking($booking->id);
                    $this->BookingAddon->removeByBooking($booking->id);
                }
            }
            foreach ($Brand->Models as $model) {
                $this->Model->remove($model->id);
                foreach ($model->Cars as $car) {
                    $this->Car->remove($car->id);
                    $this->CarSecurity->removeByCar($car->id);
                    foreach ($car->Bookings as $booking) {
                        $this->Booking->remove($booking->id);
                        $this->BookingSecurity->removeByBooking($booking->id);
                        $this->BookingAddon->removeByBooking($booking->id);
                    }
                }
            }
            $message = trans('layout.delete_suc');
            return back()->with(['message' => $message]);
              
    }


    public function getModels(Request $request) {
        $id = $request->input('id');
        $val =  $request->input('model');
        $models = $this->Models->getList($id);
        $models = array_add($models, '', 'إختر الموديل');
        $models = array_reverse($models, TRUE);
        return View('admin.car-brand.ajax', ['models' => $models, 'model' => $val]);
    }
}
