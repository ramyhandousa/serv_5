<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Country;
use App\Governate;
use App\City;
use App\Http\Requests\StoreCity;
use App\Http\Requests\UpdateCity;

class CityController extends Controller {

    //
    function __construct(Country $Country, Governate $Governate, City $City) {
        $this->country = $Country;
        $this->governate = $Governate;
        $this->city = $City;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $cities = $this->city->getAll();
        return view('admin.cities.index', ['cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);
        return view('admin.cities.create', ['countries' => $countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCity $request) {

        $file = $request['image'];
        $filename = str_random(5) . '.' . $file->getClientOriginalName();
        $destinationPath = public_path('upload/cities/');
        $file->move($destinationPath, $filename);
        $this->city->add($request,$filename);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $from = Request()->get('from');
        $governates = $this->governate->getListCountry($id);
        $governates = array_add($governates, '', trans('layout.choose_governate'));
        $governates = array_reverse($governates, TRUE);
        return view('admin.cities.show', ['governates' => $governates, 'from' => $from]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $city = $this->city->getById($id);

        $countries = $this->country->getList();
        $countries = array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);

        $governates = $this->governate->getListCountry($city['Governate']['country_id']);
        $governates = array_add($governates, '', trans('layout.choose_governate'));
        $governates = array_reverse($governates, TRUE);

        return view('admin.cities.edit', ['city' => $city, 'countries' => $countries, 'governates' => $governates]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCity $request, $id) {
        //
        $this->city->edit($id, $request);
        $message = trans('layout.edit_suc');
        return redirect('admin/cities')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $this->city->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

    public function getCities(Request $request, $id) {
        $branch = \App\Branch::find($request['branch_id']);

        $cities = $this->city->getListGovernate($id);
        $cities = array_add($cities, '', trans('layout.choose_city'));
        $cities = array_reverse($cities, TRUE);

        return View('admin.cities.ajax', ['cities' => $cities, 'branch' => $branch]);
    }

    public function getCitiesAgent(Request $request, $id) {
        $agent = \App\Agent::find($request['agent_id']);

        $cities = $this->city->getListGovernate($id);
        $cities = array_add($cities, '', trans('layout.choose_city'));
        $cities = array_reverse($cities, TRUE);

        return View('admin.cities.ajax_agent', ['cities' => $cities, 'agent' => $agent]);
    }


}
