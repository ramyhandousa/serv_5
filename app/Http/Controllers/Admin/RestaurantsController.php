<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreRestaurant;
use App\Http\Requests\UpdateRestaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Restaurant;
use App\RestaurantCategory;
use App\Country;
use App\City;

class RestaurantsController extends Controller {

    //
    function __construct(Restaurant $Restaurant,RestaurantCategory $RestaurantCategory,Country $country, City $City) {
        $this->Restaurant = $Restaurant;
        $this->RestaurantCategory = $RestaurantCategory;
        $this->city = $City;
        $this->country = $country;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $Restaurants = $this->Restaurant->getAll();
        return view('admin.restaurants.index', ['Restaurants' => $Restaurants]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //

        $countries = $this->country->getList();
        $countries= array_add($countries, '', trans('layout.choose_country'));
        $countries = array_reverse($countries, TRUE);
        $RestaurantCategory = $this->RestaurantCategory->getList();
        $RestaurantCategory= array_add($RestaurantCategory, '', trans('layout.choose_restaurant_category'));
        $RestaurantCategory = array_reverse($RestaurantCategory, TRUE);
        return view('admin.restaurants.create', ['countries' => $countries,'RestaurantCategory' => $RestaurantCategory ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request) {
        $file = $request['image'];
        $filename = str_random(5) . '.' . $file->getClientOriginalName();
        $destinationPath = public_path('upload/restaurants/');
        $file->move($destinationPath, $filename);
        $this->Restaurant->add($request, $filename);
        $message = trans('layout.add_suc');
        return back()->with(['message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
        $from = Request()->get('from');
        $cities = $this->city->getListCity($id);
        $cities = array_add($cities, '', trans('layout.choose_city'));
        $cities = array_reverse($cities, TRUE);
        return view('admin.restaurants.show', ['cities' => $cities,'from' => $from]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
        $Restaurants = $this->Restaurant->getById($id);

        $countries = $this->country->getList();
        $countries= array_add($countries, '', trans('layout.choose_city'));
        $countries = array_reverse($countries, TRUE);

        $cities = $this->city->getList();
        $cities= array_add($cities, '', trans('layout.choose_city'));
        $cities = array_reverse($cities, TRUE);

        $RestaurantCategory = $this->RestaurantCategory->getList();
        $RestaurantCategory= array_add($RestaurantCategory, '', trans('layout.choose_restaurant_category'));
        $RestaurantCategory = array_reverse($RestaurantCategory, TRUE);

        return view('admin.restaurants.edit', ['Restaurants' => $Restaurants,'countries' => $countries,'cities' => $cities,'RestaurantCategory' => $RestaurantCategory ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRestaurant $request, $id) {
        //
        $filename = FALSE;
        if ($request->hasFile('image')) {
            $file = $request['image'];
            $filename = str_random(5) . '.' . $file->getClientOriginalName();
            $destinationPath = public_path('upload/restaurants/');
            $file->move($destinationPath, $filename);
        }
        $this->Restaurant->edit($id, $request, $filename);
        $message = trans('layout.edit_suc');
        return redirect('admin/restaurants')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
        $Restaurant = $this->Restaurant->getById($id);

        $Restaurant->remove($id);
        $message = trans('layout.delete_suc');
        return back()->with(['message' => $message]);
    }

}
