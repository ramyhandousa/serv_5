<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\CarAddon;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCarAddonsTable;
use App\Http\Requests\UpdateCarAddonsTable;

class CarAddonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    function __construct(CarAddon $addon) {
        $this->Addon = $addon;       
    }


    public function index()
    {
        $addon = $this->Addon->getAll();
        return View('admin.car-addons.index',['addon' => $addon]);
 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.car-addons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCarAddonsTable $request)
    {
            $file = $request['image'];
            $filename = time() . '.' . $file->getClientOriginalName();
            $destinationPath = public_path('upload/addon/');
            // Upload Original version
            $file->move($destinationPath, $filename);
            $this->Addon->add($request, $filename);
            $message = trans('layout.add_suc');
            return back()->with(['message' => $message]); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarAddon  $carAddon
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {  
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarAddon  $carAddon
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $addon = $this->Addon->getById($id);

        return View('admin.car-addons.edit',['addon' => $addon]);    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarAddon  $carAddon
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCarAddonsTable $request,  $id)
    {
        $filename = False;
        
        if ($request->hasFile('image')) {
            $file = $request['image'];
            $filename = time() . '.' . $file->getClientOriginalName();
            $destinationPath = public_path('upload/addon/');
            // Upload Original version
            $file->move($destinationPath, $filename);
        }

        $this->Addon->edit($id, $request, $filename);
        $message = trans('layout.edit_suc');
        return redirect('admin/addons')->with(['message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarAddon  $carAddon
     * @return \Illuminate\Http\Response
     */
    public function destroy(CarAddon $carAddon)
    {
        
    }
}
