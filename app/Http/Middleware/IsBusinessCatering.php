<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsBusinessCatering {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (!Auth::Check() || (Auth::User()->active != "yes" || Auth::User()->type != "supplier")) {
            if (Auth::Check()) {
                Auth::logout();
            }
            return redirect('business/login');
        } elseif (Auth::User()->type == "supplier" && \App\Supplier::where('user_id', Auth::User()->id)->where('catering', 'yes')->count() > 0) {
            return redirect('business/not_allow');
        }
        return $next($request);
    }

}
