<?php

namespace App\Http\Library;

use URL;

class Armada {

    public static function create($data) {
        $key = "265a9deb-96fd-42d1-86eb-b48b9791e63f";
        //$key = "1000d38a-23d6-46c0-8128-39371a18447a";
        $url = "https://api.armadadelivery.com/v0/deliveries";
        //$url = "https://api-simulation-env.herokuapp.com/v0/deliveries";

        $fields = $data;

        $headers = ["Authorization: Key " . $key, 'Content-Type: application/json'];

        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
        // Close connection
        curl_close($ch);

        $result = html_entity_decode($result);
        $result = json_decode($result, true);
        return $result;
    }

}

?>