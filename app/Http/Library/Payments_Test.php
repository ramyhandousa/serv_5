<?php

namespace App\Http\Library;

use URL;

class Payments_Test {

    public static function get_url($data) {
        $code = "999999";
        $user = "testapi@myfatoorah.com";
        $password = "E55D0";
        $post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <PaymentRequest xmlns="http://tempuri.org/">
      <req>
        <CustomerDC>
          <Name>' . $data['name'] . '</Name>
          <Email>' . $data['email'] . '</Email>
          <Mobile>' . $data['phone'] . '</Mobile>
          <Gender></Gender>
          <DOB></DOB>
          <civil_id></civil_id>
          <Area></Area>
          <Block></Block>
          <Street></Street>
          <Avenue></Avenue>
          <Building></Building>
          <Floor></Floor>
          <Apartment></Apartment>
        </CustomerDC>
        <MerchantDC>
          <merchant_code>' . $code . '</merchant_code>
          <merchant_username>' . $user . '</merchant_username>
          <merchant_password>' . $password . '</merchant_password>
          <merchant_ReferenceID>' . $data['unique'] . '</merchant_ReferenceID>
          <ReturnURL>' . URL::to('payment_success') . '</ReturnURL>
          <merchant_error_url>' . URL::to('payment_fail') . '</merchant_error_url>
          <udf1>' . $data['ids'] . '</udf1>
          <udf2>' . $data['operation'] . '</udf2>
          <udf3>' . $data['platform'] . '</udf3>
          <udf4>' . $data['lang'] . '</udf4>
          <udf5></udf5>
        </MerchantDC>
        <lstProductDC>' . $data['orders'] . '</lstProductDC>
        <totalDC>
          <subtotal>' . $data['total'] . '</subtotal>
        </totalDC>
        <paymentModeDC>
          <paymentMode>' . $data['pay_type'] . '</paymentMode>
        </paymentModeDC>
        <paymentCurrencyDC>
          <paymentCurrrency>KWD</paymentCurrrency>
        </paymentCurrencyDC>
      </req>
    </PaymentRequest>
  </soap:Body>
</soap:Envelope>';


        $url = "https://test.myfatoorah.com/pg/PayGatewayServiceV2.asmx";

        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL, $url);
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array('Content-Type: text/xml; charset=utf-8', 'Content-Length: ' . strlen($post_string)));
        curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array('Content-type: text/xml'));
        $result = curl_exec($soap_do);
        $err = curl_error($soap_do);
        curl_close($soap_do);

        $result = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $result);
        $result = simplexml_load_string($result);
        $json = json_encode($result);
        $responseArray = json_decode($json, true);

        return $responseArray['soapBody']['PaymentRequestResponse']['PaymentRequestResult']['paymentURL'];
    }

    public static function check($data) {
        $url = "https://test.myfatoorah.com/pg/PayGatewayService.asmx";
        $code = "999999";
        $user = "testapi@myfatoorah.com";
        $password = "E55D0";
        $post_string = '<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Body>
<GetOrderStatusRequest xmlns="http://tempuri.org/">
<getOrderStatusRequestDC>
<merchant_code>' . $code . '</merchant_code>
<merchant_username>' . $user . '</merchant_username>
<merchant_password>' . $password . '</merchant_password>
<referenceID>' . $data['referenceID'] . '</referenceID>
</getOrderStatusRequestDC>
</GetOrderStatusRequest>
</soap:Body>
</soap:Envelope>';
        $soap_do = curl_init();
        curl_setopt($soap_do, CURLOPT_URL, $url);
        curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_TIMEOUT, 10);
        curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($soap_do, CURLOPT_POST, true);
        curl_setopt($soap_do, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($soap_do, CURLOPT_HTTPHEADER, array('Content-Type: text/xml; charset=utf-8', 'Content-Length: ' . strlen($post_string)));
        curl_setopt($soap_do, CURLOPT_USERPWD, $user . ":" . $password);
        $result = curl_exec($soap_do);
        $err = curl_error($soap_do);

        $file_contents = htmlspecialchars(curl_exec($soap_do));
        curl_close($soap_do);
        $doc = new \DOMDocument();
        $doc->loadXML(html_entity_decode($file_contents));
        
        $ResponseCode = $doc->getElementsByTagName("ResponseCode");
        $final['ResponseCode'] = $ResponseCode->item(0)->nodeValue;
        
        $ResponseMessage = $doc->getElementsByTagName("ResponseMessage");
        $final['ResponseMessage'] = $ResponseMessage->item(0)->nodeValue;
        
        $Paymode = $doc->getElementsByTagName("Paymode");
        $final['Paymode'] = $Paymode->item(0)->nodeValue;

        $PayTxnID = $doc->getElementsByTagName("PayTxnID");
        $final['PayTxnID'] = $PayTxnID->item(0)->nodeValue;

        $udf1 = $doc->getElementsByTagName("udf1");
        $final['udf1'] = $udf1->item(0)->nodeValue;

        $udf2 = $doc->getElementsByTagName("udf2");
        $final['udf2'] = $udf2->item(0)->nodeValue;

        $udf3 = $doc->getElementsByTagName("udf3");
        $final['udf3'] = $udf3->item(0)->nodeValue;

        $udf4 = $doc->getElementsByTagName("udf4");
        $final['udf4'] = $udf4->item(0)->nodeValue;

        return $final;
    }

}

?>