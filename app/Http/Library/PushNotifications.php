<?php

namespace App\Http\Library;

class PushNotifications {

    public static function android($topic, $message_data) {
        $SERVER_KEY = "AAAAAtGUi8U:APA91bF_GtwzQ83r9nOZmO3pMT2d-buulsC6nHW1fypmc_Avk2YEsYSWsp0IRkojEgS5pSRVOl6XWCMrUrqFrEtXcPKhKRqwGR6tk9Mde5cju4Tc4Lu4l6PtFvKENMPLEHHcnwCBaPuZ";

// Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'data' => $message_data,
            'to' => $topic
        );

        $headers = array(
            'Authorization: key=' . $SERVER_KEY,
            'Content-Type: application/json'
        );
// Open connection
        $ch = curl_init();
// Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
// Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
// Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            $res = 0;
        } else {
            $res = 1;
        }
// Close connection
        curl_close($ch);
        return $res;
    }

    public static function ios($topic, $message_data) {
        $SERVER_KEY = "AAAAAtGUi8U:APA91bF_GtwzQ83r9nOZmO3pMT2d-buulsC6nHW1fypmc_Avk2YEsYSWsp0IRkojEgS5pSRVOl6XWCMrUrqFrEtXcPKhKRqwGR6tk9Mde5cju4Tc4Lu4l6PtFvKENMPLEHHcnwCBaPuZ";

// Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';
        $title = $message_data['alert'];
        $fields = array(
            'notification' => ["body" => $title, "sound" => "default"],
            'data' => $message_data,
            'to' => $topic
        );

        $headers = array(
            'Authorization: key=' . $SERVER_KEY,
            'Content-Type: application/json'
        );
// Open connection
        $ch = curl_init();
// Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
// Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
// Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            $res = 0;
        } else {
            $res = 1;
        }
// Close connection
        curl_close($ch);
        return $res;
    }

    public static function android_manager($topic, $message_data) {
        $SERVER_KEY = "AAAA2ILjW74:APA91bF-PyWl16ugX-m_xMtkS5Wo2VW38ZnaLZidFA1_4kATsmo5jwLuKgR3-sxTCzPnlSowc8b4SaW8aQPomAVpcKwky7RQmAwzw7AMy8Oc49cUtTlT1TpesYn8gphY08tjptxzWULr";

// Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'data' => $message_data,
            'to' => $topic
        );

        $headers = array(
            'Authorization: key=' . $SERVER_KEY,
            'Content-Type: application/json'
        );
// Open connection
        $ch = curl_init();
// Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
// Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
// Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            $res = 0;
        } else {
            $res = 1;
        }
// Close connection
        curl_close($ch);
        return $res;
    }

    public static function ios_manager($topic, $message_data) {
        $SERVER_KEY = "AAAA2ILjW74:APA91bF-PyWl16ugX-m_xMtkS5Wo2VW38ZnaLZidFA1_4kATsmo5jwLuKgR3-sxTCzPnlSowc8b4SaW8aQPomAVpcKwky7RQmAwzw7AMy8Oc49cUtTlT1TpesYn8gphY08tjptxzWULr";

// Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';
        $title = $message_data['alert'];
        $fields = array(
            'notification' => ["body" => $title, "sound" => "default"],
            'data' => $message_data,
            'to' => $topic
        );

        $headers = array(
            'Authorization: key=' . $SERVER_KEY,
            'Content-Type: application/json'
        );
// Open connection
        $ch = curl_init();
// Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
// Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
// Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            $res = 0;
        } else {
            $res = 1;
        }
// Close connection
        curl_close($ch);
        return $res;
    }

}

?>