<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    //

    public function getAll() {
        return $this->all();
    }

    public function getAllSpecial() {
        return $this->where('special', 'yes')->get();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->nickname = $data['nickname'];
        $this->name = $data['name'];
        $this->city_id = $data['city_id'];
        $this->email = $data['email'];
        $this->phone = $data['phone'];
        $this->passport = $data['passport'];
        $this->licence = $data['licence'];
       

        return $this->save();
    }

   
    public function edit($id, $data) {
        $driver = $this->find($id);
        $driver->nickname = $data['nickname'];
        $driver->name = $data['name'];
        $driver->city_id = $data['city_id'];
        $driver->email = $data['email'];
        $driver->phone = $data['phone'];
        $driver->passport = $data['passport'];
        $driver->licence = $data['licence'];
       
        return $driver->save();
    }

    public function remove($id) {
        $driver = $this->find($id);
        return $driver->delete();
    }
}
