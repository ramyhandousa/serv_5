<?php

namespace App;

use File;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    //

    public function tags()
    {
        return $this->hasMany('App\ActivityTags','activity_id','id');
    }

    public function rates()
    {
        return $this->hasMany('App\ActivityRate','activity_id','id');
    }

    public function images()
    {
        return $this->hasMany('App\ActivityGallery','activity_id','id')->where('type', '0');
    }

    public function videos()
    {
        return $this->hasMany('App\ActivityGallery','activity_id','id')->where('type', '1');
    }

    public function type()
    {
        return $this->hasOne('App\ActivityType','id','type_id');
    }


    public function city()
    {
        return $this->hasOne('App\ContinentCountryCity','id','city_id');
    }

    public function getAll() {
        return $this->all();
    }
    public function getListCities($id) {
        $lang = Config::get('app.locale');
        return $this->where('city_id_id', $id)->pluck($lang . '_name', 'id')->toArray();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data, $file) {
        $this->type_id = $data['type_id'];
        $this->city_id = $data['city_id'];
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->ar_description = $data['ar_description'];
        $this->en_description = $data['en_description'];
        $this->image = $file;
        $this->latitude= $data['lat'];
        $this->longitude= $data['lng'];
        $this->phone= $data['phone'];
        $this->ar_address= $data['ar_address'];
        $this->en_address= $data['en_address'];
        $this->email= $data['email'];
        $this->open_from= $data['open_from'];
        $this->open_to= $data['open_to'];
        return $this->save();
    }

    public function edit($id, $data,$file = false) {
        $activity = $this->find($id);
        $activity->type_id = $data['type_id'];
        $activity->city_id = $data['city_id'];
        $activity->ar_name = $data['ar_name'];
        $activity->en_name = $data['en_name'];
        $activity->ar_description = $data['ar_description'];
        $activity->en_description = $data['en_description'];
        if ($file) {
            $path = public_path('upload/activity/');
            $filename = $activity->image;
            File::Delete($path . $filename);
            $activity->image = $file;
        }
        $activity->latitude= $data['lat'];
        $activity->longitude= $data['lng'];
        $activity->active= $data['active'];
        $activity->phone= $data['phone'];
        $activity->ar_address= $data['ar_address'];
        $activity->en_address= $data['en_address'];
        $activity->email= $data['email'];
        $activity->open_from= $data['open_from'];
        $activity->open_to= $data['open_to'];
        return $activity->save();
    }

    public function remove($id) {
        $activity = $this->find($id);
        $path = public_path('upload/activities/');
        $filename = $activity['image'];
        File::Delete($path . $filename);
        return $activity->delete();
    }



}
