<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarSecurity extends Model
{
  

    public function Car() {
        return $this->hasOne('Car', 'id', 'car_id');
    }

    public function Security() {
        return $this->hasOne('Security', 'id', 'security_id');
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $new = New CarSecurity;
        $new->car_id = strip_tags($data['car_id']);
        $new->security_id = strip_tags($data['security_id']);
        $new->price = strip_tags($data['price']);
        return $new->save();
    }

    public function removeByCar($id) {
        return $this->where('car_id', $id)->delete();
    }

    public function removeBySecurity($id) {
        return $this->where('security_id', $id)->delete();
    }

    public function getNum() {
        return $this->count();
    } 
}
