<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityDifferent extends Model
{
    public function FromCity() {
        return $this->hasOne('App\City', 'id', 'from');
    }

    public function ToCity() {
        return $this->hasOne('App\City', 'id', 'to');
    }

    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->from = strip_tags($data['from']);
        $this->to = strip_tags($data['to']);
        $this->price = strip_tags($data['price']);
        return $this->save();
    }

    public function edit($id, $data) {
        $city = $this->find($id);
        $city->from = strip_tags($data['from']);
        $city->to = strip_tags($data['to']);
        $city->price = strip_tags($data['price']);
        return $city->save();
    }

    public function remove($id) {
        $city = $this->find($id);
        return $city->delete();
    }

    public function removeByCity($id) {
        return $this->where('from', $id)->orWhere('to', $id)->delete();
    }

    public function getNum() {
        return $this->count();
    }

}
