<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use App\Governate;
use File;

class City extends Model {

    protected $table = 'cities';

    //
    public function Governate() {
        return $this->hasOne(Governate::class, 'id', 'governate_id');
    }

    public function getListGovernate($id) {
        $lang = Config::get('app.locale');
        return $this->where('governate_id', $id)->pluck($lang . '_name', 'id')->toArray();
    }

    public function getList() {
        $lang = Config::get('app.locale');
        return $this->pluck($lang . '_name', 'id')->toArray();
    }

    public function getAll() {
        return $this->all();
    }

    public function getAllSpecial() {
        return $this->where('special', 'yes')->get();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data, $file) {
        $this->governate_id = $data['governate_id'];
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->ar_description = $data['ar_description'];
        $this->en_description = $data['en_description'];
        $this->ar_overview = $data['ar_overview'];
        $this->en_overview = $data['en_overview'];
        $this->code = $data['code'];
        $this->latitude = $data['lat'];
        $this->longitude = $data['lng'];
        $this->image = $file;

        return $this->save();
    }

    public function add_special($data, $file) {
        $one = $this->find($data['city_id']);
        $one->special = 'yes';
        $one->image = $file;
        return $one->save();
    }

    public function getListCity($id) {
        $lang = Config::get('app.locale');
        
        return $this->where('governate_id', $id)->pluck($lang . '_name', 'id')->toArray();
    }

    public function marketplaces()
    {
        return $this->hasMany('App\MarketPlace','city_id','id');
    }

    public function edit($id, $data,$file = false) {
        $country = $this->find($id);
        $country->active = $data['active'];
        $country->governate_id = $data['governate_id'];
        $country->ar_name = $data['ar_name'];
        $country->en_name = $data['en_name'];
        $country->latitude = $data['lat'];
        $country->longitude = $data['lng'];
        $country->ar_description = $data['ar_description'];
        $country->en_description = $data['en_description'];
        $country->ar_overview = $data['ar_overview'];
        $country->en_overview = $data['en_overview'];
        $country->code = $data['code'];
        
        if ($file) {
            $path = public_path('upload/cities/');
            $filename = $country->image;
            File::Delete($path . $filename);
            $country->image = $file;
        }
        return $country->save();
    }

    public function edit_special($id, $data, $file = false) {
        if ($id == $data['city_id']) {
            $onee = $this->find($id);
            if ($file) {
                $path = public_path('upload/special_cities/');
                $filename = $onee->image;
                File::Delete($path . $filename);
                $onee->image = $file;
            }
            return $onee->save();
        }
        else {
            $onee = $this->find($id);
            $image = $onee->image;
            $onee->special = 'no';
            $onee->image = '';
            $onee->save();

            $one = $this->find($data['city_id']);
            $one->special = 'yes';
            $one->image = $image;
            return $one->save();
        }
    }

    public function remove($id) {
        $country = $this->find($id);
        return $country->delete();
    }

    public function remove_special($id) {
        $one = $this->find($id);
        $path = public_path('upload/special_cities/');
        $filename = $one->image;
        File::Delete($path . $filename);

        $one->special = 'no';
        $one->image = '';
        return $one->save();
    }

}
