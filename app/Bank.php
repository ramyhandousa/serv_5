<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
class Bank extends Model
{
    public function getList($lang) {
        return $this->get()->lists($lang . '_title', 'id');
    }

    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data, $file) {
        $this->ar_title = strip_tags($data['ar_title']);
        $this->en_title = strip_tags($data['en_title']);
        $this->owner = strip_tags($data['owner']);
        $this->number = strip_tags($data['number']);
        $this->country_id = strip_tags($data['country_id']);
        $this->image = $file;
        return $this->save();
    }

    public function edit($id, $data, $file = FALSE) {
        $photo = $this->find($id);
        $photo->ar_title = strip_tags($data['ar_title']);
        $photo->en_title = strip_tags($data['en_title']);
        $photo->owner = strip_tags($data['owner']);
        $photo->number = strip_tags($data['number']);
        $photo->country_id = strip_tags($data['country_id']);
        if ($file) {
            $path = public_path('upload/bank/');
            $filename = $photo->image;
            File::delete($path . $filename);
            $photo->image = $file;
        }
        return $photo->save();
    }

    public function remove($id) {
        $photo = $this->find($id);
        $filename = $photo->image;
        $path = public_path() . '/upload/bank/';
        File::delete($path . $filename);
        return $photo->delete();
    }

    public function getNum() {
        return $this->count();
    }
}
