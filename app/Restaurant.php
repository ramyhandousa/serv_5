<?php

namespace App;
use File;
use Config;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    public function tags()
    {
        return $this->hasMany('App\RestaurantTag','restaurant_id','id');
    }

    public function rates()
    {
        return $this->hasMany('App\RestaurantRate','restaurant_id','id');
    }


    public function city()
    {
        return $this->hasOne('App\ContinentCountryCity','id','city_id');
    }
    public function images()
    {
        return $this->hasMany('App\RestaurantGallery','restaurant_id','id')->where('type', '0');
    }

    public function videos()
    {
        return $this->hasMany('App\RestaurantGallery','restaurant_id','id')->where('type', '1');
    }

    public function category()
    {
        return $this->hasOne('App\RestaurantCategory', 'id','category_id');
    }

    public function getAll() {
        return $this->all();
    }

    public function getList() {
        $lang = Config::get('app.locale');
        return $this->pluck($lang . '_name', 'id')->toArray();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data,$file) {
        $this->category_id = $data['category_id'];
        $this->city_id = $data['city_id'];
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->ar_overview = $data['ar_overview'];
        $this->en_overview = $data['en_overview'];
        $this->image = $file;
        $this->latitude = $data['latitude'];
        $this->longitude = $data['longitude'];
        $this->phone = $data['phone'];
        $this->ar_address = $data['ar_address'];
        $this->en_address = $data['en_address'];
        $this->email= $data['email'];
        $this->facebook= $data['facebook'];
        $this->website= $data['website'];
        $this->delivery= $data['delivery'];
        $this->work_from= $data['work_from'];
        $this->work_to= $data['work_to'];
        return $this->save();
    }

    public function edit($id, $data,$file=false) {
        $Restaurant = $this->find($id);
        $Restaurant->category_id = $data['category_id'];
        $Restaurant->city_id = $data['city_id'];
        $Restaurant->active = $data['active'];
        $Restaurant->ar_name = $data['ar_name'];
        $Restaurant->en_name = $data['en_name'];
        if ($file) {
            $path = public_path('upload/activity/');
            $filename = $Restaurant->image;
            File::Delete($path . $filename);
            $Restaurant->image = $file;
        }

        $Restaurant->latitude = $data['latitude'];
        $Restaurant->longitude = $data['longitude'];
        $Restaurant->phone = $data['phone'];
        $Restaurant->ar_address = $data['ar_address'];
        $Restaurant->en_address = $data['en_address'];
        $Restaurant->email= $data['email'];
        $Restaurant->facebook= $data['facebook'];
        $Restaurant->website= $data['website'];
        $Restaurant->delivery= $data['delivery'];
        $Restaurant->work_from= $data['work_from'];
        $Restaurant->work_to= $data['work_to'];
        return $Restaurant->save();
    }

    public function remove($id) {
        $Restaurant = $this->find($id);
        $path = public_path('upload/restaurants/');
        $filename = $Restaurant['image'];
        File::Delete($path . $filename);
        return $Restaurant->delete();
    }

}
