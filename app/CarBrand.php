<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use Config;

class CarBrand extends Model
{
    public function Models() {
        return $this->hasMany('CarModel', 'brand_id', 'id');
    }
    
    public function Cars() {
        return $this->hasMany('Car', 'brand_id', 'id');
    }

    public function getList() {
        return $this->pluck('ar_title', 'id')->toArray();
    }

    
    public function getListLang() {
        $lang = Config::get('app.locale');

        return $this->pluck($lang . '_title', 'id')->toArray();;
    }

    
    public function getAll() {
        return $this->all();
    }

    
    public function getById($id) {
        return $this->find($id);
    }

    
    public function add($data, $file) {
        $this->ar_title = strip_tags($data['ar_title']);
        $this->en_title = strip_tags($data['en_title']);
        $this->image = $file;
        return $this->save();
    }

    
    public function edit($id, $data, $file = FALSE) {
        $photo = $this->find($id);
        $photo->ar_title = strip_tags($data['ar_title']);
        $photo->en_title = strip_tags($data['en_title']);
        if ($file) {
            $path = public_path('upload/brand/');
            $filename = $photo->image;
            File::delete($path . $filename);
            $photo->image = $file;
        }
        return $photo->save();
    }
    
    public function remove($id) {
        $photo = $this->find($id);
        $filename = $photo->image;
        $path = public_path() . '/upload/brand/';
        File::delete($path . $filename);
        return $photo->delete();
    }

    public function getNum() {
        return $this->count();
    }
}
