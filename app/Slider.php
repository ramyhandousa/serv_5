<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;

class Slider extends Model {

    protected $table = 'slider';

    //

    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data, $file) {
        if ($data['ar_title'] != "") {
            $this->ar_title = $data['ar_title'];
        } else {
            $this->ar_title = "";
        }
        if ($data['en_title'] != "") {
            $this->en_title = $data['en_title'];
        } else {
            $this->en_title = "";
        }
        $this->image = $file;
        return $this->save();
    }

    public function edit($id, $data, $file = false) {
        $dept = $this->find($id);
        if ($data['ar_title'] != "") {
            $dept->ar_title = $data['ar_title'];
        } else {
            $dept->ar_title = "";
        }
        if ($data['en_title'] != "") {
            $dept->en_title = $data['en_title'];
        } else {
            $dept->en_title = "";
        }
        if ($file) {
            $path = public_path('upload/slider/');
            $filename = $dept->image;
            File::Delete($path . $filename);
            $dept->image = $file;
        }
        return $dept->save();
    }

    public function remove($id) {
        $dept = $this->find($id);
        $path = public_path('upload/slider/');
        $filename = $dept['image'];
        File::Delete($path . $filename);
        return $dept->delete();
    }

}
