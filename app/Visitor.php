<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Country;

class Visitor extends Model {

    //

    public function Country() {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function getAll() {
        return $this->get();
    }

}
