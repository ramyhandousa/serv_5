<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityRate extends Model
{
    //
    public function Activity()
    {
        return $this->belongsTo('App\Activity', 'activity_id','id');
    }

    public function add($data) {
        $this->activity_id = $data['activity_id'];
        $this->user_id = $data['user_id'];
        $this->rate_no = $data['rate_no'];
        $this->rate_title = $data['rate_title'];
        $this->rate_description = $data['rate_description'];
        return $this->save();
    }

    public function edit($id,$data) {
        $activityRate = $this->find($id);
        $activityRate->activity_id = $data['activity_id'];
        $activityRate->user_id = $data['user_id'];
        $activityRate->rate_no = $data['rate_no'];
        $activityRate->rate_title = $data['rate_title'];
        $activityRate->rate_description = $data['rate_description'];
        return $this->save();
    }

    public function remove($id) {
        $activityRate = $this->find($id);
        return $activityRate->delete();
    }





}
