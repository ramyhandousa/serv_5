<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantTag extends Model
{
    //
    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant', 'restaurant_id', 'id');
    }
}
