<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;

class Car extends Model
{
    public function City()
    {
        return $this->hasOne('App\City', 'id', 'city_id');
    }

    public function Models()
    {
        return $this->hasOne('App\CarModel', 'id', 'model_id');
    }

    public function Brand()
    {
        return $this->hasOne('App\CarBrand', 'id', 'brand_id');
    }

    public function Security()
    {
        return $this->hasMany('App\CarSecurity', 'car_id', 'id');
    }

    public function Bookings()
    {
        return $this->hasMany('App\CarBooking', 'car_id', 'id');
    }

    public function getAlls()
    {
        return $this->where('active', 'yes')->orderBy('id', 'desc')->paginate(15);
    }

    public function getAllss()
    {
        return $this->where('active', 'yes')->orderBy('id', 'desc')->get();
    }

    public function getAllsss($city)
    {
        return $this->where('active', 'yes')->where('city_id', $city)->orderBy('id', 'desc')->get();
    }

    public function getLast()
    {
        return $this->where('active', 'yes')->orderBy('id', 'desc')->take(8)->get();
    }

    public function getAll()
    {
        return $this->all();
    }

    public function getById($id)
    {
        return $this->find($id);
    }

    public function add($data, $file,$key)
    {

        $this->paypal = $data['paypal'];
        $this->cash = $data['cash'];
        $this->bank = $data['bank'];
        $this->admin_sure = $data['admin_sure'];
        $this->brand_id = $data['brand_id'];
        $this->model_id = $data['model_id'];
        $this->city_id = $key;
        $this->count = $data['count'];
        $this->seats = $data['seats'];
        $this->doors = $data['doors'];
        $this->bags = $data['bags'];
        $this->air = $data['air'];
        $this->vetis =$data['vetis'];
        $this->fuel = $data['fuel'];
        $this->size = $data['size'];
        $this->price = $data['price'];
        $this->comision = $data['comision'];
        $this->ar_details = $data['ar_details'];
        $this->en_details = $data['en_details'];
        $this->ar_conditions = $data['ar_conditions'];
        $this->en_conditions = $data['en_conditions'];
        $this->image = $file;
        $this->save();
        $id=$this->id;
        return $id;
    }

    public function edit($id, $data, $file = FALSE)
    {
        $car = $this->find($id);
        $car->paypal = strip_tags($data['paypal']);
        $car->cash = strip_tags($data['cash']);
        $car->bank = strip_tags($data['bank']);
        $car->admin_sure = strip_tags($data['admin_sure']);
        $car->active = strip_tags($data['active']);
        $car->brand_id = strip_tags($data['brand_id']);
        $car->model_id = strip_tags($data['model_id']);
        $car->city_id = strip_tags($data['city_id']);
        $car->count = strip_tags($data['count']);
        $car->seats = strip_tags($data['seats']);
        $car->doors = strip_tags($data['doors']);
        $car->bags = strip_tags($data['bags']);
        $car->air = strip_tags($data['air']);
        $car->vetis = strip_tags($data['vetis']);
        $car->fuel = strip_tags($data['fuel']);
        $car->size = strip_tags($data['size']);
        $car->price = strip_tags($data['price']);
        $car->comision = strip_tags($data['comision']);
        $car->ar_details = $data['ar_details'];
        $car->en_details = $data['en_details'];
        $car->ar_conditions = $data['ar_conditions'];
        $car->en_conditions = $data['en_conditions'];
        
        if ($file) {
            $path = public_path('upload/cars/');
            $filename = $car->image;
            File::delete($path . $filename);
            $car->image = $file;
        }

        return $car->save();
    }
 
    public function remove($id)
    {
        $car = $this->find($id);
        $filename = $car->image;
        $path = public_path() . '/upload/cars/';
        File::delete($path . $filename);
        return $car->delete();
    }

    public function getNum()
    {
        return $this->count();
    }

    public function search_fast($data)
    {
        if ($data['country'] != "") {
            $v1 = $this->where('city_id', $data['country']);
        } else {
            $v1 = $this;
        }

        if ($data['mark'] != "") {
            $v2 = $v1->where('mark_id', $data['mark']);
        } else {
            $v2 = $v1;
        }

        if ($data['model'] != "") {
            $v3 = $v2->where('mark_id', $data['mark']);
        } else {
            $v3 = $v2;
        }

        return $v3->get();
    }

    public function search_filter($data)
    {
        $v1 = $this->whereBetween('price', [$data['price_from'], $data['price_to']])->where('city_id', $data['city']);

        if (isset($data['size'])) {
            $v2 = $v1->whereIn('size', $data['size']);
        } else {
            $v2 = $v1;
        }

        if (isset($data['air'])) {
            $v3 = $v2->where('air', 'yes');
        } else {
            $v3 = $v2;
        }

        if (isset($data['fuel'])) {
            $v4 = $v3->whereIn('fuel', $data['fuel']);
        } else {
            $v4 = $v3;
        }

        if (isset($data['vetis'])) {
            $v5 = $v4->whereIn('vetis', $data['vetis']);
        } else {
            $v5 = $v4;
        }

        if (isset($data['marks'])) {
            $v6 = $v5->whereIn('mark_id', $data['marks']);
        } else {
            $v6 = $v5;
        }

        return $v6->get();
    }
}
