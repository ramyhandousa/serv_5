<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use File;

class CarModel extends Model
{  

    public function brand() {
        return $this->hasOne('brand', 'id', 'brand_id');
    }

    public function Cars() {
        return $this->hasMany('Car', 'model_id', 'id');
    }

    public function getByBrand($id) {
        return $this->where('brand_id', $id)->get();
    }

    public function getList($id) {
        $lang = Config::get('app.locale');

        return $this->where('brand_id', $id)->pluck($lang . '_title', 'id')->toArray();;
    }

    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->brand_id = strip_tags($data['brand_id']);
        $this->ar_title = strip_tags($data['ar_title']);
        $this->en_title = strip_tags($data['en_title']);
        $this->year = strip_tags($data['year']);
        return $this->save();
    }

    public function edit($id, $data) {
        $photo = $this->find($id);
        $photo->brand_id = strip_tags($data['brand_id']);
        $photo->ar_title = strip_tags($data['ar_title']);
        $photo->en_title = strip_tags($data['en_title']);
        $photo->year = strip_tags($data['year']);
        return $photo->save();
    }

    public function remove($id) {
        $photo = $this->find($id);
        return $photo->delete();
    }

    public function getNum() {
        return $this->count();
    }

    public function search($data) {
        $lang = App::getLocale();
        return $this->where($lang . '_title', 'like', '%' . $data['word'] . '%')->get();
    }


}
