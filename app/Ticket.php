<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Ticket extends Model {

    //
    public function Admin() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function User() {
        return $this->hasOne(User::class, 'phone', 'phone');
    }

    public function Visitor() {
        return $this->hasOne(Visitor::class, 'phone', 'phone');
    }

    public function getAll() {
        return $this->all();
    }

    public function getByPhone($phone, $status) {
        if ($phone != "") {
            $s1 = $this->where('phone', $phone);
        } else {
            $s1 = $this;
        }
        if ($status != "") {
            $s2 = $s1->where('status', $status);
        } else {
            $s2 = $s1;
        }

        return $s2->get();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->user_id = Auth::user()->id;
        $this->phone = $data['phone'];
        $this->title = $data['title'];
        $this->note = $data['note'];
        $this->status = $data['status'];
        return $this->save();
    }

    public function edit($id, $data) {
        $ticket = $this->find($id);
        $ticket->title = $data['title'];
        $ticket->note = $data['note'];
        $ticket->status = $data['status'];
        return $ticket->save();
    }

}
