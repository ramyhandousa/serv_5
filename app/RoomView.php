<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomView extends Model {

    //

    protected $table = 'room_views';
    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->ar_notes = $data['ar_notes'];
        $this->en_notes = $data['en_notes'];
        return $this->save();
    }

    public function edit($id, $data) {
        $RoomView = $this->find($id);
        $RoomView->ar_name = $data['ar_name'];
        $RoomView->en_name = $data['en_name'];
        $RoomView->ar_notes = $data['ar_notes'];
        $RoomView->en_notes = $data['en_notes'];
        return $RoomView->save();
    }

    public function remove($id) {
        $RoomView = $this->find($id);
        return $RoomView->delete();
    }

}
