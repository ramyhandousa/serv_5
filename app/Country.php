<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use Config;
use App\Governate;

class Country extends Model {

    protected $table = 'countries';

    //
    public function Governates() {
        return $this->hasMany(Governate::class, 'country_id', 'id');
    }

    public function GoverantesActive() {
        return $this->hasMany(Governate::class, 'country_id', 'id')->where('active', 'yes');
    }

    public function getListOne($id) {
        $lang = Config::get('app.locale');
        return $this->where('id', $id)->pluck($lang . '_name', 'id')->toArray();
    }

    public function getList() {
        $lang = Config::get('app.locale');
        return $this->pluck($lang . '_name', 'id')->toArray();
    }

    public function getAll() {
        return $this->all();
    }

    public function getAlls() {
        return $this->where('active', 'yes')->get();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data, $file) {
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->ar_currency = $data['ar_currency'];
        $this->en_currency = $data['en_currency'];
        $this->ar_description = $data['ar_description'];
        $this->en_description = $data['en_description'];
        $this->code = $data['code'];
        $this->iso_code = $data['iso_code'];
    
        $this->image = $file;
        return $this->save();
    }

    public function edit($id, $data, $file = false) {
        $country = $this->find($id);
        $country->active = $data['active'];
        $country->ar_name = $data['ar_name'];
        $country->en_name = $data['en_name'];
        $country->ar_currency = $data['ar_currency'];
        $country->en_currency = $data['en_currency'];
        $country->ar_description = $data['ar_description'];
        $country->en_description = $data['en_description'];
        $country->code = $data['code'];
        $country->iso_code = $data['iso_code'];
    
        if ($file) {
            $path = public_path('upload/countries/');
            $filename = $country->image;
            File::Delete($path . $filename);
            $country->image = $file;
        }
        return $country->save();
    }

    public function remove($id) {
        $country = $this->find($id);
        $path = public_path('upload/countries/');
        $filename = $country['image'];
        File::Delete($path . $filename);
        return $country->delete();
    }

}
