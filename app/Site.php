<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model {

    protected $table = 'site';

    //
    public function getData() {
        return $this->first();
    }

    public function edit($data) {
        $site = $this->first();
        $site->ar_title = $data['ar_title'];
        $site->en_title = $data['en_title'];
        $site->ar_desc = $data['ar_desc'];
        $site->en_desc = $data['en_desc'];
        $site->tags = $data['tags'];
        $site->email = $data['email'];
        $site->phone = $data['phone'];
        $site->mobile = $data['mobile'];
        $site->fax = $data['fax'];
        $site->facebook = $data['facebook'];
        $site->twitter = $data['twitter'];
        $site->google = $data['google'];
        $site->youtube = $data['youtube'];
        $site->instagram = $data['instagram'];
        $site->snapchat = $data['snapchat'];
        $site->android = $data['android'];
        $site->ios = $data['ios'];
        return $site->save();
    }

}
