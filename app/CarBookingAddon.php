<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarBookingAddon extends Model
{
    protected $fillable = ['booking_id', 'addon_id', 'price'];
    protected $guarded = ['id'];

    public function Booking() {
        return $this->hasOne('Booking', 'id', 'booking_id');
    }

    public function Addon() {
        return $this->hasOne('Addon', 'id', 'addon_id');
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $new = New BookingAddon;
        $new->booking_id = strip_tags($data['booking_id']);
        $new->addon_id = strip_tags($data['addon_id']);
        $new->price = strip_tags($data['price']);
        return $new->save();
    }

    public function removeByBooking($id) {
        return $this->where('booking_id', $id)->delete();
    }

    public function removeByAddon($id) {
        return $this->where('addon_id', $id)->delete();
    }

    public function getNum() {
        return $this->count();
    }

}
