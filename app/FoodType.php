<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodType extends Model {

    //

    protected $table = 'food_types';
    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->ar_notes = $data['ar_notes'];
        $this->en_notes = $data['en_notes'];
        return $this->save();
    }

    public function edit($id, $data) {
        $FoodType = $this->find($id);
        $FoodType->ar_name = $data['ar_name'];
        $FoodType->en_name = $data['en_name'];
        $FoodType->ar_notes = $data['ar_notes'];
        $FoodType->en_notes = $data['en_notes'];
        return $FoodType->save();
    }

    public function remove($id) {
        $FoodType = $this->find($id);
        return $FoodType->delete();
    }

}
