<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Group;
use App\Country;
use App\Order;
use App\SubscribeOrder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Group() {
        return $this->hasOne(Group::class, 'id', 'group_id');
    }

    public function Branch() {
        return $this->hasOne(Branch::class, 'id', 'branch_id');
    }

    public function Country() {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function Supplier() {
        return $this->hasOne(Supplier::class, 'user_id', 'id');
    }

    public function Orders() {
        return $this->hasMany(Order::class, 'user_id', 'id')->where('user_type', 'user');
    }

    public function Addresses() {
        return $this->hasMany(Address::class, 'user_id', 'id')->where('user_type', 'user');
    }

    public function getType($type) {
        return $this->where('type', $type)->get();
    }

    public function getByBranch($id) {
        return $this->where('branch_id', $id)->get();
    }

    public function getByBranches($ids) {
        return $this->whereIn('branch_id', $ids)->get();
    }

    public function getList($type) {
        if (Auth::User()->Group['country'] == "all") {
            return $this->where('type', $type)->pluck('name', 'id')->toArray();
        } else {
            return $this->where('type', $type)->where('country_id', Auth::User()->country_id)->pluck('name', 'id')->toArray();
        }
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function edit($id, $data) {
        $user = $this->find($id);
        $user->active = $data['active'];
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        $user->country_id = $data['country_id'];
        if ($data['password'] != "") {
            $user->password = Hash::make($data['password']);
        }
        return $user->save();
    }

    public function edit_seen($id) {
        $user = $this->find($id);
        $user->seen = 'yes';
        return $user->save();
    }

    public function edit_active($id, $active) {
        $user = $this->find($id);
        $user->active = $active;
        return $user->save();
    }

    public function add_admin($data) {
        $this->group_id = $data['group_id'];
        $this->country_id = $data['country_id'];
        $this->name = $data['name'];
        $this->username = $data['username'];
        $this->email = $data['email'];
        $this->phone = $data['phone'];
        $this->password = Hash::make($data['password']);
        $this->type = 'admin';
        return $this->save();
    }

    public function add_supplier($data) {
        $this->country_id = $data['country_id'];
        $this->name = $data['name'];
        $this->username = $data['username'];
        $this->email = $data['email'];
        $this->phone = $data['phone'];
        $this->password = Hash::make($data['password']);
        $this->type = 'supplier';
        $this->save();
        return $this->id;
    }

    public function add_employee($data) {
        $this->country_id = $data['country_id'];
        $this->branch_id = $data['branch_id'];
        $this->name = $data['name'];
        $this->username = $data['username'];
        $this->email = $data['email'];
        $this->phone = $data['phone'];
        $this->password = Hash::make($data['password']);
        $this->type = 'employee';
        return $this->save();
    }

    public function edit_admin($id, $data) {
        $user = $this->find($id);
        $user->active = $data['active'];
        $user->group_id = $data['group_id'];
        $user->country_id = $data['country_id'];
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        if ($data['password'] != "") {
            $user->password = Hash::make($data['password']);
        }
        return $user->save();
    }

    public function edit_supplier($id, $data) {
        $user = $this->find($id);
        $user->active = $data['active'];
        $user->country_id = $data['country_id'];
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        if ($data['password'] != "") {
            $user->password = Hash::make($data['password']);
        }
        return $user->save();
    }

    public function edit_employee($id, $data) {
        $user = $this->find($id);
        $user->active = $data['active'];
        $user->branch_id = $data['branch_id'];
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        if ($data['password'] != "") {
            $user->password = Hash::make($data['password']);
        }
        return $user->save();
    }

    public function edit_profile($id, $data) {
        $user = $this->find($id);
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        if ($data['password'] != "") {
            $user->password = Hash::make($data['password']);
        }
        return $user->save();
    }

}
