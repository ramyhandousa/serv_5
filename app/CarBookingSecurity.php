<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarBookingSecurity extends Model
{
    
    protected $fillable = ['booking_id', 'car_security_id', 'price'];
    protected $guarded = ['id'];

    public function Booking() {
        return $this->hasOne('CarBooking', 'id', 'booking_id');
    }

    public function CarSecurity() {
        return $this->hasOne('CarSecurity', 'id', 'car_security_id');
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $new = New BookingSecurity;
        $new->booking_id = strip_tags($data['booking_id']);
        $new->car_security_id = strip_tags($data['car_security_id']);
        $new->price = strip_tags($data['price']);
        return $new->save();
    }

    public function removeByBooking($id) {
        return $this->where('booking_id', $id)->delete();
    }

    public function removeByCarSecurity($id) {
        return $this->where('car_security_id', $id)->delete();
    }

    public function getNum() {
        return $this->count();
    }

}
