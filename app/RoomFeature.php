<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomFeature extends Model {

    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->icon = $data['icon'];
        return $this->save();
    }

    public function edit($id, $data) {
        $RoomFeatures = $this->find($id);
        $RoomFeatures->ar_name = $data['ar_name'];
        $RoomFeatures->en_name = $data['en_name'];
        $RoomFeatures->icon = $data['icon'];
        return $RoomFeatures->save();
    }

    public function remove($id) {
        $RoomFeatures = $this->find($id);
        return $RoomFeatures->delete();
    }

}
