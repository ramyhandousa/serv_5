<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantRate extends Model
{
    //
    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant', 'restaurant_id', 'id');
    }

    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->restaurant_id = $data['restaurant_id'];
        $this->user_id = $data['user_id'];
        $this->ar_title = $data['ar_title'];
        $this->en_title = $data['en_title'];
        $this->ar_description = $data['ar_description'];
        $this->en_description = $data['en_description'];
        return $this->save();
    }

    public function edit($id, $data) {
        $HotelRate = $this->find($id);
        $HotelRate->restaurant_id = $data['restaurant_id'];
        $HotelRate->user_id = $data['user_id'];
        $HotelRate->ar_title = $data['ar_title'];
        $HotelRate->en_title = $data['en_title'];
        $HotelRate->ar_description = $data['ar_description'];
        $HotelRate->en_description = $data['en_description'];
        return $HotelRate->save();
    }

    public function remove($id) {
        $HotelRate = $this->find($id);
        return $HotelRate->delete();
    }
}
