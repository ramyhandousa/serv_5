<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;

class ActivityType extends Model
{
    //

    public function Activity()
    {
        return $this->hasOne('App\Activity');
    }

    public function getAll() {
        return $this->all();
    }
    public function getList() {
        $lang = Config::get('app.locale');
        return $this->pluck($lang . '_name', 'id')->toArray();
    }



    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        return $this->save();
    }

    public function edit($id, $data) {
        $ActivityType = $this->find($id);
        $ActivityType->ar_name = $data['ar_name'];
        $ActivityType->en_name = $data['en_name'];
        $ActivityType->active = $data['active'];
        return $ActivityType->save();
    }

    public function remove($id) {
        $ActivityType = $this->find($id);
        return $ActivityType->delete();
    }

}
