<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use Config;
class RestaurantCategory extends Model
{
    //
    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }
    public function getList() {
        $lang = Config::get('app.locale');
        return $this->pluck($lang . '_title', 'id')->toArray();
    }
    public function add($data) {
        $this->ar_title = $data['ar_title'];
        $this->en_title = $data['en_title'];
        return $this->save();
    }

    public function edit($id, $data) {
        $category = $this->find($id);
        $category->ar_title = $data['ar_title'];
        $category->en_title = $data['en_title'];
        $category->active = $data['active'];
        return $category->save();
    }

    public function remove($id) {
        $category = $this->find($id);
        return $category->delete();
    }
}
