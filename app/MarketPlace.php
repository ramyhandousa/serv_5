<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
class MarketPlace extends Model
{
    //

    public function city()
    {
        return $this->hasOne('App\ContinentCountryCity','id','city_id');
    }


    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data,$file) {
        $this->city_id = $data['city_id'];
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->ar_overview = $data['ar_overview'];
        $this->en_overview = $data['en_overview'];
        $this->image = $file;
        $this->latitude = $data['latitude'];
        $this->longitude = $data['longitude'];
        $this->phone = $data['phone'];
        $this->ar_address = $data['ar_address'];
        $this->en_address = $data['en_address'];
        $this->email = $data['email'];
        $this->no_of_shops = $data['no_of_shops'];
        return $this->save();
    }

    public function edit($id, $data,$file=false) {
        $Market = $this->find($id);
        $Market->city_id = $data['city_id'];
        $Market->ar_name = $data['ar_name'];
        $Market->en_name = $data['en_name'];
        $Market->ar_overview = $data['ar_overview'];
        $Market->en_overview = $data['en_overview'];
        if ($file) {
            $path = public_path('upload/activity/');
            $filename = $Market->image;
            File::Delete($path . $filename);
            $Market->image = $file;
        }

        $Market->latitude = $data['latitude'];
        $Market->longitude = $data['longitude'];
        $Market->phone = $data['phone'];
        $Market->ar_address = $data['ar_address'];
        $Market->en_address = $data['en_address'];
        $Market->email = $data['email'];
        $Market->no_of_shops = $data['no_of_shops'];
        return $Market->save();
    }

    public function remove($id) {
        $Market = $this->find($id);
        $path = public_path('upload/markets/');
        $filename = $Market['image'];
        File::Delete($path . $filename);
        return $Market->delete();
    }


}
