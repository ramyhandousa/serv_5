<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
    public function City() {
        return $this->hasOne('City', 'id', 'city_id');
    }

    public function getByCity($id) {
        return $this->where('city_id', $id)->get();
    }

    public function getList($id) {
        return $this->where('city_id', $id)->lists('ar_title', 'id');
    }

    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->city_id = strip_tags($data['city_id']);
        $this->ar_title = strip_tags($data['ar_title']);
        $this->en_title = strip_tags($data['en_title']);
        $this->lat = strip_tags($data['lat']);
        $this->lng = strip_tags($data['lng']);
        return $this->save();
    }

    public function edit($id, $data) {
        $photo = $this->find($id);
        $photo->city_id = strip_tags($data['city_id']);
        $photo->ar_title = strip_tags($data['ar_title']);
        $photo->en_title = strip_tags($data['en_title']);
        $photo->lat = strip_tags($data['lat']);
        $photo->lng = strip_tags($data['lng']);
        return $photo->save();
    }

    public function remove($id) {
        $photo = $this->find($id);
        return $photo->delete();
    }

    public function getNum() {
        return $this->count();
    }
}
