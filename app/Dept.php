<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use Config;
use App\Meal;

class Dept extends Model {

    //
    public function Meals() {
        return $this->hasMany(Meal::class, 'dept_id', 'id')->where('active', '!=', 'delete');
    }

    public function MealsActive() {
        return $this->hasMany(Meal::class, 'dept_id', 'id')->where('active', 'yes');
    }

    public function getList() {
        $lang = Config::get('app.locale');
        return $this->pluck($lang . '_name', 'id')->toArray();
    }

    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data, $file) {
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->image = $file;
        return $this->save();
    }

    public function edit($id, $data, $file = false) {
        $dept = $this->find($id);
        $dept->active = $data['active'];
        $dept->ar_name = $data['ar_name'];
        $dept->en_name = $data['en_name'];
        if ($file) {
            $path = public_path('upload/depts/');
            $filename = $dept->image;
            File::Delete($path . $filename);
            $dept->image = $file;
        }
        return $dept->save();
    }

    public function remove($id) {
        $dept = $this->find($id);
        $path = public_path('upload/depts/');
        $filename = $dept['image'];
        File::Delete($path . $filename);
        return $dept->delete();
    }

}
