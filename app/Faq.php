<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model {

    protected $table = 'faq';

    //
    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->ar_title = $data['ar_title'];
        $this->en_title = $data['en_title'];
        $this->ar_details = $data['ar_details'];
        $this->en_details = $data['en_details'];
        return $this->save();
    }

    public function edit($id, $data) {
        $faq = $this->find($id);
        $faq->ar_title = $data['ar_title'];
        $faq->en_title = $data['en_title'];
        $faq->ar_details = $data['ar_details'];
        $faq->en_details = $data['en_details'];
        return $faq->save();
    }

    public function remove($id) {
        $faq = $this->find($id);
        return $faq->delete();
    }

}
