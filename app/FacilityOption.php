<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacilityOption extends Model {

    //
    protected $table = 'facilities_options';
    public function add($data, $id) {
        $new = new FacilityOption();
        $new->ar_option = $data['ar_option'];
        $new->en_option = $data['en_option'];
        $new->facility_id = $id;
        return $new->save();
    }

    public function delByFacility($id) {
        return $this->where('facility_id', $id)->delete();
    }

}
