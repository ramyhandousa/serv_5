<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantGallery extends Model
{
    //
    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant', 'restaurant_id', 'id');
    }


    public function add($data,$file) {
           $this->restaurant_id = $data['restaurant_id'];
           $this->ar_name = $data['ar_name'];
           $this->en_name = $data['en_name'];
           $this->url = $file;
           $this->type = $data['type'];
           return $this->save();
    }

    public function edit($id, $data,$file=false) {
        $gallery = $this->find($id);
        $gallery->restaurant_id = $data['restaurant_id'];
        $gallery->ar_name = $data['ar_name'];
        $gallery->en_name = $data['en_name'];
        if ($file) {
            $path = public_path('upload/restaurant/gallery/');
            $filename =  $gallery->url;
            File::Delete($path . $filename);
            $gallery->url = $file;
        }
        $gallery->type = $data['type'];
        return $gallery->save();
    }

    public function remove($id) {
        $gallery = $this->find($id);
        $path = public_path('upload/restaurant/gallery/');
        $filename = $gallery['image'];
        File::Delete($path . $filename);
        return $gallery->delete();
    }

}
