<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use Config;

class Continent extends Model
{
    //

    public function countries()
    {
        return $this->hasMany('App\ContinentCountries','continent_id','id');
    }


    public function getAll() {
        return $this->all();
    }


    public function getList() {
        $lang = Config::get('app.locale');
        return $this->pluck($lang . '_name', 'id')->toArray();
    }


    public function getById($id) {
        return $this->find($id);
    }


    public function add($data, $file) {
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->ar_description = $data['ar_description'];
        $this->en_description = $data['en_description'];
        $this->code = $data['code'];
        $this->image = $file;
        return $this->save();
    }


    public function edit($id, $data, $file = false) {
        $continent = $this->find($id);

        $continent->active = $data['active'];
        $continent->ar_name = $data['ar_name'];
        $continent->en_name = $data['en_name'];
        $continent->ar_description = $data['ar_description'];
        $continent->en_description = $data['en_description'];
        $continent->code = $data['code'];
        if ($file) {
            $path = public_path('upload/continents/');
            $filename = $continent->image;
            File::Delete($path . $filename);
            $this->image = $file;
            $continent->image = $file;
        }
        return $continent->save();
    }


    public function remove($id) {
        $continent = $this->find($id);
        $path = public_path('upload/continents/');
        $filename = $continent['image'];
        File::Delete($path . $filename);
        return $continent->delete();
    }

}
