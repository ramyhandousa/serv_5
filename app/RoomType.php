<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model {

    //

    protected $table = 'room_types';
    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->ar_notes = $data['ar_notes'];
        $this->en_notes = $data['en_notes'];
        return $this->save();
    }

    public function edit($id, $data) {
        $RoomType = $this->find($id);
        $RoomType->ar_name = $data['ar_name'];
        $RoomType->en_name = $data['en_name'];
        $RoomType->ar_notes = $data['ar_notes'];
        $RoomType->en_notes = $data['en_notes'];
        return $RoomType->save();
    }

    public function remove($id) {
        $RoomType = $this->find($id);
        return $RoomType->delete();
    }

}
