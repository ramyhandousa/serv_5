<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parking extends Model {

    //

    protected $table = 'parking';
    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->icon = $data['icon'];
        return $this->save();
    }

    public function edit($id, $data) {
        $Parking = $this->find($id);
        $Parking->ar_name = $data['ar_name'];
        $Parking->en_name = $data['en_name'];
        $Parking->icon = $data['icon'];
        return $Parking->save();
    }

    public function remove($id) {
        $Parking = $this->find($id);
        return $Parking->delete();
    }

}
