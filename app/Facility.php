<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model {

    //
    public function Options() {
        return $this->hasMany(FacilityOption::class, 'facility_id', 'id');
    }

    public function hotels() {
        return $this->belongsToMany('App\Hotel', 'hotel_facilities');
    }

    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->icon = $data['icon'];
        $this->save();
        return $this->id;
    }

    public function edit($id, $data) {
        $Facility = $this->find($id);
        $Facility->ar_name = $data['ar_name'];
        $Facility->en_name = $data['en_name'];
        $Facility->icon = $data['icon'];
        return $Facility->save();
    }

    public function remove($id) {
        $Facility = $this->find($id);
        return $Facility->delete();
    }

}
