<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
class Job extends Model
{
    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data, $file) {
        $this->name = strip_tags($data['name']);
        $this->email = strip_tags($data['email']);
        $this->phone = strip_tags($data['phone']);
        $this->notes = strip_tags($data['notes']);
        $this->url = $file;
        return $this->save();
    }

    public function seen($id) {
        $one = $this->find($id);
        $one->seen = "yes";
        return $one->save();
    }

    public function remove($id) {
        $photo = $this->find($id);
        $filename = $photo->url;
        $path = public_path() . '/upload/job/';
        File::delete($path . $filename);
        return $photo->delete();
    }

    public function getNum() {
        return $this->count();
    }
}
