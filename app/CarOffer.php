<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
class CarOffer extends Model
{
    public function getAlls() {
        return $this->orderBy('id', 'desc')->paginate(8);
    }

    public function getExample() {
        return $this->orderBy('views', 'desc')->take(4)->get();
    }

    public function getAll() {
        return $this->all();
    }

    public function getLast() {
        return $this->orderBy('id', 'desc')->take(10)->get();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data, $file) {
        $this->ar_title = strip_tags($data['ar_title']);
        $this->en_title = strip_tags($data['en_title']);
        $this->ar_details = $data['ar_details'];
        $this->en_details = $data['en_details'];
        $this->image = $file;
        return $this->save();
    }

    public function edit($id, $data, $file = FALSE) {
        $photo = $this->find($id);
        $photo->active = strip_tags($data['active']);
        $photo->ar_title = strip_tags($data['ar_title']);
        $photo->en_title = strip_tags($data['en_title']);
        $photo->ar_details = $data['ar_details'];
        $photo->en_details = $data['en_details'];
        if ($file) {
            $path = public_path('upload/offers/');
            $filename = $photo->image;
            File::delete($path . $filename);
            $photo->image = $file;
        }
        return $photo->save();
    }

    public function remove($id) {
        $photo = $this->find($id);
        $filename = $photo->image;
        $path = public_path() . '/upload/offers/';
        File::delete($path . $filename);
        return $photo->delete();
    }

    public function addView($id) {
        $photo = $this->find($id);
        $photo->views = $photo->views + 1;
        return $photo->save();
    }

    public function getNum() {
        return $this->count();
    }
}
