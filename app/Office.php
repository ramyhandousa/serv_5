<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
class Office extends Model
{
    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->ar_title = strip_tags($data['ar_title']);
        $this->en_title = strip_tags($data['en_title']);
        $this->ar_address = strip_tags($data['ar_address']);
        $this->en_address = strip_tags($data['en_address']);
        $this->lat = strip_tags($data['lat']);
        $this->lng = strip_tags($data['lng']);
        return $this->save();
    }

    public function edit($id, $data) {
        $photo = $this->find($id);
        $photo->ar_title = strip_tags($data['ar_title']);
        $photo->en_title = strip_tags($data['en_title']);
        $photo->ar_address = strip_tags($data['ar_address']);
        $photo->en_address = strip_tags($data['en_address']);
        $photo->lat = strip_tags($data['lat']);
        $photo->lng = strip_tags($data['lng']);
        return $photo->save();
    }

    public function remove($id) {
        $photo = $this->find($id);
        return $photo->delete();
    }

    public function getNum() {
        return $this->count();
    }

}
