<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use App\Country;
use App\City;

class Governate extends Model {

    //
    public function Country() {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function Cities() {
        return $this->hasMany(City::class, 'governate_id', 'id');
    }

    public function CitiesActive() {
        return $this->hasMany(City::class, 'governate_id', 'id')->where('active', 'yes');
    }

    public function getList() {
        $lang = Config::get('app.locale');
        return $this->pluck($lang . '_name', 'id')->toArray();
    }

    public function getListCountry($id) {
        $lang = Config::get('app.locale');
        return $this->where('country_id', $id)->pluck($lang . '_name', 'id')->toArray();
    }

    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data, $file) {
        $this->country_id = $data['country_id'];
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->code = $data['code'];
        $this->ar_description = $data['ar_description'];
        $this->en_description = $data['en_description'];
        $this->image = $file;

        return $this->save();
    }

    public function edit($id, $data, $file = false) {
        $governate = $this->find($id);
        $governate->active = $data['active'];
        $governate->country_id = $data['country_id'];
        $governate->ar_name = $data['ar_name'];
        $governate->en_name = $data['en_name'];
        $governate->code = $data['code'];
        $governate->ar_description = $data['ar_description'];
        $governate->en_description = $data['en_description'];

        if ($file) {
            $path = public_path('upload/governates/');
            $filename = $country->image;
            File::Delete($path . $filename);
            $governate->image = $file;
        }
        return $governate->save();
    }

    public function remove($id) {
        $governate = $this->find($id);
        return $governate->delete();
    }

}
