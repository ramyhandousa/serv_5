<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Security extends Model
{
    public function getAll() {
        return $this->all();
    }
    
    public function getAlls() {
        return $this->where('active','yes')->get();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->ar_title = strip_tags($data['ar_title']);
        $this->en_title = strip_tags($data['en_title']);
        $this->price = $data['price'];
        return $this->save();
    }

    public function edit($id, $data) {
        $photo = $this->find($id);
        $photo->ar_title = strip_tags($data['ar_title']);
        $photo->en_title = strip_tags($data['en_title']);
        $photo->price = $data['price'];
        $photo->active = $data['active'];
        return $photo->save();
    }

    public function getNum() {
        return $this->count();
    }
}
