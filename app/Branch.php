<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;

class Branch extends Model {

    protected $table = 'branches';

    //
    public function Country() {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function City() {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function Governate() {
        return $this->hasOne(Governate::class, 'id', 'governate_id');
    }

    public function Employees() {
        return $this->hasMany(User::class, 'branch_id', 'id');
    }

    public function getList() {
        $lang = Config::get('app.locale');
        return $this->pluck($lang . '_name', 'id')->where('active', '!=', 'delete')->toArray();
    }

    public function getAll() {
        return $this->where('active', '!=', 'delete')->get();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->country_id = $data['country_id'];
        $this->governate_id = $data['governate_id'];
        $this->city_id = $data['city_id'];
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->phone = $data['phone'];
        $this->mobile = $data['mobile'];
        $this->ar_address = $data['ar_address'];
        $this->en_address = $data['en_address'];
        $this->lat = $data['lat'];
        $this->lng = $data['lng'];
        return $this->save();
    }

    public function edit($id, $data) {
        $branch = $this->find($id);
        $branch->active = $data['active'];
        $branch->country_id = $data['country_id'];
        $branch->governate_id = $data['governate_id'];
        $branch->city_id = $data['city_id'];
        $branch->ar_name = $data['ar_name'];
        $branch->en_name = $data['en_name'];
        $branch->phone = $data['phone'];
        $branch->mobile = $data['mobile'];
        $branch->ar_address = $data['ar_address'];
        $branch->en_address = $data['en_address'];
        $branch->lat = $data['lat'];
        $branch->lng = $data['lng'];
        return $branch->save();
    }

    public function remove($id) {
        $branch = $this->find($id);
        $branch->active = 'delete';
        return $branch->save();
    }

}
