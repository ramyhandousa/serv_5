<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarAddon extends Model
{
    public function getAll() {
        return $this->all();
    }
    
    public function getAlls() {
        return $this->where('active','yes')->get();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data, $file) {
        $this->ar_title = strip_tags($data['ar_title']);
        $this->en_title = strip_tags($data['en_title']);
        $this->ar_details = strip_tags($data['ar_details']);
        $this->en_details = strip_tags($data['en_details']);
        $this->price = $data['price'];
        $this->image = $file;
        return $this->save();
    }

    public function edit($id, $data, $file = FALSE) {
        $photo = $this->find($id);
        $photo->ar_title = strip_tags($data['ar_title']);
        $photo->en_title = strip_tags($data['en_title']);
        $photo->ar_details = strip_tags($data['ar_details']);
        $photo->en_details = strip_tags($data['en_details']);
        $photo->price = $data['price'];
        $photo->active = $data['active'];
        if ($file) {
            $path = public_path('images/addon/');
            $filename = $photo->image;
            File::delete($path . $filename);
            $photo->image = $file;
        }
        return $photo->save();
    }

    public function getNum() {
        return $this->count();
    }

    
}
