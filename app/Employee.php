<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->dept_id = $data['dept_id'];
        $this->first_name = $data['first_name'];
        $this->second_name = $data['second_name'];
        $this->gender = $data['gender'];
        $this->address1 = $data['address1'];
        $this->address2 = $data['address2'];
        $this->address3 = $data['address3'];
        return $this->save();
    }

    public function edit($id, $data) {
        $Employee = $this->find($id);
        $Employee->dept_id = $data['dept_id'];
        $Employee->first_name = $data['first_name'];
        $Employee->second_name = $data['second_name'];
        $Employee->gender = $data['gender'];
        $Employee->address1 = $data['address1'];
        $Employee->address2 = $data['address2'];
        $Employee->address3 = $data['address3'];
        return $Employee->save();
    }

    public function remove($id) {
        $Employee = $this->find($id);
        return $Employee->delete();
    }
}
