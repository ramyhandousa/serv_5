<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //

    public function employee()
    {
        return $this->hasMany('App\Employee');
    }

    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }

    public function getAll() {
        return $this->all();
    }

    public function getById($id) {
        return $this->find($id);
    }

    public function add($data) {
        $this->hotel_id = $data['hotel_id'];
        $this->ar_name = $data['ar_name'];
        $this->en_name = $data['en_name'];
        $this->ar_remarks = $data['ar_remarks'];
        $this->en_remarks = $data['en_remarks'];
        return $this->save();
    }

    public function edit($id,$data) {
        $Department = $this->find($id);
        $Department->hotel_id = $data['hotel_id'];
        $Department->ar_name = $data['ar_name'];
        $Department->en_name = $data['en_name'];
        $Department->ar_remarks = $data['ar_remarks'];
        $Department->en_remarks = $data['en_remarks'];
        return $Department->save();
    }

    public function remove($id) {
        $Department = $this->find($id);
        return $Department->delete();
    }


}
